#Script to generate LaTeX tables.
#Jedna se o jednoduchou tabulku s polozkami
#Alpha ... uhel vyrezu
#Refinement ... 0-7
#DOF ... pocet stupnu volnosti
#Elements ... pocet elementu
#W12 norma rezidua
#Pomerna W12 norma daneho zjemneni oproti reseni na nejvice zjemnene siti 
from SolutionComparatorExtended import SolutionComparatorExtended
from tabulate import tabulate
sc = SolutionComparatorExtended('/usr/users/kulvv1am/VK/FENICS', '/usr/users/kulvv1am/VK')
header = ["Refinement", "DOFs", "Elements", "|res|12", "rel12"]
for i in [1,2]:
	[deg, _unussed, _unussed, _unussed] = sc.dp.processParameters(i);
	print("Generating table of basic statistic and residual norms for alpha={}".format(deg))
	t = sc.generateComparisonTable(i)
	print tabulate(t, tablefmt="latex", headers=header)
