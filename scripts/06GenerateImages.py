#!/usr/bin/env python
'''
Created on March 28, 2016

Testing new computations for new models.

@author: Vojtech Kulvait
'''
from DirectoryProcessor import DirectoryProcessor
from VNotchSolverExtended import VNotchSolverExtended
from dolfin import *
import argparse
import SolutionExporter
import SolutionImporter

parser = argparse.ArgumentParser(description='Compute supplied tables')
parser.add_argument('--homedir', dest='homedir', default='/usr/users/kulvv1am/VK')
parser.add_argument('--destdir', dest='destdir', default='/usr/users/kulvv1am/VK/FENICS')
parser.add_argument('--imgdir', dest='imgdir', default='/usr/users/kulvv1am/VK/FENICS/IMAGES')
parser.add_argument('--table', dest='table', default=1)
parser.add_argument('--refinement', dest='refinement', default=1)
args = parser.parse_args()
print("Generating images for table {0}, refinement {1} into dir {2}.".format(args.table, args.refinement, args.imgdir))

dp = DirectoryProcessor(args.homedir)
[deg, a, kappa, mu] = dp.processParameters(args.table)
mu = 9.15 * 10**39
mu_linear = 26 * 10**9
r = 5.25 
#meshXML = dp.getMesh(3,2)
#solver  = VNotchSolverExtended(deg, r, mu, mu_linear)
#toExtrapolate = None
#VExtrapolate = None
#Anonlinear = solver.solveNonlinear(meshXML, 2, "damped", 0.7, toExtrapolate, VExtrapolate)
mesh = dolfin.Mesh(dp.getMesh(args.refinement, args.table))
functionSpace = dolfin.FunctionSpace(mesh, 'Lagrange', 2)
importer = SolutionImporter.SolutionImporter()
NLsolutionFile = "{0}/table{1}_refinement{2}_FENICSNLsolution.txt".format(args.destdir, args.table, args.refinement)
LsolutionFile = "{0}/table{1}_refinement{2}_FENICSLsolution.txt".format(args.destdir, args.table, args.refinement)
NLsolution = importer.importSolution(functionSpace, NLsolutionFile)
Lsolution = importer.importSolution(functionSpace, LsolutionFile)
solver = VNotchSolverExtended(deg, r, mu, mu_linear)
solver.initMesh(mesh, 2)
dp.createImages(Lsolution, NLsolution, solver, str(args.table) + "_refinement" + str(args.refinement), args.imgdir)
