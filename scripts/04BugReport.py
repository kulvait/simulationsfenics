'''
Created on May 23, 2014

@author: Vojtech Kulvait
'''
from dolfin import *
from BoundaryConditions import boundary_definition

def reproduceBug(meshDir):
    meshXML = "{0}/domain.xml".format(meshDir)
    mesh = dolfin.Mesh(meshXML);
    V = FunctionSpace(mesh, 'Lagrange', 2)
    bc = boundary_definition()
    u1 = dolfin.interpolate(dolfin.Expression('1'), V)
    u2 = dolfin.interpolate(dolfin.Expression('1'), V)
    bc1 = bc.zeroBC(V)
    bc2 = bc.zeroBC2(V)
    for i in range(0, len(bc1)):
        bc1[i].apply(u1.vector())
    bc2.apply(u2.vector())
    bc2.apply(u2.vector())
    
reproduceBug("/home/vojta/BUG")
