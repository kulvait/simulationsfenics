#!/usr/bin/env python
'''
Created on April 13, 2017

Calling solver and producing FEniCS solutions.

@author: Vojtech Kulvait
'''
from VNotchSolverPowerLaw import VNotchSolverPowerLaw
import argparse
import SolutionExporter
import SolutionImporter
import numpy as np

parser = argparse.ArgumentParser(description='Solve power law.')
parser.add_argument('--linear', dest='linearproblem', action='store_true')
parser.add_argument('--non-linear', dest='linearproblem', action='store_false')
parser.set_defaults(linearproblem=False)
parser.add_argument('--linearproblemname', dest='linearproblemname', default='A1L', help='For nonlinears problem this define initial solution.')
parser.add_argument('--mu_linear', dest='mu_linear', default=23.5*10**9, type=float)
parser.add_argument('--tau_zero', dest='tau_zero', default=0.5*10**9, type=float)
parser.add_argument('--qprime', dest='qprime', default=2.23, type=float)
parser.add_argument('--mu', dest='mu', default=24.7*10**9, type=float)
parser.add_argument('--mesh_xml', dest='mesh_xml', default='/usr/users/kulvv1am/POWERLAWMESHES/1/V/A_refinement5_dolfin.xml')
parser.add_argument('--F', dest='F', default=100*10**9, type=float)
parser.add_argument('--adaptive', dest='adaptive', default="damped",
                   help='Choose solver (newton(default), snes, damped)')
parser.add_argument('--damping', dest='damping', default=1.0, type=float)
parser.add_argument('--output', dest='output', default='/usr/users/kulvv1am/POWERLAWMESHES/1/V/FEniCS_A1L_refinement5.txt')

args = parser.parse_args()

#parameters["form_compiler"]["quadrature_degree"]
#solver  = VNotchSolverExtended(deg, r, mu, mu_linear)
#toExtrapolate = None
#VExtrapolate = None
#Anonlinear = solver.solveNonlinear(meshXML, 2, "damped", 0.7, toExtrapolate, VExtrapolate)

linearproblem = args.linearproblem
linearproblemname = str(args.linearproblemname)
mu_linear = float(args.mu_linear)
tau_zero = float(args.tau_zero)
qprime = float(args.qprime)
mu = float(args.mu)
mesh_xml = str(args.mesh_xml)
F = float(args.F)
adaptiveSolver = str(args.adaptive)
damping = float(args.damping)
outputfile = str(args.output)
exporter = SolutionExporter.SolutionExporter()
importer = SolutionImporter.SolutionImporter()
solver = VNotchSolverPowerLaw(linearproblem, mu_linear, tau_zero, qprime, mu)
try:
	print("Creating {}.".format(outputfile))
	if linearproblem:
		Alinear = solver.solveLinear(mesh_xml, 2)
		exporter.exportSolution(Alinear, outputfile)
	else:
		toExtrapolate = None
		VExtrapolate = None
		if False:#i != 0:
			extrapolateFile = "{0}/table{1}_refinement{2}_FENICSNLsolution.txt".format(outputDir, table, i)
			if os.path.isfile(extrapolateFile):
				meshExtrapolate = dolfin.Mesh(self.getMesh(i, table))
				VExtrapolate = dolfin.FunctionSpace(meshExtrapolate, 'Lagrange', 2)
				toExtrapolate = importer.importSolution(VExtrapolate, extrapolateFile)
		Anonlinear = solver.solveNonlinear(mesh_xml, 2, adaptiveSolver, damping, toExtrapolate, VExtrapolate)
		exporter.exportSolution(Anonlinear, outputfile)
except RuntimeError, e:
	sys.stderr.write("Error occurred solving table{0}_refinement{1}_FENICSNLsolution.txt".format(table, i+1))
	sys.stderr.write(e.message)
