#!/usr/bin/env python
'''
Created on April 15, 2016

Process all pvd and vtu files for single (table, refinement) pair in the directory to the single tuple (pvd, vtu) with all relevant information. This is needed in order to use ParaView effectively and be able to use calculator herein.

@author: Vojtech Kulvait
'''
from VTUDirectoryProcessor import VTUDirectoryProcessor
p = VTUDirectoryProcessor('/usr/users/kulvv1am/VK/FENICS/IMAGES')
p.createAllPvd()
