#!/usr/bin/env python
'''
Created on April 13, 2017

Calling solver and producing FEniCS solutions.

@author: Vojtech Kulvait
'''
import numpy as np
import sys
import os
from DirectoryVisitor import DirectoryVisitor as dv
from MaterialModel import MaterialModel
from GeometryParams import GeometryParams as gp
from VNotchSolverPowerLaw import VNotchSolverPowerLaw
import argparse
import SolutionExporter
import SolutionImporter
from dolfin import *

parser = argparse.ArgumentParser(description='Solve power law.')
parser.add_argument('--linear', dest='linearproblem', action='store_true')
parser.add_argument('--non-linear', dest='linearproblem', action='store_false')
parser.set_defaults(linearproblem=True)
parser.add_argument('--linearproblemname', dest='linearproblemname', default='A1L', help='For nonlinears problem this define initial solution.')
parser.add_argument('--model_label', dest='model_label', default='A1L', help='Label for model')
parser.add_argument('--mu_linear', dest='mu_linear', default=23.5*10**9, type=float)
parser.add_argument('--tau_zero', dest='tau_zero', default=0.5*10**9, type=float)
parser.add_argument('--qprime', dest='qprime', default=2.23, type=float)
parser.add_argument('--mu', dest='mu', default=24.7*10**9, type=float)
parser.add_argument('--mesh_xml', dest='mesh_xml', default='/usr/users/kulvv1am/POWERLAWMESHES/1/V/A_refinement5_dolfin.xml')
parser.add_argument('--F', dest='F', default=100*10**9, type=float)
parser.add_argument('--adaptive', dest='adaptive', default="damped",
                   help='Choose solver (newton(default), snes, damped)')
parser.add_argument('--damping', dest='damping', default=1.0, type=float)
parser.add_argument('--output', dest='output', default='/usr/users/kulvv1am/POWERLAWMESHES/1/V/FEniCS_A1L_refinement5.txt')

args = parser.parse_args()

def mySolveReal(xmlfile, modellabel, model, forcesize, dampingFactor):
    exporter = SolutionExporter.SolutionExporter()
    importer = SolutionImporter.SolutionImporter()
    solver = VNotchSolverPowerLaw(model.linearmodel, model.mu_l, model.tau0, model.qprime, model.mu)
    refinement = dv.numOfRefinements(xmlfile)
    parrentdir = os.path.dirname(xmlfile)
    outputfile = "{}/FEniCS_{}_refinement{}.txt".format(parrentdir, modellabel, refinement)
    try:
        print("Creating {}.".format(outputfile))
        if model.linearmodel:
            solution = solver.solveLinear(xmlfile, 2)
            exporter.exportSolution(solution, outputfile)
        else:
            toExtrapolate = None
            VExtrapolate = None
            solution = solver.solveNonlinear(xmlfile, 2, "damped", dampingFactor, toExtrapolate, VExtrapolate)
            exporter.exportSolution(solution, outputfile)
        inputFilePrefix = outputfile.rsplit('.', 1)[0]
        t13 = solver.gett13(solution)
        t23 = solver.gett23(solution)
        if model.linearmodel:
            e13 = solver.gete13Linear(solution)
            e23 = solver.gete23Linear(solution)
            t13.rename('T13_LINEAR', 'Size of stress component T13 in Pa.')
            t23.rename('T23_LINEAR', 'Size of stress component T23 in Pa.')
        else:
            e13 = solver.gete13Nonlinear(solution)
            e23 = solver.gete23Nonlinear(solution)
        ft13 = dolfin.io.File("{0}_t13.pvd".format(inputFilePrefix))
        ft23 = dolfin.io.File("{0}_t23.pvd".format(inputFilePrefix))
        fe13 = dolfin.io.File("{0}_e13.pvd".format(inputFilePrefix))
        fe23 = dolfin.io.File("{0}_e23.pvd".format(inputFilePrefix))
        ft13 << t13
        ft23 << t23
        fe13 << e13
        fe23 << e23
    except RuntimeError, e:
        sys.stderr.write("Error occured when generating {}.".format(outputfile))
        sys.stderr.write(e.message)


#parameters["form_compiler"]["quadrature_degree"]
#solver  = VNotchSolverExtended(deg, r, mu, mu_linear)
#toExtrapolate = None
#VExtrapolate = None
#Anonlinear = solver.solveNonlinear(meshXML, 2, "damped", 0.7, toExtrapolate, VExtrapolate)

linearproblem = args.linearproblem
linearproblemname = str(args.linearproblemname)
mu_linear = float(args.mu_linear)
tau_zero = float(args.tau_zero)
qprime = float(args.qprime)
mu = float(args.mu)
mesh_xml = str(args.mesh_xml)
F = float(args.F)
adaptiveSolver = str(args.adaptive)
damping = float(args.damping)
outputfile = str(args.output)
model_label = str(args.model_label)
model = MaterialModel(not linearproblem, mu_linear, mu, qprime, tau_zero, 'NoneApproxSol')
mySolveReal(mesh_xml, model_label, model, F, damping)
