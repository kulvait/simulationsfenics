#Script to generate LaTeX tables.
#Jedna se o jednoduchou tabulku s polozkami
#Alpha ... uhel vyrezu
#Refinement ... 0-7
#DOF ... pocet stupnu volnosti
#Elements ... pocet elementu
#W12 norma rezidua
#Pomerna W12 norma daneho zjemneni oproti reseni na nejvice zjemnene siti 
from SolutionComparatorExtended import SolutionComparatorExtended
from tabulate import tabulate
sc = SolutionComparatorExtended('/usr/users/kulvv1am/VK/FENICS', '/usr/users/kulvv1am/VK')
header = ["refinement", "T_{max}", "T^{lin}_{max}", "T_2", "T_{10}", "T^{lin}_2", "T^{lin}_{10}"]
for i in [1,2]:#
	xxx = sc.generateDistanceTable(i)
#	sc.generateCsvValues(i)#In directory '/usr/users/kulvv1am/VK/FENICS' it generates csv file with columns x, y, T, T_lin to use for further processing
	[deg, _unussed, _unussed, _unussed] = sc.dp.processParameters(i);
#	print "Difference between refinement (NL) 6 and 7 is less than 1% for T={}*10^8 and deg={}".format(sc.differenceBetweenRefinements(i, 7, 8, True, 10**8, 0.01)-1, deg)
#	print "Difference between refinement (NL) 6 and 7 is less than 10% for T={}*10^8 and deg={}".format(sc.differenceBetweenRefinements(i, 7, 8, True, 10**8, 0.1)-1, deg)
#	print "Difference between refinement (L) 6 and 7 is less than 1% for T={}*10^8 and deg={}".format(sc.differenceBetweenRefinements(i, 7, 8, False, 10**8, 0.01)-1, deg)
#	print "Difference between refinement (L) 6 and 7 is less than 1% for T={}*10^8 and deg={}".format(sc.differenceBetweenRefinements(i, 7, 8, False, 10**8, 0.1)-1, deg)
	print "Generating table for alpha = {}".format(deg)
	print tabulate(xxx, tablefmt="latex", headers=header)
