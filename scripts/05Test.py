#!/usr/bin/env python
'''
Created on March 28, 2016

Testing new computations for new models.

@author: Vojtech Kulvait
'''
from DirectoryProcessor import DirectoryProcessor
from VNotchSolverExtended import VNotchSolverExtended
import argparse
import SolutionExporter
import SolutionImporter

parser = argparse.ArgumentParser(description='Compute supplied tables')
parser.add_argument('integers', metavar='N', type=int, nargs='+',
                   help='Table numbers')
parser.add_argument('--adaptive', dest='adaptive', default="damped",
                   help='Choose solver (newton(default), snes, damped)')
parser.add_argument('--homedir', dest='homedir', default='/usr/users/kulvv1am/VK')
parser.add_argument('--destdir', dest='destdir', default='/usr/users/kulvv1am/VK/FENICS')
parser.add_argument('--frommesh', dest='frommesh', default=1)
parser.add_argument('--tomesh', dest='tomesh', default=1)
parser.add_argument('--damping', dest='damping', default=1.0)

print("Uz jsem tu")

args = parser.parse_args()
for i in range(0, len(args.integers)):
    if args.integers[i] > 9 or args.integers[i] < 1:
        args.integers = [1]
        break

print("Solving for tables {0}, adaptive {1}.".format(args.integers, args.adaptive))

#dp = DirectoryProcessor(args.homedir)
#[deg, a, kappa, mu] = dp.processParameters(args.integers)
mu = 9.15 * 10**39
mu_linear = 26 * 10**9
r = 5.25 
#meshXML = dp.getMesh(3,2)
#solver  = VNotchSolverExtended(deg, r, mu, mu_linear)
#toExtrapolate = None
#VExtrapolate = None
#Anonlinear = solver.solveNonlinear(meshXML, 2, "damped", 0.7, toExtrapolate, VExtrapolate)

def generateFenicsSolutions(dp, tableNums, outputDir, adaptiveSolver = None, fromMesh = None, toMesh = None, damping = None, mu = None, mu_linear = None, r = None):
	if adaptiveSolver is None:
		adaptiveSolver = "newton"
	else:
		adaptiveSolver = str(adaptiveSolver)
	if fromMesh is None:
		fromMesh = 1
	else:
		fromMesh = int(fromMesh)
	if toMesh is None:
		toMesh = 8
	else:
		toMesh = int(toMesh)
	if damping is None:
		damping = float(1.0)
	else:
		damping = float(damping)
	if mu is None:
		mu = 9.15 * 10**39
	else:
		mu = float(mu)
	if mu_linear is None:
		mu_linear = 26 * 10**9
	else:
		mu_linear = float(mu_linear)
	if r is None:
		r = 5.25
	else:
		r = float(r)
	fromMesh = fromMesh - 1;
	exporter = SolutionExporter.SolutionExporter()
	importer = SolutionImporter.SolutionImporter()
	for table in tableNums:
		[deg, a, kappa, mu_xxx] = dp.processParameters(table)
		maxRefinementNum = dp.getDensestMeshNumber(1000, table)
		print("Max refinement num for table {0} is {1}".format(table, maxRefinementNum))
		tomesh = min(maxRefinementNum, toMesh)
		for i in range(fromMesh, toMesh):
			meshXML = dp.getMesh(i+1, table)
			print("Mesh to process: {0}".format(meshXML))
			solver = VNotchSolverExtended(deg, r, mu, mu_linear)
			try:
				print("Solving table{0}_refinement{1}_FENICSNLsolution.txt".format(table, i+1))
				toExtrapolate = None
				VExtrapolate = None
				if False:#i != 0:
					extrapolateFile = "{0}/table{1}_refinement{2}_FENICSNLsolution.txt".format(outputDir, table, i)
					if os.path.isfile(extrapolateFile):
						meshExtrapolate = dolfin.Mesh(self.getMesh(i, table))
						VExtrapolate = dolfin.FunctionSpace(meshExtrapolate, 'Lagrange', 2)
						toExtrapolate = importer.importSolution(VExtrapolate, extrapolateFile)
				Anonlinear = solver.solveNonlinear(meshXML, 2, adaptiveSolver, damping, toExtrapolate, VExtrapolate)
				exporter.exportSolution(Anonlinear, "{0}/table{1}_refinement{2}_FENICSNLsolution.txt".format(outputDir, table, i+1))
				Alinear = solver.solveLinear(meshXML, 2)
				exporter.exportSolution(Alinear, "{0}/table{1}_refinement{2}_FENICSLsolution.txt".format(outputDir, table, i+1))
			except RuntimeError, e:
				sys.stderr.write("Error occurred solving table{0}_refinement{1}_FENICSNLsolution.txt".format(table, i+1))
				sys.stderr.write(e.message)

generateFenicsSolutions(dp, args.integers, args.destdir, args.adaptive, args.frommesh, args.tomesh, args.damping, mu, mu_linear, r)
