#!/usr/bin/env python
'''
Created on May 14, 2014

Script to process directory with all COMSOL meshes and compute solutions in FeniCS.
It then stores its solutions to the file.

@author: Vojtech Kulvait
'''
from DirectoryProcessor import DirectoryProcessor
import argparse

parser = argparse.ArgumentParser(description='Compute supplied tables')
parser.add_argument('integers', metavar='N', type=int, nargs='+',
                   help='Table numbers')
parser.add_argument('--adaptive', dest='adaptive', default="damped",
                   help='Choose solver (newton(default), snes, damped)')
parser.add_argument('--homedir', dest='homedir', default='/home/vojta')
parser.add_argument('--destdir', dest='destdir', default='/fenicsSolutions')
parser.add_argument('--frommesh', dest='frommesh', default=1)
parser.add_argument('--tomesh', dest='tomesh', default=1)
parser.add_argument('--damping', dest='damping', default=1.0)

args = parser.parse_args()
for i in range(0, len(args.integers)):
    if args.integers[i] > 9 or args.integers[i] < 1:
        args.integers = [1]
        break

print("Solving for tables {0}, adaptive {1}.".format(args.integers, args.adaptive))

dp = DirectoryProcessor(args.homedir + '/comsolAntiplaneSolutions', 0.01)
dp.generateFenicsSolutions(args.integers, args.homedir + args.destdir, args.adaptive, args.frommesh, args.tomesh, args.damping)
