#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Created on Apr 12, 2017

@author: Vojtech Kulvait
'''
#For directoryVisitor you have to define function that calls actual solver
#Import me using testVisitor = __import__('10TestVisitor')
import numpy as np
import sys
import os
from DirectoryVisitor import DirectoryVisitor as dv
from MaterialModel import MaterialModel
from GeometryParams import GeometryParams as gp
from VNotchSolverPowerLaw import VNotchSolverPowerLaw
import argparse
import SolutionExporter
import SolutionImporter
from dolfin import *

def mySolveFAKEPRINT(xmlfile, modellabel, model, forcesize):
	refinement = dv.numOfRefinements(xmlfile)
	parrentdir = os.path.dirname(xmlfile)
	sys.stdout.write("Creating solution file {}/FEniCS_{}_refinement{}.txt\n".format(parrentdir, modellabel, refinement))

def mySolveReal(xmlfile, modellabel, model, forcesize):
	exporter = SolutionExporter.SolutionExporter()
	importer = SolutionImporter.SolutionImporter()
	solver = VNotchSolverPowerLaw(model.linearmodel, model.mu_l, model.tau0, model.qprime, model.mu)
	refinement = dv.numOfRefinements(xmlfile)
	parrentdir = os.path.dirname(xmlfile)
	outputfile = "{}/FEniCS_{}_refinement{}.txt".format(parrentdir, modellabel, refinement)
	try:
		print("Creating {}.".format(outputfile))
		if model.linearmodel:
			solution = solver.solveLinear(xmlfile, 2)
			exporter.exportSolution(solution, outputfile)
		else:
			toExtrapolate = None
			VExtrapolate = None
			solution = solver.solveNonlinear(xmlfile, 2, "damped", 1.0, toExtrapolate, VExtrapolate)
			exporter.exportSolution(solution, outputfile)
		inputFilePrefix = outputfile.rsplit('.', 1)[0]
		t13 = solver.gett13(solution)
		t23 = solver.gett23(solution)
		if model.linearmodel:
			e13 = solver.gete13Linear(solution)
			e23 = solver.gete23Linear(solution)
			t13.rename('T13_LINEAR', 'Size of stress component T13 in Pa.')
			t23.rename('T23_LINEAR', 'Size of stress component T23 in Pa.')
		else:
			e13 = solver.gete13Nonlinear(solution)
			e23 = solver.gete23Nonlinear(solution)
		ft13 = dolfin.io.File("{0}_t13.pvd".format(inputFilePrefix))
		ft23 = dolfin.io.File("{0}_t23.pvd".format(inputFilePrefix))
		fe13 = dolfin.io.File("{0}_e13.pvd".format(inputFilePrefix))
		fe23 = dolfin.io.File("{0}_e23.pvd".format(inputFilePrefix))
		ft13 << t13
		ft23 << t23
		fe13 << e13
		fe23 << e23
	except RuntimeError, e:
		sys.stderr.write("Error occured when generating {}.".format(outputfile))
		sys.stderr.write(e.message)

def callVisitor():
	vis = dv("/usr/users/kulvv1am/POWERLAWMESHES", mySolveReal)
	modelsdic = MaterialModel.getModels();
	keys = modelsdic.keys();
	#Definitions of particular subdictionaries to minimize run time 
        linearkeys = filter(lambda x: modelsdic[x].linearmodel, keys)
        nonlinearkeys = filter(lambda x: modelsdic[x].nonlinearmodel, keys)
	gummetalkeys = ('A1L', 'A1NLMIN', 'A1NLMAX')
	lineardic = {x: modelsdic[x] for x in linearkeys}
	nonlineardic = {x: modelsdic[x] for x in nonlinearkeys}
	gummetaldic = {x: modelsdic[x] for x in gummetalkeys}
	forcesize = 100*10^6
	alphaslist = np.append([1, 5, 10], np.arange(20,180,20))
	#Create domains definitions
	domaindeflist = [gp.createGeometryParams('V')]
	#useddomains = ["VO","VC"]
	#usedrs = [0, 0.05, 0.01, 0.005, 0.001, 0.0001]
	#for g in useddomains:
	#	for r in usedrs:
	#		domaindeflist.append(gp.createGeometryParams('{}_{}'.format(g, r)))
	refinementslist = [1,2,3,4,5]
	overwriteexisting = False
	vis.generateFEniCSSolutions(modelsdic, forcesize, alphaslist, domaindeflist, refinementslist, overwriteexisting)

callVisitor()
