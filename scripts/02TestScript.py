'''
Created on May 14, 2014

@author: vojta
'''
from VNotchSolver import VNotchSolver
from DirectoryProcessor import DirectoryProcessor
dp = DirectoryProcessor('/home/vojta/comsolAntiplaneSolutions', 0.01)
[deg, a, kappa, mu] = dp.processParameters(2)
meshXML = dp.getMesh(3, 2)
solver = VNotchSolver(deg, a, kappa, mu)
Alinear = solver.solveLinear(meshXML, 2)
Anonlinear = solver.solveNonlinear(meshXML, 2, True)