#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Created on Apr 12, 2017

@author: Vojtech Kulvait
'''
#For directoryVisitor you have to define function that calls actual solver
#Import me using testVisitor = __import__('10TestVisitor')
import numpy as np
import sys
import os
from DirectoryVisitor import DirectoryVisitor as dv
from MaterialModel import MaterialModel
from GeometryParams import GeometryParams as gp
from VNotchSolverPowerLaw import VNotchSolverPowerLaw
import argparse
import SolutionExporter
import SolutionImporter
from ErrorNorms import ErrorNorm
from dolfin import *
import copy
from math import log10, floor
import tabulate as tab

tab.LATEX_ESCAPE_RULES = {}

data = []
#data.append({'geom':'V', 'alpha':1})
#data.append({'geom':'V', 'alpha':90})
#data.append({'geom':'V', 'alpha':170})
data.append({'geom':'VO', 'alpha':1, 'rc':0.001})
data.append({'geom':'VO', 'alpha':1, 'rc':0.01})
data.append({'geom':'VO', 'alpha':1, 'rc':0.05})
data.append({'geom':'VC', 'alpha':100, 'rc':0.001})
data.append({'geom':'VC', 'alpha':100, 'rc':0.01})
data.append({'geom':'VC', 'alpha':100, 'rc':0.05})

models = MaterialModel.getModels()
keys = filter(lambda x: x[:2]!='A4', models.keys())
keys = filter(lambda x: x[:3]=='A1L' or x[2]=='N', keys)
models = {x:models[x] for x in keys}

basedir = '/usr/nobackup2/kulvv1am/POWERLAWMESHES'
for geo in data:
	en = ErrorNorm(basedir, geo, models)
	print en.generateLatexCode(6)
	raw_input("Press Enter to continue...")
#xxx = en.process(3)
#print ErrorNorm.generateLatexTable(xxx[0], xxx[1])

