#!/usr/bin/env python
'''
Created on March 28, 2016

Testing new computations for new models.

@author: Vojtech Kulvait
'''
from VNotchSolverPowerLaw import VNotchSolverPowerLaw
from dolfin import *
import argparse
import SolutionExporter
import SolutionImporter

parser = argparse.ArgumentParser(description='Create images.')
parser.add_argument('--linear', dest='linearproblem', action='store_true')
parser.add_argument('--non-linear', dest='linearproblem', action='store_false')
parser.set_defaults(linearproblem=False)
parser.add_argument('--linearproblemname', dest='linearproblemname', default='A1L', help='For nonlinears problem this define initial solution.')
parser.add_argument('--mu_linear', dest='mu_linear', default=23.5*10**9, type=float)
parser.add_argument('--tau_zero', dest='tau_zero', default=0.5*10**9, type=float)
parser.add_argument('--qprime', dest='qprime', default=2.23, type=float)
parser.add_argument('--mu', dest='mu', default=24.7*10**9, type=float)
parser.add_argument('--mesh_xml', dest='mesh_xml', default='/usr/users/kulvv1am/POWERLAWMESHES/1/V/A_refinement1_dolfin.xml')
parser.add_argument('--F', dest='F', default=100*10**9, type=float)
parser.add_argument('--inputfile', dest='inputfile', default='/usr/users/kulvv1am/POWERLAWMESHES/1/V/FEniCS_A1L_refinement1.txt')
parser.add_argument('--output', dest='output', default='/usr/users/kulvv1am/POWERLAWMESHES/1/V/IMG')

args = parser.parse_args()
linearproblem = args.linearproblem
linearproblemname = str(args.linearproblemname)
mu_linear = float(args.mu_linear)
tau_zero = float(args.tau_zero)
qprime = float(args.qprime)
mu = float(args.mu)
mesh_xml = str(args.mesh_xml)
F = float(args.F)
outputfile = str(args.output)
inputfile = str(args.inputfile)
mesh = dolfin.Mesh(mesh_xml)
functionSpace = dolfin.FunctionSpace(mesh, 'Lagrange', 2)
importer = SolutionImporter.SolutionImporter()
solution = importer.importSolution(functionSpace, inputfile)
solver = VNotchSolverPowerLaw(linearproblem, mu_linear, tau_zero, qprime, mu)
solver.initMesh(mesh, 2)
inputFilePrefix = args.inputfile.rsplit('.', 1)[0]
t13 = solver.gett13(solution)
t23 = solver.gett23(solution)
if linearproblem:       
	e13 = solver.gete13Linear(solution)
	e23 = solver.gete23Linear(solution)
	t13.rename('T13_LINEAR', 'Size of stress component T13 in Pa.')
	t23.rename('T23_LINEAR', 'Size of stress component T23 in Pa.')
else:
	e13 = solver.gete13Nonlinear(solution)
	e23 = solver.gete23Nonlinear(solution)
ft13 = dolfin.io.File("{0}_t13.pvd".format(inputFilePrefix))
ft23 = dolfin.io.File("{0}_t23.pvd".format(inputFilePrefix))
fe13 = dolfin.io.File("{0}_e13.pvd".format(inputFilePrefix))
fe23 = dolfin.io.File("{0}_e23.pvd".format(inputFilePrefix))
ft13 << t13
ft23 << t23
fe13 << e13
fe23 << e23
