from paraview.simple import *

images = '/mnt/r1d1/PYTHON/FeniCSVKSCRIPTS/sandbox/figures/V1T.xdmf'
xdmf = XDMFReader(FileNames=[images])
rv = CreateRenderView('RenderView')
Render()
rv.ResetCamera()
rv.OrientationAxesVisibility = 0
rv.InteractionMode = '3D'
rv.ViewSize = [1000, 1000]
rv.CameraViewUp = [0.0, 1.0, 0.0]
rv.CameraPosition = [0.5, 0.5, 0.51]
rv.CameraFocalPoint = [0.5, 0.5, 0]
rv.CameraViewAngle = 90.0
rv.AxesGrid.Visibility = 1
rv.AxesGrid.XTitle = ''
rv.AxesGrid.YTitle = ''
rv.AxesGrid.ShowEdges = 0

from writeImages import ImageWritter
xdmffile = '/mnt/r1d1/PYTHON/FeniCSVKSCRIPTS/sandbox/figures/V1T.xdmf'
tfrange = [-1, 1]
belowCol = 1
aboveCol = 1
rangeLabelFormat = '%.0f'
labelFormat='%.0f'
iv = ImageWritter(xdmffile)

fileName = '/b/git/doctoralthesismff/MFF/chapters/05/img/errors/test.png'
title = '$T_{13}^0 - T_{13}^5$ [MPa]'
calcFormula = '(T13NLB3R1-T13NLB3R6)/10^6'
iv.createImage(fileName, calcFormula, title, tfrange, belowCol, aboveCol, rangeLabelFormat, labelFormat)

fileName = '/b/git/doctoralthesismff/MFF/chapters/05/img/errors/T13ERR.png'
title = '$T_{13}^0 - T_{13}^5$ [MPa]'
calcFormula = '(T13NLB3R1-T13NLB3R6)/10^6'
iv.createImage(fileName, calcFormula, title, tfrange, belowCol, aboveCol, rangeLabelFormat, labelFormat)

fileName = '/b/git/doctoralthesismff/MFF/chapters/05/img/errors/T23ERR.png'
title = '$T_{23}^0 - T_{23}^5$ [MPa]'
calcFormula = '(T23NLB3R1-T23NLB3R6)/10^6' 
iv.createImage(fileName, calcFormula, title, tfrange, belowCol, aboveCol, rangeLabelFormat, labelFormat)

#convert T13ERR.png T23ERR.png  +append TERRNLB3.png

fileName = '/b/git/doctoralthesismff/MFF/chapters/05/img/errors/TERR0.png'
title = '$|T^{\: 0} - T^{5}|$ [MPa]'
calcFormula = 'sqrt((T23NLB3R1-T23NLB3R6)^2+(T13NLB3R1-T13NLB3R6)^2)/10^6'
iv.createImage(fileName, calcFormula, title, tfrange, belowCol, aboveCol, rangeLabelFormat, labelFormat)

fileName = '/b/git/doctoralthesismff/MFF/chapters/05/img/errors/TERR1.png'
title = '$|T^{\, 1} - T^{5}|$ [MPa]'
calcFormula = 'sqrt((T23NLB3R2-T23NLB3R6)^2+(T13NLB3R2-T13NLB3R6)^2)/10^6'
iv.createImage(fileName, calcFormula, title, tfrange, belowCol, aboveCol, rangeLabelFormat, labelFormat)

fileName = '/b/git/doctoralthesismff/MFF/chapters/05/img/errors/TERR2.png'
title = '$|T^{\; 2} - T^{5}|$ [MPa]'
calcFormula = 'sqrt((T23NLB3R3-T23NLB3R6)^2+(T13NLB3R3-T13NLB3R6)^2)/10^6'
iv.createImage(fileName, calcFormula, title, tfrange, belowCol, aboveCol, rangeLabelFormat, labelFormat)

fileName = '/b/git/doctoralthesismff/MFF/chapters/05/img/errors/TERR3.png'
title = '$|T^{\ 3} - T^{5}|$ [MPa]'
calcFormula = 'sqrt((T23NLB3R4-T23NLB3R6)^2+(T13NLB3R4-T13NLB3R6)^2)/10^6'
iv.createImage(fileName, calcFormula, title, tfrange, belowCol, aboveCol, rangeLabelFormat, labelFormat)

fileName = '/b/git/doctoralthesismff/MFF/chapters/05/img/errors/TERR4.png'
title = '$|T^{\quad 4} - T^{5}|$ [MPa]'
calcFormula = 'sqrt((T23NLB3R5-T23NLB3R6)^2+(T13NLB3R5-T13NLB3R6)^2)/10^6'
iv.createImage(fileName, calcFormula, title, tfrange, belowCol, aboveCol, rangeLabelFormat, labelFormat)

# convert image1.png image2.png image3.png -append result/result-sprite.png







#Distribution of error in the vicinity of V-notch tip in T23
rv.ResetCamera()
Render()
rv.OrientationAxesVisibility = 0
rv.InteractionMode = '3D'
rv.ViewSize = [1000, 1200]
rv.CameraViewUp = [0.0, 1.0, 0.0]
rv.CameraPosition = [0.5, 0.60, 28.64*2**0.5*0.86]
rv.CameraFocalPoint = [0.5, 0.60, 0.0]
rv.CameraViewAngle = 2
rv.AxesGrid.Visibility = 0
clc = Calculator(Input=xdmf)
clc.Function = '(T23NLB3R1-T23NLB3R6)/10^6'
dis = Show(clc, rv)
ColorBy(dis, ('POINTS', 'Result'))
LUT = GetColorTransferFunction('Result')
LUT.ApplyPreset('Blue to Red Rainbow', True)
LUT.RescaleTransferFunction(-1, 1)
CB = GetScalarBar(LUT, rv)
CB.AutoOrient = 0
CB.Orientation = 'Horizontal'
CB.RangeLabelFormat = '%0.0f'
CB.Title = '$T_{23}^0 - T_{23}^5$ [MPa]'
CB.TitleFontSize = 10
#CB.Title = ''
CB.TitleFontSize = 20
CB.LabelFontSize = 15
CB.ComponentTitle  = ''
CB.Position = [0.26, 0.90-0.06]
CB.Position2 = [0.5, 0]
Render()
WriteImage('test.png')
WriteImage('/b/dizertace/T23ERR.png')

#Distribution of error in the vicinity of V-notch tip in T13
rv.ResetCamera()
Render()
rv.OrientationAxesVisibility = 0
rv.InteractionMode = '3D'
rv.ViewSize = [1000, 1150]
rv.CameraViewUp = [0.0, 1.0, 0.0]
rv.CameraPosition = [0.5, 0.57, 28.64*2**0.5*0.82]
rv.CameraFocalPoint = [0.5, 0.57, 0.0]
rv.CameraViewAngle = 2
rv.AxesGrid.Visibility = 0
clc = Calculator(Input=xdmf)
clc.Function = '(T13NLB3R1-T13NLB3R6)/10^6'
dis = Show(clc, rv)
ColorBy(dis, ('POINTS', 'Result'))
LUT = GetColorTransferFunction('Result')
LUT.ApplyPreset('Blue to Red Rainbow', True)
LUT.RescaleTransferFunction(-1, 1)
CB = GetScalarBar(LUT, rv)
CB.AutoOrient = 0
CB.Orientation = 'Horizontal'
CB.RangeLabelFormat = '%0.0f'
CB.Title = '$T_{13}^5 - T_{13}^5$ [MPa]'
CB.TitleFontSize = 10
#CB.Title = ''
CB.ComponentTitle  = ''
CB.Position = [0.26, 0.90]
CB.Position2 = [0.5, 0]
Render()
WriteImage('test.png')
WriteImage('T13ERR.png')




#Now create picture of T23^6-T23^1
rv.ResetCamera()
Render()
rv.CameraViewUp = [0.0, 1.0, 0.0]
rv.CameraPosition = [0.5, 0.57, 28.64*2**0.5*0.82]
rv.CameraFocalPoint = [0.5, 0.57, 0.0]
rv.CameraViewAngle = 2
clc = Calculator(Input=xdmf)
clc.Function = 'sqrt((T13LIN3R1-T13LIN3R6)^2 + (T23LIN3R1-T23LIN3R6)^2)/10^6'
dis = Show(clc, rv)
ColorBy(dis, ('POINTS', 'Result'))
LUT = GetColorTransferFunction('Result')
LUT.ApplyPreset('Blue to Red Rainbow', True)
LUT.RescaleTransferFunction(-1, 1)
LUT.UseBelowRangeColor = 1
LUT.UseAboveRangeColor = 1
CB = GetScalarBar(LUT, rv)
CB.AutoOrient = 0
CB.Orientation = 'Horizontal'
CB.RangeLabelFormat = '%0.0f'
CB.Title = '$T_{23}^5-T_{23}^0$ [MPa]'
CB.TitleFontSize = 11
#CB.Title = ''
CB.ComponentTitle  = ''
CB.Position = [0.26, 0.90]
CB.Position2 = [0.5, 0]
Render()
WriteImage('test.png')




clc = Calculator(Input=xdmf)
clc.Function = 'T23LIN3R6/10^6'
dis = Show(clc, rv)
ColorBy(dis, ('POINTS', 'Result'))
LUT = GetColorTransferFunction('Result')
LUT.ApplyPreset('Blue to Red Rainbow', True)
LUT.RescaleTransferFunction(0.0, 200)
CB = GetScalarBar(LUT, rv)
CB.AutoOrient = 0
CB.Orientation = 'Horizontal'
CB.Title = '$T_{23}$ [MPa]'
CB.RangeLabelFormat = '%0.0f'

# change scalar bar placement
resultLUTColorBar.Position = [0.2883062880324544, 0.8907142857142859]
resultLUTColorBar.Position2 = [0.43000000000000005, 0.11999999999999977]
Render()

v1Txdmf = XDMFReader(FileNames=['/mnt/r1d1/PYTHON/FeniCSVKSCRIPTS/sandbox/figures/V1T.xdmf'])
v1Txdmf.PointArrayStatus = ['T13LIN3AS', 'T13LIN3R1', 'T13LIN3R2', 'T13LIN3R3', 'T13LIN3R4', 'T13LIN3R5', 'T13LIN3R6', 'T13NLB3AS', 'T13NLB3R1', 'T13NLB3R2', 'T13NLB3R3', 'T13NLB3R4', 'T13NLB3R5', 'T13NLB3R6', 'T13NLS3AS', 'T13NLS3R1', 'T13NLS3R2', 'T13NLS3R3', 'T13NLS3R4', 'T13NLS3R5', 'T13NLS3R6', 'T23LIN3AS', 'T23LIN3R1', 'T23LIN3R2', 'T23LIN3R3', 'T23LIN3R4', 'T23LIN3R5', 'T23LIN3R6', 'T23NLB3AS', 'T23NLB3R1', 'T23NLB3R2', 'T23NLB3R3', 'T23NLB3R4', 'T23NLB3R5', 'T23NLB3R6', 'T23NLS3AS', 'T23NLS3R1', 'T23NLS3R2', 'T23NLS3R3', 'T23NLS3R4', 'T23NLS3R5', 'T23NLS3R6']

# Properties modified on v1Txdmf
v1Txdmf.GridStatus = ['grid_1']

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [534, 580]

# get color transfer function/color map for 'T13LIN3R6'
t13LIN3R6LUT = GetColorTransferFunction('T13LIN3R6')

# show data in view
v1TxdmfDisplay = Show(v1Txdmf, renderView1)
# trace defaults for the display properties.
v1TxdmfDisplay.Representation = 'Surface'
v1TxdmfDisplay.AmbientColor = [0.0, 0.0, 0.0]
v1TxdmfDisplay.ColorArrayName = ['POINTS', 'T13LIN3R6']
v1TxdmfDisplay.LookupTable = t13LIN3R6LUT
v1TxdmfDisplay.OSPRayScaleArray = 'T13LIN3R6'
v1TxdmfDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
v1TxdmfDisplay.SelectOrientationVectors = 'None'
v1TxdmfDisplay.ScaleFactor = 0.1
v1TxdmfDisplay.SelectScaleArray = 'T13LIN3R6'
v1TxdmfDisplay.GlyphType = 'Arrow'
v1TxdmfDisplay.PolarAxes = 'PolarAxesRepresentation'
v1TxdmfDisplay.ScalarOpacityUnitDistance = 0.03170162391333772
v1TxdmfDisplay.GaussianRadius = 0.05
v1TxdmfDisplay.SetScaleArray = ['POINTS', 'T13LIN3R6']
v1TxdmfDisplay.ScaleTransferFunction = 'PiecewiseFunction'
v1TxdmfDisplay.OpacityArray = ['POINTS', 'T13LIN3R6']
v1TxdmfDisplay.OpacityTransferFunction = 'PiecewiseFunction'

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
v1TxdmfDisplay.PolarAxes.PolarAxisTitleColor = [0.0, 0.0, 0.0]
v1TxdmfDisplay.PolarAxes.PolarAxisLabelColor = [0.0, 0.0, 0.0]
v1TxdmfDisplay.PolarAxes.LastRadialAxisTextColor = [0.0, 0.0, 0.0]
v1TxdmfDisplay.PolarAxes.SecondaryRadialAxesTextColor = [0.0, 0.0, 0.0]

# reset view to fit data
rme = '/b/git/doctoralthesismff/MFF/chapters/05/img/errors/T23ERR.png'
title = '$T_{23}^0 - T_{23}^5$ [MPa]'
calcFormula = '(T23NLB3R1-T23NLB3R6)/10^6'
iv.createImage(fileName, calcFormula, title, tfrange, belowCol, aboveCol, rangeLabelFormat, labelFormat)enderView1.ResetCamera()

