#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'XDMF Reader'
figuresxdmf = XDMFReader(FileNames=['/mnt/r1d1/PYTHON/FeniCSVKSCRIPTS/sandbox/tstsolutions/figures.xdmf'])
figuresxdmf.PointArrayStatus = ['LIN3AS', 'LIN3R1', 'LIN3R3', 'LIN3R6', 'NLB3AS', 'NLB3R1', 'NLB3R3', 'NLB3R6', 'NLS3AS', 'NLS3R1', 'NLS3R3', 'NLS3R6']

# Properties modified on figuresxdmf
figuresxdmf.GridStatus = ['grid_1']

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [930, 580]

# get color transfer function/color map for 'LIN3R1'
lIN3R1LUT = GetColorTransferFunction('LIN3R1')
lIN3R1LUT.RGBPoints = [21592.791015625, 0.231373, 0.298039, 0.752941, 501731660.3955078, 0.865003, 0.865003, 0.865003, 1003441728.0, 0.705882, 0.0156863, 0.14902]
lIN3R1LUT.ScalarRangeInitialized = 1.0

# show data in view
figuresxdmfDisplay = Show(figuresxdmf, renderView1)
# trace defaults for the display properties.
figuresxdmfDisplay.Representation = 'Surface'
figuresxdmfDisplay.AmbientColor = [0.0, 0.0, 0.0]
figuresxdmfDisplay.ColorArrayName = ['POINTS', 'LIN3R1']
figuresxdmfDisplay.LookupTable = lIN3R1LUT
figuresxdmfDisplay.OSPRayScaleArray = 'LIN3R1'
figuresxdmfDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
figuresxdmfDisplay.SelectOrientationVectors = 'LIN3AS'
figuresxdmfDisplay.ScaleFactor = 0.1
figuresxdmfDisplay.SelectScaleArray = 'LIN3R1'
figuresxdmfDisplay.GlyphType = 'Arrow'
figuresxdmfDisplay.PolarAxes = 'PolarAxesRepresentation'
figuresxdmfDisplay.ScalarOpacityUnitDistance = 0.03170162391333772
figuresxdmfDisplay.GaussianRadius = 0.05
figuresxdmfDisplay.SetScaleArray = ['POINTS', 'LIN3R1']
figuresxdmfDisplay.ScaleTransferFunction = 'PiecewiseFunction'
figuresxdmfDisplay.OpacityArray = ['POINTS', 'LIN3R1']
figuresxdmfDisplay.OpacityTransferFunction = 'PiecewiseFunction'

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
figuresxdmfDisplay.PolarAxes.PolarAxisTitleColor = [0.0, 0.0, 0.0]
figuresxdmfDisplay.PolarAxes.PolarAxisLabelColor = [0.0, 0.0, 0.0]
figuresxdmfDisplay.PolarAxes.LastRadialAxisTextColor = [0.0, 0.0, 0.0]
figuresxdmfDisplay.PolarAxes.SecondaryRadialAxesTextColor = [0.0, 0.0, 0.0]

# reset view to fit data
renderView1.ResetCamera()

#changing interaction mode based on data extents
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [0.5, 0.5, 10000.0]
renderView1.CameraFocalPoint = [0.5, 0.5, 0.0]

# show color bar/color legend
figuresxdmfDisplay.SetScalarBarVisibility(renderView1, True)

# set scalar coloring
ColorBy(figuresxdmfDisplay, ('POINTS', 'NLS3R6'))

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(lIN3R1LUT, renderView1)

# rescale color and/or opacity maps used to include current data range
figuresxdmfDisplay.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
figuresxdmfDisplay.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'NLS3R6'
nLS3R6LUT = GetColorTransferFunction('NLS3R6')
nLS3R6LUT.RGBPoints = [6521.35009765625, 0.231373, 0.298039, 0.752941, 249078044.67504883, 0.865003, 0.865003, 0.865003, 498149568.0, 0.705882, 0.0156863, 0.14902]
nLS3R6LUT.ScalarRangeInitialized = 1.0

# Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
nLS3R6LUT.ApplyPreset('Blue to Red Rainbow', True)

# Rescale transfer function
nLS3R6LUT.RescaleTransferFunction(0.0, 200000000.0)

# get opacity transfer function/opacity map for 'NLS3R6'
nLS3R6PWF = GetOpacityTransferFunction('NLS3R6')
nLS3R6PWF.Points = [6521.35009765625, 0.0, 0.5, 0.0, 498149568.0, 1.0, 0.5, 0.0]
nLS3R6PWF.ScalarRangeInitialized = 1

# Rescale transfer function
nLS3R6PWF.RescaleTransferFunction(0.0, 200000000.0)

# Properties modified on nLS3R6LUT
nLS3R6LUT.UseBelowRangeColor = 1

# Properties modified on nLS3R6LUT
nLS3R6LUT.UseBelowRangeColor = 0

# Properties modified on nLS3R6LUT
nLS3R6LUT.UseAboveRangeColor = 1

# get color legend/bar for nLS3R6LUT in view renderView1
nLS3R6LUTColorBar = GetScalarBar(nLS3R6LUT, renderView1)
nLS3R6LUTColorBar.Title = 'NLS3R6'
nLS3R6LUTColorBar.ComponentTitle = ''
nLS3R6LUTColorBar.TitleColor = [0.0, 0.0, 0.0]
nLS3R6LUTColorBar.LabelColor = [0.0, 0.0, 0.0]

# change scalar bar placement
nLS3R6LUTColorBar.Position = [0.8851288056206089, 0.22857142857142854]
nLS3R6LUTColorBar.Position2 = [0.12, 0.4299999999999994]

# Properties modified on nLS3R6LUTColorBar
nLS3R6LUTColorBar.AutoOrient = 0
nLS3R6LUTColorBar.Orientation = 'Horizontal'

# change scalar bar placement
nLS3R6LUTColorBar.Position = [0.28750585480093693, 0.8954761904761902]
nLS3R6LUTColorBar.Position2 = [0.42999999999999927, 0.11999999999999977]

# Properties modified on nLS3R6LUTColorBar
nLS3R6LUTColorBar.AutomaticLabelFormat = 0
nLS3R6LUTColorBar.LabelFormat = '%-#6.0g'

# Properties modified on nLS3R6LUTColorBar
nLS3R6LUTColorBar.LabelFormat = '%-#6.1g'

# Properties modified on nLS3R6LUTColorBar
nLS3R6LUTColorBar.LabelFormat = '%-#0.1g'

# Properties modified on nLS3R6LUTColorBar
nLS3R6LUTColorBar.LabelFormat = '%-#0.3g'

# Properties modified on nLS3R6LUTColorBar
nLS3R6LUTColorBar.LabelFormat = '%-#0.3f'

# Properties modified on nLS3R6LUTColorBar
nLS3R6LUTColorBar.LabelFormat = '%-#0.3g'

# Properties modified on nLS3R6LUTColorBar
nLS3R6LUTColorBar.DrawSubTickMarks = 0

# Properties modified on nLS3R6LUTColorBar
nLS3R6LUTColorBar.AddRangeLabels = 0

# Properties modified on nLS3R6LUTColorBar
nLS3R6LUTColorBar.AddRangeLabels = 1
nLS3R6LUTColorBar.RangeLabelFormat = '%4.0e'

# Properties modified on nLS3R6LUTColorBar
nLS3R6LUTColorBar.TitleFontSize = 9

# Properties modified on nLS3R6LUTColorBar
nLS3R6LUTColorBar.LabelFontSize = 10

# Properties modified on nLS3R6LUTColorBar
nLS3R6LUTColorBar.RangeLabelFormat = '%3.0e'

# Properties modified on nLS3R6LUTColorBar
nLS3R6LUTColorBar.RangeLabelFormat = '%3.0f'

# Properties modified on nLS3R6LUTColorBar
nLS3R6LUTColorBar.RangeLabelFormat = '%3.0g'

# Properties modified on nLS3R6LUTColorBar
nLS3R6LUTColorBar.LabelFormat = '%-#3.0g'

# Properties modified on nLS3R6LUTColorBar
nLS3R6LUTColorBar.LabelFormat = '%-#3.1g'

# Properties modified on nLS3R6LUTColorBar
nLS3R6LUTColorBar.LabelFormat = '%-#4.1g'

# Properties modified on nLS3R6LUTColorBar
nLS3R6LUTColorBar.LabelFormat = '%-#4.0g'

# Properties modified on nLS3R6LUTColorBar
nLS3R6LUTColorBar.LabelFormat = '%-#6.0g'

# Properties modified on nLS3R6LUTColorBar
nLS3R6LUTColorBar.LabelFormat = '%-#6.3g'

# Properties modified on nLS3R6LUTColorBar
nLS3R6LUTColorBar.LabelFormat = '%-#6.2g'

# create a new 'Contour'
contour1 = Contour(Input=figuresxdmf)
contour1.ContourBy = ['POINTS', 'LIN3R1']
contour1.Isosurfaces = [501731660.3955078]
contour1.PointMergeMethod = 'Uniform Binning'

# show data in view
contour1Display = Show(contour1, renderView1)
# trace defaults for the display properties.
contour1Display.Representation = 'Surface'
contour1Display.AmbientColor = [0.0, 0.0, 0.0]
contour1Display.ColorArrayName = ['POINTS', 'NLS3R6']
contour1Display.LookupTable = nLS3R6LUT
contour1Display.OSPRayScaleArray = 'LIN3AS'
contour1Display.OSPRayScaleFunction = 'PiecewiseFunction'
contour1Display.SelectOrientationVectors = 'LIN3AS'
contour1Display.ScaleFactor = 0.0031969577074050905
contour1Display.SelectScaleArray = 'LIN3AS'
contour1Display.GlyphType = 'Arrow'
contour1Display.PolarAxes = 'PolarAxesRepresentation'
contour1Display.GaussianRadius = 0.0015984788537025453
contour1Display.SetScaleArray = ['POINTS', 'LIN3AS']
contour1Display.ScaleTransferFunction = 'PiecewiseFunction'
contour1Display.OpacityArray = ['POINTS', 'LIN3AS']
contour1Display.OpacityTransferFunction = 'PiecewiseFunction'

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
contour1Display.PolarAxes.PolarAxisTitleColor = [0.0, 0.0, 0.0]
contour1Display.PolarAxes.PolarAxisLabelColor = [0.0, 0.0, 0.0]
contour1Display.PolarAxes.LastRadialAxisTextColor = [0.0, 0.0, 0.0]
contour1Display.PolarAxes.SecondaryRadialAxesTextColor = [0.0, 0.0, 0.0]

# hide data in view
Hide(figuresxdmf, renderView1)

# show color bar/color legend
contour1Display.SetScalarBarVisibility(renderView1, True)

# Properties modified on contour1
contour1.ContourBy = ['POINTS', 'NLS3R6']

# Properties modified on contour1Display
contour1Display.SelectScaleArray = 'None'

# Properties modified on contour1Display
contour1Display.SelectOrientationVectors = 'None'

# Properties modified on contour1
contour1.Isosurfaces = [501731660.3955078, 0.0, 50000000.0, 100000000.0, 150000000.0, 200000000.0]

# change representation type
contour1Display.SetRepresentationType('Outline')

# set scalar coloring
ColorBy(contour1Display, ('POINTS', 'NLS3R3'))

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(nLS3R6LUT, renderView1)

# rescale color and/or opacity maps used to include current data range
contour1Display.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
contour1Display.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'NLS3R3'
nLS3R3LUT = GetColorTransferFunction('NLS3R3')
nLS3R3LUT.RGBPoints = [38031716.0, 0.231373, 0.298039, 0.752941, 132543282.0, 0.865003, 0.865003, 0.865003, 227054848.0, 0.705882, 0.0156863, 0.14902]
nLS3R3LUT.ScalarRangeInitialized = 1.0

# set active source
SetActiveSource(figuresxdmf)

# show data in view
figuresxdmfDisplay = Show(figuresxdmf, renderView1)

# show color bar/color legend
figuresxdmfDisplay.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(figuresxdmf, renderView1)

# show data in view
figuresxdmfDisplay = Show(figuresxdmf, renderView1)

# show color bar/color legend
figuresxdmfDisplay.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(contour1, renderView1)

# set active source
SetActiveSource(contour1)

# show data in view
contour1Display = Show(contour1, renderView1)

# show color bar/color legend
contour1Display.SetScalarBarVisibility(renderView1, True)

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(nLS3R3LUT, renderView1)

# hide data in view
Hide(contour1, renderView1)

# show data in view
contour1Display = Show(contour1, renderView1)

# change solid color
contour1Display.AmbientColor = [1.0, 1.0, 1.0]

# hide data in view
Hide(figuresxdmf, renderView1)

# set active source
SetActiveSource(figuresxdmf)

# show data in view
figuresxdmfDisplay = Show(figuresxdmf, renderView1)

# show color bar/color legend
figuresxdmfDisplay.SetScalarBarVisibility(renderView1, True)

# change representation type
figuresxdmfDisplay.SetRepresentationType('Outline')

# change representation type
figuresxdmfDisplay.SetRepresentationType('Surface')

# set active source
SetActiveSource(contour1)

# change representation type
contour1Display.SetRepresentationType('Point Gaussian')

# change representation type
contour1Display.SetRepresentationType('Outline')

# change representation type
contour1Display.SetRepresentationType('Surface')

# Properties modified on contour1
contour1.Isosurfaces = [501731660.3955078, 501731660.3955078, 0.0, 50000000.0, 100000000.0, 150000000.0, 200000000.0]

# Properties modified on contour1
contour1.Isosurfaces = [501731660.3955078, 300000000.0, 0.0, 50000000.0, 100000000.0, 150000000.0, 200000000.0]

# Properties modified on contour1
contour1.Isosurfaces = [300000000.0, 0.0, 50000000.0, 100000000.0, 150000000.0, 200000000.0]

# Properties modified on contour1
contour1.Isosurfaces = [300000000.0, 0.0, 100000000.0, 150000000.0, 200000000.0]

# Properties modified on contour1
contour1.Isosurfaces = [300000000.0, 0.0, 150000000.0, 200000000.0]

# Properties modified on contour1
contour1.Isosurfaces = [300000000.0, 0.0, 200000000.0]

# Properties modified on contour1
contour1.Isosurfaces = [0.0, 200000000.0]

# Properties modified on contour1
contour1.Isosurfaces = [0.0, 0.0, 200000000.0]

# Properties modified on contour1
contour1.Isosurfaces = [0.0, 300000000.0, 200000000.0]

# Properties modified on contour1
contour1.Isosurfaces = [0.0, 200000000.0]

# set active source
SetActiveSource(figuresxdmf)

# Properties modified on nLS3R6LUT
nLS3R6LUT.UseAboveRangeColor = 0

# set active source
SetActiveSource(contour1)

# Properties modified on contour1
contour1.Isosurfaces = [0.0, 200000000.0, 200000000.0]

# Properties modified on contour1
contour1.Isosurfaces = [0.0, 200000000.0, 300000000.0]

# Properties modified on contour1
contour1.Isosurfaces = [0.0, 200000000.0, 300000000.0, 300000000.0]

# Properties modified on contour1
contour1.Isosurfaces = [0.0, 200000000.0, 300000000.0, 50000000.0]

# Properties modified on contour1
contour1.Isosurfaces = [0.0, 200000000.0, 300000000.0, 50000000.0, 50000000.0]

# Properties modified on contour1
contour1.Isosurfaces = [0.0, 200000000.0, 300000000.0, 50000000.0, 10000000.0]

# Properties modified on contour1
contour1.Isosurfaces = [0.0, 200000000.0, 300000000.0, 50000000.0, 10000000.0, 10000000.0]

# Properties modified on contour1
contour1.Isosurfaces = [0.0, 200000000.0, 300000000.0, 50000000.0, 10000000.0, 100000000.0]

# Properties modified on contour1
contour1.Isosurfaces = [0.0, 200000000.0, 300000000.0, 50000000.0, 10000000.0, 100000000.0, 100000000.0]

# Properties modified on contour1
contour1.Isosurfaces = [0.0, 200000000.0, 300000000.0, 50000000.0, 10000000.0, 100000000.0, 50000000.0]

# Properties modified on contour1
contour1.Isosurfaces = [0.0, 200000000.0, 300000000.0, 50000000.0, 10000000.0, 100000000.0, 50000000.0, 50000000.0]

# Properties modified on contour1
contour1.Isosurfaces = [0.0, 200000000.0, 300000000.0, 50000000.0, 10000000.0, 100000000.0, 50000000.0, 70000000.0]

# set active source
SetActiveSource(figuresxdmf)

# Properties modified on renderView1.AxesGrid
renderView1.AxesGrid.Visibility = 1

# Properties modified on renderView1
renderView1.CenterAxesVisibility = 1

# Properties modified on renderView1
renderView1.CenterAxesVisibility = 0

# Properties modified on figuresxdmfDisplay.PolarAxes
figuresxdmfDisplay.PolarAxes.Visibility = 1

# Properties modified on figuresxdmfDisplay.PolarAxes
figuresxdmfDisplay.PolarAxes.Visibility = 0

# Properties modified on renderView1.AxesGrid
renderView1.AxesGrid.XTitle = 'X'
renderView1.AxesGrid.YTitle = 'Y'

# Properties modified on renderView1.AxesGrid
renderView1.AxesGrid.Visibility = 0

# Properties modified on renderView1.AxesGrid
renderView1.AxesGrid.Visibility = 1
renderView1.AxesGrid.XTitle = ''
renderView1.AxesGrid.YTitle = ''

# Properties modified on renderView1.AxesGrid
renderView1.AxesGrid.ShowGrid = 1

# Properties modified on renderView1.AxesGrid
renderView1.AxesGrid.ShowGrid = 0

# Properties modified on renderView1.AxesGrid
renderView1.AxesGrid.ShowEdges = 0

# Properties modified on renderView1.AxesGrid
renderView1.AxesGrid.ShowTicks = 0

# Properties modified on renderView1.AxesGrid
renderView1.AxesGrid.ShowTicks = 1

# Properties modified on renderView1.AxesGrid
renderView1.AxesGrid.Visibility = 0

# Properties modified on renderView1.AxesGrid
renderView1.AxesGrid.Visibility = 1

# Properties modified on renderView1
renderView1.OrientationAxesVisibility = 0

# Properties modified on renderView1
renderView1.OrientationAxesVisibility = 1

# Properties modified on renderView1
renderView1.OrientationAxesVisibility = 0

# set active source
SetActiveSource(contour1)

# Properties modified on contour1
contour1.Isosurfaces = []

# Properties modified on contour1
contour1.Isosurfaces = [0.0]

# Properties modified on contour1
contour1.Isosurfaces = [0.0, 0.0, 75000000.0, 150000000.0, 225000000.0, 300000000.0]

# Properties modified on contour1
contour1.Isosurfaces = [0.0, 75000000.0, 150000000.0, 225000000.0, 300000000.0]

# Properties modified on contour1
contour1.Isosurfaces = []

# Properties modified on contour1
contour1.Isosurfaces = [0.0, 55555555.55555555, 111111111.1111111, 166666666.66666666, 222222222.2222222, 277777777.7777778, 333333333.3333333, 388888888.8888889, 444444444.4444444, 500000000.0]

# Properties modified on contour1
contour1.Isosurfaces = []

# Properties modified on contour1
contour1.Isosurfaces = [0.0, 50000000.0, 100000000.0, 150000000.0, 200000000.0, 250000000.0, 300000000.0, 350000000.0, 400000000.0, 450000000.0, 500000000.0]

# set active source
SetActiveSource(figuresxdmf)

# create a new 'Calculator'
calculator1 = Calculator(Input=figuresxdmf)
calculator1.Function = ''

# Properties modified on calculator1
calculator1.Function = ''

# show data in view
calculator1Display = Show(calculator1, renderView1)
# trace defaults for the display properties.
calculator1Display.Representation = 'Surface'
calculator1Display.AmbientColor = [0.0, 0.0, 0.0]
calculator1Display.ColorArrayName = ['POINTS', 'LIN3R1']
calculator1Display.LookupTable = lIN3R1LUT
calculator1Display.OSPRayScaleArray = 'LIN3R1'
calculator1Display.OSPRayScaleFunction = 'PiecewiseFunction'
calculator1Display.SelectOrientationVectors = 'LIN3AS'
calculator1Display.ScaleFactor = 0.1
calculator1Display.SelectScaleArray = 'LIN3R1'
calculator1Display.GlyphType = 'Arrow'
calculator1Display.PolarAxes = 'PolarAxesRepresentation'
calculator1Display.ScalarOpacityUnitDistance = 0.03170162391333772
calculator1Display.GaussianRadius = 0.05
calculator1Display.SetScaleArray = ['POINTS', 'LIN3R1']
calculator1Display.ScaleTransferFunction = 'PiecewiseFunction'
calculator1Display.OpacityArray = ['POINTS', 'LIN3R1']
calculator1Display.OpacityTransferFunction = 'PiecewiseFunction'

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
calculator1Display.PolarAxes.PolarAxisTitleColor = [0.0, 0.0, 0.0]
calculator1Display.PolarAxes.PolarAxisLabelColor = [0.0, 0.0, 0.0]
calculator1Display.PolarAxes.LastRadialAxisTextColor = [0.0, 0.0, 0.0]
calculator1Display.PolarAxes.SecondaryRadialAxesTextColor = [0.0, 0.0, 0.0]

# hide data in view
Hide(figuresxdmf, renderView1)

# show color bar/color legend
calculator1Display.SetScalarBarVisibility(renderView1, True)

# Properties modified on calculator1
calculator1.Function = 'LIN3R6'

# Properties modified on calculator1
calculator1.Function = 'LIN3R6LIN3R6'

# set active source
SetActiveSource(figuresxdmf)

# set active source
SetActiveSource(figuresxdmf)

# show data in view
figuresxdmfDisplay = Show(figuresxdmf, renderView1)

# show color bar/color legend
figuresxdmfDisplay.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(calculator1, renderView1)

# set active source
SetActiveSource(calculator1)

# show data in view
calculator1Display = Show(calculator1, renderView1)

# show color bar/color legend
calculator1Display.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(calculator1, renderView1)

# show data in view
calculator1Display = Show(calculator1, renderView1)

# show color bar/color legend
calculator1Display.SetScalarBarVisibility(renderView1, True)

# Properties modified on calculator1
calculator1.Function = 'LIN3R6'

# set scalar coloring
ColorBy(calculator1Display, ('POINTS', 'LIN3R6'))

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(lIN3R1LUT, renderView1)

# rescale color and/or opacity maps used to include current data range
calculator1Display.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
calculator1Display.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'LIN3R6'
lIN3R6LUT = GetColorTransferFunction('LIN3R6')
lIN3R6LUT.RGBPoints = [5073.91943359375, 0.231373, 0.298039, 0.752941, 9864433128.959717, 0.865003, 0.865003, 0.865003, 19728861184.0, 0.705882, 0.0156863, 0.14902]
lIN3R6LUT.ScalarRangeInitialized = 1.0

# set scalar coloring
ColorBy(calculator1Display, ('POINTS', 'Result'))

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(lIN3R6LUT, renderView1)

# rescale color and/or opacity maps used to include current data range
calculator1Display.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
calculator1Display.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'Result'
resultLUT = GetColorTransferFunction('Result')
resultLUT.RGBPoints = [5073.91943359375, 0.231373, 0.298039, 0.752941, 9864433128.959717, 0.865003, 0.865003, 0.865003, 19728861184.0, 0.705882, 0.0156863, 0.14902]
resultLUT.ScalarRangeInitialized = 1.0

# Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
resultLUT.ApplyPreset('Blue to Red Rainbow', True)

# Rescale transfer function
resultLUT.RescaleTransferFunction(0.0, 100000000.0)

# get opacity transfer function/opacity map for 'Result'
resultPWF = GetOpacityTransferFunction('Result')
resultPWF.Points = [5073.91943359375, 0.0, 0.5, 0.0, 19728861184.0, 1.0, 0.5, 0.0]
resultPWF.ScalarRangeInitialized = 1

# Rescale transfer function
resultPWF.RescaleTransferFunction(0.0, 100000000.0)

# Properties modified on resultLUT
resultLUT.UseAboveRangeColor = 1

# Properties modified on resultLUT
resultLUT.UseAboveRangeColor = 0

# Rescale transfer function
resultLUT.RescaleTransferFunction(0.0, 300000000.0)

# Rescale transfer function
resultPWF.RescaleTransferFunction(0.0, 300000000.0)

# Properties modified on resultLUT
resultLUT.UseAboveRangeColor = 1

# Properties modified on resultLUT
resultLUT.UseAboveRangeColor = 0

# get color legend/bar for resultLUT in view renderView1
resultLUTColorBar = GetScalarBar(resultLUT, renderView1)
resultLUTColorBar.Title = 'Result'
resultLUTColorBar.ComponentTitle = ''
resultLUTColorBar.TitleColor = [0.0, 0.0, 0.0]
resultLUTColorBar.LabelColor = [0.0, 0.0, 0.0]

# change scalar bar placement
resultLUTColorBar.Position = [0.8933255269320843, 0.22142857142857142]
resultLUTColorBar.Position2 = [0.12000000000000055, 0.4299999999999997]

# hide data in view
Hide(figuresxdmf, renderView1)

# hide data in view
Hide(contour1, renderView1)

# set active source
SetActiveSource(contour1)

# show data in view
contour1Display = Show(contour1, renderView1)

# hide data in view
Hide(calculator1, renderView1)

# set active source
SetActiveSource(figuresxdmf)

# show data in view
figuresxdmfDisplay = Show(figuresxdmf, renderView1)

# show color bar/color legend
figuresxdmfDisplay.SetScalarBarVisibility(renderView1, True)

# set active source
SetActiveSource(calculator1)

# show data in view
calculator1Display = Show(calculator1, renderView1)

# show color bar/color legend
calculator1Display.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(calculator1, renderView1)

# show data in view
calculator1Display = Show(calculator1, renderView1)

# show color bar/color legend
calculator1Display.SetScalarBarVisibility(renderView1, True)

# set active source
SetActiveSource(calculator1)

# Properties modified on calculator1
calculator1.Function = 'LIN3R6-LIN3R1'

# Rescale transfer function
resultLUT.RescaleTransferFunction(-10000000.0, 10000000.0)

# Rescale transfer function
resultPWF.RescaleTransferFunction(-10000000.0, 10000000.0)

# Properties modified on resultLUT
resultLUT.UseAboveRangeColor = 1

# Properties modified on resultLUT
resultLUT.UseAboveRangeColor = 0

# Properties modified on resultLUT
resultLUT.UseBelowRangeColor = 1

# Properties modified on resultLUT
resultLUT.UseBelowRangeColor = 0

# Properties modified on resultLUT
resultLUT.UseBelowRangeColor = 1

# Properties modified on resultLUT
resultLUT.UseAboveRangeColor = 1

# Properties modified on resultLUT
resultLUT.UseAboveRangeColor = 0

# Properties modified on resultLUT
resultLUT.UseBelowRangeColor = 0

# Properties modified on calculator1
calculator1.Function = 'LIN3R6-NLB3R1'

# Properties modified on calculator1
calculator1.Function = 'NLBR6-NLB3R1'

# Properties modified on calculator1
calculator1.Function = 'NLB3R6-NLB3R1'

# Properties modified on calculator1
calculator1.Function = 'NLB3R6-NLS3R6'

# Properties modified on calculator1
calculator1.Function = 'NLB3R6-LIN3R6'

# Properties modified on resultLUT
resultLUT.UseAboveRangeColor = 1

# Properties modified on resultLUT
resultLUT.UseBelowRangeColor = 1

# Rescale transfer function
resultLUT.RescaleTransferFunction(-100000000.0, 100000000.0)

# Rescale transfer function
resultPWF.RescaleTransferFunction(-100000000.0, 100000000.0)

# Properties modified on calculator1
calculator1.Function = 'NLS3R6-LIN3R6'

# Rescale transfer function
resultLUT.RescaleTransferFunction(-10000000.0, 10000000.0)

# Rescale transfer function
resultPWF.RescaleTransferFunction(-10000000.0, 10000000.0)

# Rescale transfer function
resultLUT.RescaleTransferFunction(-50000000.0, 50000000.0)

# Rescale transfer function
resultPWF.RescaleTransferFunction(-50000000.0, 50000000.0)

# set active source
SetActiveSource(figuresxdmf)

# create a new 'Calculator'
calculator2 = Calculator(Input=figuresxdmf)
calculator2.Function = ''

# Properties modified on calculator2
calculator2.Function = ''

# show data in view
calculator2Display = Show(calculator2, renderView1)
# trace defaults for the display properties.
calculator2Display.Representation = 'Surface'
calculator2Display.AmbientColor = [0.0, 0.0, 0.0]
calculator2Display.ColorArrayName = ['POINTS', 'LIN3R1']
calculator2Display.LookupTable = lIN3R1LUT
calculator2Display.OSPRayScaleArray = 'LIN3R1'
calculator2Display.OSPRayScaleFunction = 'PiecewiseFunction'
calculator2Display.SelectOrientationVectors = 'LIN3AS'
calculator2Display.ScaleFactor = 0.1
calculator2Display.SelectScaleArray = 'LIN3R1'
calculator2Display.GlyphType = 'Arrow'
calculator2Display.PolarAxes = 'PolarAxesRepresentation'
calculator2Display.ScalarOpacityUnitDistance = 0.03170162391333772
calculator2Display.GaussianRadius = 0.05
calculator2Display.SetScaleArray = ['POINTS', 'LIN3R1']
calculator2Display.ScaleTransferFunction = 'PiecewiseFunction'
calculator2Display.OpacityArray = ['POINTS', 'LIN3R1']
calculator2Display.OpacityTransferFunction = 'PiecewiseFunction'

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
calculator2Display.PolarAxes.PolarAxisTitleColor = [0.0, 0.0, 0.0]
calculator2Display.PolarAxes.PolarAxisLabelColor = [0.0, 0.0, 0.0]
calculator2Display.PolarAxes.LastRadialAxisTextColor = [0.0, 0.0, 0.0]
calculator2Display.PolarAxes.SecondaryRadialAxesTextColor = [0.0, 0.0, 0.0]

# hide data in view
Hide(figuresxdmf, renderView1)

# show color bar/color legend
calculator2Display.SetScalarBarVisibility(renderView1, True)

# Properties modified on calculator2
calculator2.ResultArrayName = 'NLS'

# Properties modified on calculator2
calculator2.Function = 'NLS3R6'

# rename source object
RenameSource('NLS', calculator2)

# Rescale transfer function
lIN3R1LUT.RescaleTransferFunction(0.0, 200000000.0)

# get opacity transfer function/opacity map for 'LIN3R1'
lIN3R1PWF = GetOpacityTransferFunction('LIN3R1')
lIN3R1PWF.Points = [21592.791015625, 0.0, 0.5, 0.0, 1003441728.0, 1.0, 0.5, 0.0]
lIN3R1PWF.ScalarRangeInitialized = 1

# Rescale transfer function
lIN3R1PWF.RescaleTransferFunction(0.0, 200000000.0)

# Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
lIN3R1LUT.ApplyPreset('Blue to Red Rainbow', True)

# set active source
SetActiveSource(figuresxdmf)

# show data in view
figuresxdmfDisplay = Show(figuresxdmf, renderView1)

# show color bar/color legend
figuresxdmfDisplay.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(contour1, renderView1)

# hide data in view
Hide(calculator1, renderView1)

# hide data in view
Hide(calculator2, renderView1)

# set active source
SetActiveSource(figuresxdmf)

# set active source
SetActiveSource(calculator2)

# hide data in view
Hide(figuresxdmf, renderView1)

# set active source
SetActiveSource(calculator2)

# show data in view
calculator2Display = Show(calculator2, renderView1)

# show color bar/color legend
calculator2Display.SetScalarBarVisibility(renderView1, True)

# reset view to fit data
renderView1.ResetCamera()

# set scalar coloring
ColorBy(calculator2Display, ('POINTS', 'NLS'))

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(lIN3R1LUT, renderView1)

# rescale color and/or opacity maps used to include current data range
calculator2Display.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
calculator2Display.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'NLS'
nLSLUT = GetColorTransferFunction('NLS')
nLSLUT.RGBPoints = [6521.35009765625, 0.231373, 0.298039, 0.752941, 249078044.67504883, 0.865003, 0.865003, 0.865003, 498149568.0, 0.705882, 0.0156863, 0.14902]
nLSLUT.ScalarRangeInitialized = 1.0

# Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
nLSLUT.ApplyPreset('Blue to Red Rainbow', True)

# Rescale transfer function
nLSLUT.RescaleTransferFunction(0.0, 200000000.0)

# get opacity transfer function/opacity map for 'NLS'
nLSPWF = GetOpacityTransferFunction('NLS')
nLSPWF.Points = [6521.35009765625, 0.0, 0.5, 0.0, 498149568.0, 1.0, 0.5, 0.0]
nLSPWF.ScalarRangeInitialized = 1

# Rescale transfer function
nLSPWF.RescaleTransferFunction(0.0, 200000000.0)

# Rescale transfer function
nLSLUT.RescaleTransferFunction(0.0, 300000000.0)

# Rescale transfer function
nLSPWF.RescaleTransferFunction(0.0, 300000000.0)

# Rescale transfer function
nLSLUT.RescaleTransferFunction(0.0, 250000000.0)

# Rescale transfer function
nLSPWF.RescaleTransferFunction(0.0, 250000000.0)

# set active source
SetActiveSource(figuresxdmf)

# set active source
SetActiveSource(calculator2)

# set active source
SetActiveSource(figuresxdmf)

# create a new 'Calculator'
calculator2_1 = Calculator(Input=figuresxdmf)
calculator2_1.Function = ''

# Properties modified on calculator2_1
calculator2_1.Function = ''

# show data in view
calculator2_1Display = Show(calculator2_1, renderView1)
# trace defaults for the display properties.
calculator2_1Display.Representation = 'Surface'
calculator2_1Display.AmbientColor = [0.0, 0.0, 0.0]
calculator2_1Display.ColorArrayName = ['POINTS', 'LIN3R1']
calculator2_1Display.LookupTable = lIN3R1LUT
calculator2_1Display.OSPRayScaleArray = 'LIN3R1'
calculator2_1Display.OSPRayScaleFunction = 'PiecewiseFunction'
calculator2_1Display.SelectOrientationVectors = 'LIN3AS'
calculator2_1Display.ScaleFactor = 0.1
calculator2_1Display.SelectScaleArray = 'LIN3R1'
calculator2_1Display.GlyphType = 'Arrow'
calculator2_1Display.PolarAxes = 'PolarAxesRepresentation'
calculator2_1Display.ScalarOpacityUnitDistance = 0.03170162391333772
calculator2_1Display.GaussianRadius = 0.05
calculator2_1Display.SetScaleArray = ['POINTS', 'LIN3R1']
calculator2_1Display.ScaleTransferFunction = 'PiecewiseFunction'
calculator2_1Display.OpacityArray = ['POINTS', 'LIN3R1']
calculator2_1Display.OpacityTransferFunction = 'PiecewiseFunction'

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
calculator2_1Display.PolarAxes.PolarAxisTitleColor = [0.0, 0.0, 0.0]
calculator2_1Display.PolarAxes.PolarAxisLabelColor = [0.0, 0.0, 0.0]
calculator2_1Display.PolarAxes.LastRadialAxisTextColor = [0.0, 0.0, 0.0]
calculator2_1Display.PolarAxes.SecondaryRadialAxesTextColor = [0.0, 0.0, 0.0]

# hide data in view
Hide(figuresxdmf, renderView1)

# show color bar/color legend
calculator2_1Display.SetScalarBarVisibility(renderView1, True)

# rename source object
RenameSource('LIN', calculator2_1)

# Properties modified on calculator2_1
calculator2_1.ResultArrayName = 'LIN'

# Properties modified on calculator2_1
calculator2_1.Function = 'LIN3R6'

# Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
lIN3R1LUT.ApplyPreset('Blue to Red Rainbow', True)

# Rescale transfer function
lIN3R1LUT.RescaleTransferFunction(0.0, 250000000.0)

# Rescale transfer function
lIN3R1PWF.RescaleTransferFunction(0.0, 250000000.0)

# hide data in view
Hide(calculator2, renderView1)

# set active source
SetActiveSource(calculator2)

# show data in view
calculator2Display = Show(calculator2, renderView1)

# show color bar/color legend
calculator2Display.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(calculator2_1, renderView1)

# set active source
SetActiveSource(calculator2_1)

# show data in view
calculator2_1Display = Show(calculator2_1, renderView1)

# show color bar/color legend
calculator2_1Display.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(calculator2_1, renderView1)

# show data in view
calculator2_1Display = Show(calculator2_1, renderView1)

# show color bar/color legend
calculator2_1Display.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(calculator2, renderView1)

# set scalar coloring
ColorBy(calculator2_1Display, ('POINTS', 'LIN'))

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(lIN3R1LUT, renderView1)

# rescale color and/or opacity maps used to include current data range
calculator2_1Display.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
calculator2_1Display.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'LIN'
lINLUT = GetColorTransferFunction('LIN')
lINLUT.RGBPoints = [5073.91943359375, 0.231373, 0.298039, 0.752941, 9864433128.959717, 0.865003, 0.865003, 0.865003, 19728861184.0, 0.705882, 0.0156863, 0.14902]
lINLUT.ScalarRangeInitialized = 1.0

# Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
lINLUT.ApplyPreset('Blue to Red Rainbow', True)

# Rescale transfer function
lINLUT.RescaleTransferFunction(0.0, 200000000.0)

# get opacity transfer function/opacity map for 'LIN'
lINPWF = GetOpacityTransferFunction('LIN')
lINPWF.Points = [5073.91943359375, 0.0, 0.5, 0.0, 19728861184.0, 1.0, 0.5, 0.0]
lINPWF.ScalarRangeInitialized = 1

# Rescale transfer function
lINPWF.RescaleTransferFunction(0.0, 200000000.0)

# set active source
SetActiveSource(calculator2)

# show data in view
calculator2Display = Show(calculator2, renderView1)

# show color bar/color legend
calculator2Display.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(calculator2_1, renderView1)

# set active source
SetActiveSource(calculator2_1)

# show data in view
calculator2_1Display = Show(calculator2_1, renderView1)

# show color bar/color legend
calculator2_1Display.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(calculator2_1, renderView1)

# show data in view
calculator2_1Display = Show(calculator2_1, renderView1)

# show color bar/color legend
calculator2_1Display.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(calculator2_1, renderView1)

# show data in view
calculator2_1Display = Show(calculator2_1, renderView1)

# show color bar/color legend
calculator2_1Display.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(calculator2_1, renderView1)

# show data in view
calculator2_1Display = Show(calculator2_1, renderView1)

# show color bar/color legend
calculator2_1Display.SetScalarBarVisibility(renderView1, True)

# set active source
SetActiveSource(calculator1)

# hide data in view
Hide(calculator2, renderView1)

# hide data in view
Hide(calculator2_1, renderView1)

# set active source
SetActiveSource(calculator1)

# show data in view
calculator1Display = Show(calculator1, renderView1)

# show color bar/color legend
calculator1Display.SetScalarBarVisibility(renderView1, True)

# reset view to fit data
renderView1.ResetCamera()

# set active source
SetActiveSource(figuresxdmf)

# create a new 'Calculator'
calculator2_2 = Calculator(Input=figuresxdmf)
calculator2_2.Function = ''

# Properties modified on calculator2_2
calculator2_2.Function = ''

# show data in view
calculator2_2Display = Show(calculator2_2, renderView1)
# trace defaults for the display properties.
calculator2_2Display.Representation = 'Surface'
calculator2_2Display.AmbientColor = [0.0, 0.0, 0.0]
calculator2_2Display.ColorArrayName = ['POINTS', 'LIN3R1']
calculator2_2Display.LookupTable = lIN3R1LUT
calculator2_2Display.OSPRayScaleArray = 'LIN3R1'
calculator2_2Display.OSPRayScaleFunction = 'PiecewiseFunction'
calculator2_2Display.SelectOrientationVectors = 'LIN3AS'
calculator2_2Display.ScaleFactor = 0.1
calculator2_2Display.SelectScaleArray = 'LIN3R1'
calculator2_2Display.GlyphType = 'Arrow'
calculator2_2Display.PolarAxes = 'PolarAxesRepresentation'
calculator2_2Display.ScalarOpacityUnitDistance = 0.03170162391333772
calculator2_2Display.GaussianRadius = 0.05
calculator2_2Display.SetScaleArray = ['POINTS', 'LIN3R1']
calculator2_2Display.ScaleTransferFunction = 'PiecewiseFunction'
calculator2_2Display.OpacityArray = ['POINTS', 'LIN3R1']
calculator2_2Display.OpacityTransferFunction = 'PiecewiseFunction'

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
calculator2_2Display.PolarAxes.PolarAxisTitleColor = [0.0, 0.0, 0.0]
calculator2_2Display.PolarAxes.PolarAxisLabelColor = [0.0, 0.0, 0.0]
calculator2_2Display.PolarAxes.LastRadialAxisTextColor = [0.0, 0.0, 0.0]
calculator2_2Display.PolarAxes.SecondaryRadialAxesTextColor = [0.0, 0.0, 0.0]

# hide data in view
Hide(figuresxdmf, renderView1)

# show color bar/color legend
calculator2_2Display.SetScalarBarVisibility(renderView1, True)

# Properties modified on calculator2_2
calculator2_2.Function = 'NLS3R6-'

# Properties modified on calculator2_2
calculator2_2.Function = 'NLS3R6-NLS3R1'

# hide data in view
Hide(calculator2_2, renderView1)

# set active source
SetActiveSource(calculator1)

# Properties modified on calculator1
calculator1.Function = 'NLS3R6-'

# Properties modified on calculator1
calculator1.Function = 'NLS3R6-NLS3R1'

# Rescale transfer function
resultLUT.RescaleTransferFunction(10000000.0, 10002048.0)

# Rescale transfer function
resultPWF.RescaleTransferFunction(10000000.0, 10002048.0)

# Rescale transfer function
resultLUT.RescaleTransferFunction(-1000000.0, 1000200.0)

# Rescale transfer function
resultPWF.RescaleTransferFunction(-1000000.0, 1000200.0)

# Properties modified on resultLUT
resultLUT.UseBelowRangeColor = 0

# Properties modified on resultLUT
resultLUT.UseAboveRangeColor = 0

# Properties modified on calculator1
calculator1.Function = 'NLB3R6-NLS3R1'

# Properties modified on calculator1
calculator1.Function = 'NLB3R6-NLB3R1'

# Properties modified on resultLUT
resultLUT.UseBelowRangeColor = 1

# Properties modified on resultLUT
resultLUT.UseAboveRangeColor = 1

# rename source object
RenameSource('ERR16', calculator1)

# create a new 'Calculator'
calculator1_1 = Calculator(Input=calculator1)
calculator1_1.Function = ''

# Properties modified on calculator1_1
calculator1_1.Function = ''

# show data in view
calculator1_1Display = Show(calculator1_1, renderView1)
# trace defaults for the display properties.
calculator1_1Display.Representation = 'Surface'
calculator1_1Display.AmbientColor = [0.0, 0.0, 0.0]
calculator1_1Display.ColorArrayName = ['POINTS', 'Result']
calculator1_1Display.LookupTable = resultLUT
calculator1_1Display.OSPRayScaleArray = 'Result'
calculator1_1Display.OSPRayScaleFunction = 'PiecewiseFunction'
calculator1_1Display.SelectOrientationVectors = 'LIN3AS'
calculator1_1Display.ScaleFactor = 0.1
calculator1_1Display.SelectScaleArray = 'Result'
calculator1_1Display.GlyphType = 'Arrow'
calculator1_1Display.PolarAxes = 'PolarAxesRepresentation'
calculator1_1Display.ScalarOpacityUnitDistance = 0.03170162391333772
calculator1_1Display.GaussianRadius = 0.05
calculator1_1Display.SetScaleArray = ['POINTS', 'Result']
calculator1_1Display.ScaleTransferFunction = 'PiecewiseFunction'
calculator1_1Display.OpacityArray = ['POINTS', 'Result']
calculator1_1Display.OpacityTransferFunction = 'PiecewiseFunction'

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
calculator1_1Display.PolarAxes.PolarAxisTitleColor = [0.0, 0.0, 0.0]
calculator1_1Display.PolarAxes.PolarAxisLabelColor = [0.0, 0.0, 0.0]
calculator1_1Display.PolarAxes.LastRadialAxisTextColor = [0.0, 0.0, 0.0]
calculator1_1Display.PolarAxes.SecondaryRadialAxesTextColor = [0.0, 0.0, 0.0]

# hide data in view
Hide(calculator1, renderView1)

# show color bar/color legend
calculator1_1Display.SetScalarBarVisibility(renderView1, True)

# set active source
SetActiveSource(calculator1)

# set active source
SetActiveSource(calculator1_1)

# set active source
SetActiveSource(calculator1)

# set active source
SetActiveSource(calculator1_1)

# set active source
SetActiveSource(calculator1)

# show data in view
calculator1Display = Show(calculator1, renderView1)

# show color bar/color legend
calculator1Display.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(calculator1_1, renderView1)

# set active source
SetActiveSource(contour1)

# show data in view
contour1Display = Show(contour1, renderView1)

# hide data in view
Hide(calculator1, renderView1)

# hide data in view
Hide(contour1, renderView1)

# set active source
SetActiveSource(calculator1)

# set active source
SetActiveSource(calculator1)

# show data in view
calculator1Display = Show(calculator1, renderView1)

# show color bar/color legend
calculator1Display.SetScalarBarVisibility(renderView1, True)

# reset view to fit data
renderView1.ResetCamera()

# Rescale transfer function
resultLUT.RescaleTransferFunction(-1000000.0, 1000000.0)

# Rescale transfer function
resultPWF.RescaleTransferFunction(-1000000.0, 1000000.0)

# set active source
SetActiveSource(calculator1_1)

# set active source
SetActiveSource(calculator1)

# destroy calculator1_1
Delete(calculator1_1)
del calculator1_1

# set active source
SetActiveSource(figuresxdmf)

# create a new 'Calculator'
calculator1_1 = Calculator(Input=figuresxdmf)
calculator1_1.Function = ''

# Properties modified on calculator1_1
calculator1_1.Function = ''

# show data in view
calculator1_1Display = Show(calculator1_1, renderView1)
# trace defaults for the display properties.
calculator1_1Display.Representation = 'Surface'
calculator1_1Display.AmbientColor = [0.0, 0.0, 0.0]
calculator1_1Display.ColorArrayName = ['POINTS', 'LIN3R1']
calculator1_1Display.LookupTable = lIN3R1LUT
calculator1_1Display.OSPRayScaleArray = 'LIN3R1'
calculator1_1Display.OSPRayScaleFunction = 'PiecewiseFunction'
calculator1_1Display.SelectOrientationVectors = 'LIN3AS'
calculator1_1Display.ScaleFactor = 0.1
calculator1_1Display.SelectScaleArray = 'LIN3R1'
calculator1_1Display.GlyphType = 'Arrow'
calculator1_1Display.PolarAxes = 'PolarAxesRepresentation'
calculator1_1Display.ScalarOpacityUnitDistance = 0.03170162391333772
calculator1_1Display.GaussianRadius = 0.05
calculator1_1Display.SetScaleArray = ['POINTS', 'LIN3R1']
calculator1_1Display.ScaleTransferFunction = 'PiecewiseFunction'
calculator1_1Display.OpacityArray = ['POINTS', 'LIN3R1']
calculator1_1Display.OpacityTransferFunction = 'PiecewiseFunction'

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
calculator1_1Display.PolarAxes.PolarAxisTitleColor = [0.0, 0.0, 0.0]
calculator1_1Display.PolarAxes.PolarAxisLabelColor = [0.0, 0.0, 0.0]
calculator1_1Display.PolarAxes.LastRadialAxisTextColor = [0.0, 0.0, 0.0]
calculator1_1Display.PolarAxes.SecondaryRadialAxesTextColor = [0.0, 0.0, 0.0]

# hide data in view
Hide(figuresxdmf, renderView1)

# show color bar/color legend
calculator1_1Display.SetScalarBarVisibility(renderView1, True)

# rename source object
RenameSource('ERR36', calculator1_1)

# Properties modified on calculator1_1
calculator1_1.Function = 'NLB3R6-'

# Properties modified on calculator1_1
calculator1_1.Function = 'NLB3R6-NLB3R3'

# hide data in view
Hide(calculator1, renderView1)

# Properties modified on calculator1_1
calculator1_1.ResultArrayName = 'ERR'

# set scalar coloring
ColorBy(calculator1_1Display, ('POINTS', 'ERR'))

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(lIN3R1LUT, renderView1)

# rescale color and/or opacity maps used to include current data range
calculator1_1Display.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
calculator1_1Display.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'ERR'
eRRLUT = GetColorTransferFunction('ERR')
eRRLUT.RGBPoints = [-455564544.0, 0.231373, 0.298039, 0.752941, 932660352.0, 0.865003, 0.865003, 0.865003, 2320885248.0, 0.705882, 0.0156863, 0.14902]
eRRLUT.ScalarRangeInitialized = 1.0

# Rescale transfer function
eRRLUT.RescaleTransferFunction(-1000000.0, 1000000.0)

# get opacity transfer function/opacity map for 'ERR'
eRRPWF = GetOpacityTransferFunction('ERR')
eRRPWF.Points = [-455564544.0, 0.0, 0.5, 0.0, 2320885248.0, 1.0, 0.5, 0.0]
eRRPWF.ScalarRangeInitialized = 1

# Rescale transfer function
eRRPWF.RescaleTransferFunction(-1000000.0, 1000000.0)

# set active source
SetActiveSource(calculator1)

# show data in view
calculator1Display = Show(calculator1, renderView1)

# show color bar/color legend
calculator1Display.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(calculator1, renderView1)

# show data in view
calculator1Display = Show(calculator1, renderView1)

# show color bar/color legend
calculator1Display.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(calculator1_1, renderView1)

# set active source
SetActiveSource(calculator1_1)

# show data in view
calculator1_1Display = Show(calculator1_1, renderView1)

# show color bar/color legend
calculator1_1Display.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(calculator1, renderView1)

# Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
eRRLUT.ApplyPreset('Blue to Red Rainbow', True)

# set active source
SetActiveSource(calculator1)

# show data in view
calculator1Display = Show(calculator1, renderView1)

# show color bar/color legend
calculator1Display.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(calculator1_1, renderView1)

# set active source
SetActiveSource(calculator1_1)

# set active source
SetActiveSource(calculator1_1)

# show data in view
calculator1_1Display = Show(calculator1_1, renderView1)

# show color bar/color legend
calculator1_1Display.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(calculator1, renderView1)

# create a new 'Calculator'
calculator1_2 = Calculator(Input=calculator1_1)
calculator1_2.Function = ''

# Properties modified on calculator1_2
calculator1_2.Function = ''

# show data in view
calculator1_2Display = Show(calculator1_2, renderView1)
# trace defaults for the display properties.
calculator1_2Display.Representation = 'Surface'
calculator1_2Display.AmbientColor = [0.0, 0.0, 0.0]
calculator1_2Display.ColorArrayName = ['POINTS', 'ERR']
calculator1_2Display.LookupTable = eRRLUT
calculator1_2Display.OSPRayScaleArray = 'ERR'
calculator1_2Display.OSPRayScaleFunction = 'PiecewiseFunction'
calculator1_2Display.SelectOrientationVectors = 'ERR'
calculator1_2Display.ScaleFactor = 0.1
calculator1_2Display.SelectScaleArray = 'ERR'
calculator1_2Display.GlyphType = 'Arrow'
calculator1_2Display.PolarAxes = 'PolarAxesRepresentation'
calculator1_2Display.ScalarOpacityUnitDistance = 0.03170162391333772
calculator1_2Display.GaussianRadius = 0.05
calculator1_2Display.SetScaleArray = ['POINTS', 'ERR']
calculator1_2Display.ScaleTransferFunction = 'PiecewiseFunction'
calculator1_2Display.OpacityArray = ['POINTS', 'ERR']
calculator1_2Display.OpacityTransferFunction = 'PiecewiseFunction'

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
calculator1_2Display.PolarAxes.PolarAxisTitleColor = [0.0, 0.0, 0.0]
calculator1_2Display.PolarAxes.PolarAxisLabelColor = [0.0, 0.0, 0.0]
calculator1_2Display.PolarAxes.LastRadialAxisTextColor = [0.0, 0.0, 0.0]
calculator1_2Display.PolarAxes.SecondaryRadialAxesTextColor = [0.0, 0.0, 0.0]

# hide data in view
Hide(calculator1_1, renderView1)

# show color bar/color legend
calculator1_2Display.SetScalarBarVisibility(renderView1, True)

# set active source
SetActiveSource(calculator1_1)

# hide data in view
Hide(calculator1_2, renderView1)

# show data in view
calculator1_1Display = Show(calculator1_1, renderView1)

# show color bar/color legend
calculator1_1Display.SetScalarBarVisibility(renderView1, True)

# destroy calculator1_2
Delete(calculator1_2)
del calculator1_2

# set active source
SetActiveSource(figuresxdmf)

# create a new 'Calculator'
calculator1_2 = Calculator(Input=figuresxdmf)
calculator1_2.Function = ''

# Properties modified on calculator1_2
calculator1_2.Function = ''

# show data in view
calculator1_2Display = Show(calculator1_2, renderView1)
# trace defaults for the display properties.
calculator1_2Display.Representation = 'Surface'
calculator1_2Display.AmbientColor = [0.0, 0.0, 0.0]
calculator1_2Display.ColorArrayName = ['POINTS', 'LIN3R1']
calculator1_2Display.LookupTable = lIN3R1LUT
calculator1_2Display.OSPRayScaleArray = 'LIN3R1'
calculator1_2Display.OSPRayScaleFunction = 'PiecewiseFunction'
calculator1_2Display.SelectOrientationVectors = 'LIN3AS'
calculator1_2Display.ScaleFactor = 0.1
calculator1_2Display.SelectScaleArray = 'LIN3R1'
calculator1_2Display.GlyphType = 'Arrow'
calculator1_2Display.PolarAxes = 'PolarAxesRepresentation'
calculator1_2Display.ScalarOpacityUnitDistance = 0.03170162391333772
calculator1_2Display.GaussianRadius = 0.05
calculator1_2Display.SetScaleArray = ['POINTS', 'LIN3R1']
calculator1_2Display.ScaleTransferFunction = 'PiecewiseFunction'
calculator1_2Display.OpacityArray = ['POINTS', 'LIN3R1']
calculator1_2Display.OpacityTransferFunction = 'PiecewiseFunction'

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
calculator1_2Display.PolarAxes.PolarAxisTitleColor = [0.0, 0.0, 0.0]
calculator1_2Display.PolarAxes.PolarAxisLabelColor = [0.0, 0.0, 0.0]
calculator1_2Display.PolarAxes.LastRadialAxisTextColor = [0.0, 0.0, 0.0]
calculator1_2Display.PolarAxes.SecondaryRadialAxesTextColor = [0.0, 0.0, 0.0]

# hide data in view
Hide(figuresxdmf, renderView1)

# show color bar/color legend
calculator1_2Display.SetScalarBarVisibility(renderView1, True)

# rename source object
RenameSource('T_minus_Tlinear', calculator1_2)

# hide data in view
Hide(calculator1_1, renderView1)

# Properties modified on calculator1_2
calculator1_2.ResultArrayName = 'DIF'

# Properties modified on calculator1_2
calculator1_2.Function = 'NLB3R6-LIN3R6'

# set scalar coloring
ColorBy(calculator1_2Display, ('POINTS', 'DIF'))

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(lIN3R1LUT, renderView1)

# rescale color and/or opacity maps used to include current data range
calculator1_2Display.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
calculator1_2Display.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'DIF'
dIFLUT = GetColorTransferFunction('DIF')
dIFLUT.RGBPoints = [-15834838272.0, 0.231373, 0.298039, 0.752941, -7911716600.0, 0.865003, 0.865003, 0.865003, 11405072.0, 0.705882, 0.0156863, 0.14902]
dIFLUT.ScalarRangeInitialized = 1.0

# Rescale transfer function
dIFLUT.RescaleTransferFunction(-10000000.0, 10000000.0)

# get opacity transfer function/opacity map for 'DIF'
dIFPWF = GetOpacityTransferFunction('DIF')
dIFPWF.Points = [-15834838272.0, 0.0, 0.5, 0.0, 11405072.0, 1.0, 0.5, 0.0]
dIFPWF.ScalarRangeInitialized = 1

# Rescale transfer function
dIFPWF.RescaleTransferFunction(-10000000.0, 10000000.0)

# reset view to fit data
renderView1.ResetCamera()

# Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
dIFLUT.ApplyPreset('Blue to Red Rainbow', True)

# Properties modified on dIFLUT
dIFLUT.UseBelowRangeColor = 1

# Properties modified on dIFLUT
dIFLUT.UseAboveRangeColor = 1

# Properties modified on calculator1_2
calculator1_2.Function = 'NLS3R6-LIN3R6'

# Rescale transfer function
dIFLUT.RescaleTransferFunction(-50000000.0, 50000000.0)

# Rescale transfer function
dIFPWF.RescaleTransferFunction(-50000000.0, 50000000.0)