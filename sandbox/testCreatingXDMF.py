from dolfin import *
import SolutionImporter
from VNotchSolverPowerLaw import VNotchSolverPowerLaw

inputfile6 = '/usr/nobackup2/kulvv1am/POWERLAWMESHES/1/V/FEniCS_A4NLMIN_refinement6.txt'
mesh_xml6 = '/usr/nobackup2/kulvv1am/POWERLAWMESHES/1/V/A_refinement6_dolfin.xml'

inputfile = '/usr/nobackup2/kulvv1am/POWERLAWMESHES/1/V/FEniCS_A4L_refinement6.txt'
mesh_xml = '/usr/nobackup2/kulvv1am/POWERLAWMESHES/1/V/A_refinement6_dolfin.xml'
#inputfile5 = '/usr/nobackup2/kulvv1am/POWERLAWMESHES/1/V/FEniCS_A4NLMIN_refinement5.txt'
#mesh_xml5 = '/usr/nobackup2/kulvv1am/POWERLAWMESHES/1/V/A_refinement5_dolfin.xml'
#mesh5 = dolfin.Mesh(mesh_xml5)
mesh6 = dolfin.Mesh(mesh_xml6)

functionSpace6 = dolfin.FunctionSpace(mesh6, 'Lagrange', 2)
#functionSpace5 = dolfin.FunctionSpace(mesh5, 'Lagrange', 2)

importer = SolutionImporter.SolutionImporter()
solution6 = importer.importSolution(functionSpace6, inputfile6)
solution6 = importer.importSolution(functionSpace6, inputfile)
solver = VNotchSolverPowerLaw(False, 25*10**9, 0.5*10**9, 5, 25*10**9)
#xf = XDMFFile("tst.xdmf")
V = FunctionSpace(mesh6, 'Lagrange', 2)
solver.initMesh(mesh6, 2)
tas = solver.gettAsymptoticLinear(solution6,  True, 'TAS')
xf = XDMFFile("tst.xdmf")
t13 = solver.gett13(solution6, 'T13_A4NLMIN_refinement6')
t23 = solver.gett23(solution6, 'T23_A4NLMIN_refinement6')
e13 = solver.gete13(solution6, 'E13_A4NLMIN_refinement6')
e23 = solver.gete23(solution6, 'E23_A4NLMIN_refinement6')
xf.write(t13, 0.0)
xf.write(t23, 0.0)
xf.write(e13, 0.0)
xf.write(e23, 0.0)
xf.write(tas, 0.0)
#solver.initMesh(mesh5, 2)
#xf.Encoding_ASCII = 1 ... nefunguje!
parameters['allow_extrapolation'] = True
solution5 = importer.importSolution(functionSpace5, inputfile5)
t13 = solver.gett13(solution5, 'T13_A4NLMIN_refinement5')
t23 = solver.gett23(solution5, 'T23_A4NLMIN_refinement5')
e13 = solver.gete13(solution5, 'E13_A4NLMIN_refinement5')
e23 = solver.gete23(solution5, 'E23_A4NLMIN_refinement5')
#parameters['allow_extrapolation'] = False
#t13 = project(t13, V)
#t23 = project(t23, V)
#e13 = project(e13, V)
#e23 = project(e23, V)
xf.write(t13, 0.0)
xf.write(t23, 0.0)
xf.write(e13, 0.0)
xf.write(e23, 0.0)
#type(t13)
#t13.name()
#valueat - https://fenicsproject.org/qa/980/how-to-extract-value-of-function-in-current-point

ufunc = dolfin.interpolate(dolfin.Expression('0', degree=1), V)
        # ... without degree=1 results in warnings, jde o stupen polynomu pro ktery je numericka integrace vyrazu na danem elementu presna, tady s klidem 1
        u = ufunc.vector()
        if self.DEBUG:
            totalError = 0.0;
            for i in range(0, dofs):
                u[dofindices[i]] = matlabArray[matlabindices[i], 0][0][geomdim];
                totalError = totalError + (matlabArray[matlabindices[i], 0][0][0] - dof_coordinates[dofindices[i]][0][0][0])**2 + (matlabArray[matlabindices[i], 0][0][1] - dof_coordinates[dofindices[i]][0][0][1])**2
            assert(totalError==0)
        else:
            for i in range(0, dofs):
                u[dofindices[i]] = matlabArray[matlabindices[i], 0][0][geomdim]
        return(ufunc)



def estimateAlphaLocally(LL, r):
    geomdim = 2 
    dofcoord = V.tabulate_dof_coordinates()
    dofcoord.reshape(-1, geomdim)
    dofshifted = np.subtract(dofcoord, np.array([LL, 0.0]))
    ind = (LA.norm(x, axis=1) < r).nonzero()
    maxind = dofcoord[ind,1].argmax()#Should be at the edge
    X = dofcoord[maxind]
    alphaest = 2*np.tan((X[1]-LL)/X[0])
    return(alphaest)

