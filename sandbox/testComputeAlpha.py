from dolfin import *
import SolutionImporter
from VNotchSolverPowerLaw import VNotchSolverPowerLaw
import numpy as np
from numpy import linalg as LA


inputfile6 = '/usr/nobackup2/kulvv1am/POWERLAWMESHES/1/V/FEniCS_A4NLMIN_refinement6.txt'
mesh_xml6 = '/usr/nobackup2/kulvv1am/POWERLAWMESHES/1/V/A_refinement6_dolfin.xml'
mesh = dolfin.Mesh(mesh_xml6)
V = FunctionSpace(mesh, 'Lagrange', 2)
importer = SolutionImporter.SolutionImporter()
solution6 = importer.importSolution(V, inputfile6)
solver = VNotchSolverPowerLaw(False, 25*10**9, 0.5*10**9, 5, 25*10**9)
solver.initMesh(mesh, 2)

def estimateAlphaLocally(LL, r):
    geomdim = mesh.geometry().dim()
    dofcoord = V.tabulate_dof_coordinates()
    dofs = V.dim()
    dofcoord.resize(dofs, geomdim)
    dofshifted = np.subtract(dofcoord, np.array([0.0, LL]))
    ind = (LA.norm(dofshifted, axis=1) < r).nonzero()
    maxind = dofcoord[ind,1].argmax()#Should be at the edge
    X = dofcoord[maxind]
    alphaest = 2*np.arctan((X[1]-LL)/X[0])
    return(alphaest)

def estimateAlpha():
    dofcoord = V.tabulate_dof_coordinates()
    geomdim = mesh.geometry().dim()
    dofs = V.dim()
    dofcoord.resize(dofs, geomdim)
    #Find triangle of lowerleft, upperright, center
    ind = np.logical_and(dofcoord[:,1] < 0.5, dofcoord[:,0]==0).nonzero() #All coordinates along x=0 with y>0.5
    LL = dofcoord[ind,1].max()
    r=0.5-LL
    epsilon=1e-5
    alphaest = estimateAlphaLocally(LL, r)
    while np.absolute(estimateAlphaLocally(LL, r) - estimateAlphaLocally(LL, r/2)) > epsilon:
        r=r/2
		print(estimateAlphaLocally(LL, r))
    alphaest = estimateAlphaLocally(LL, r)
    return(alphaest)

