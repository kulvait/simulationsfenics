from paraview.simple import *

camfac = 0.5
far = camfac
near = 0

def exportMesh(inputFile, outputPng):
	#paraview.simple._DisableFirstRenderCameraReset()
	xxx = PVDReader(FileName=inputFile)
	rv = CreateRenderView('RenderView')
	Render()
	rv.ResetCamera()
	rv.OrientationAxesVisibility = 0
	rv.InteractionMode = '3D'
	rv.ViewSize = [1000, 1000]
	rv.CameraViewUp = [0.0, 1.0, 0.0]
	rv.CameraPosition = [0.5, 0.5, 0.51]
	rv.CameraFocalPoint = [0.5, 0.5, 0]
	rv.CameraViewAngle = 90.0
	#rv.CameraParallelScale = 0.55
	dis = Show(xxx, rv)
	ColorBy(dis, ('POINTS', 'SOLID'))
	dis.SetRepresentationType('Wireframe')
	dis.AmbientColor = [0.0, 0.0, 0.0]
	#rv.ResetCamera(0.35, 0.65, -0.1, 0.9, 0.1, 0.5)
	Render()
	WriteImage(outputPng)
	#SaveScreenshot(outputPng, magnification=1, quality=100, view=rv)

for i in range(6):
	inputFile = '/mnt/r1d1/BACKUP/POWERLAWMESHES/170/VC_0.05/FEniCS_A2L_refinement{}_e23.pvd'.format(i+1)
	outputPng = 'VC150R{}.png'.format(i+1)
	exportMesh(inputFile, outputPng)

for i in range(6):
	inputFile = '/mnt/r1d1/BACKUP/POWERLAWMESHES/1/VO_0.05/FEniCS_A3L_refinement{}_e23.pvd'.format(i+1)
	outputPng = 'VO1R{}.png'.format(i+1)
	exportMesh(inputFile, outputPng)

for i in range(6):
    inputFile = '/mnt/r1d1/BACKUP/POWERLAWMESHES/30/V/FEniCS_A3L_refinement{}_e23.pvd'.format(i+1)
    outputPng = 'V30R{}.png'.format(i+1)
    exportMesh(inputFile, outputPng)

inputFile = '/mnt/r1d1/BACKUP/POWERLAWMESHES/30/VO_0.05/FEniCS_A3L_refinement1_e23.pvd'
outputPng = 'ref1.png'
exportMesh(inputFile, outputPng)
