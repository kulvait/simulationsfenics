# import testLinearRegression as x
DEBUG=True
from dolfin import *
import SolutionImporter
from VNotchSolverPowerLaw import VNotchSolverPowerLaw
import numpy as np
LA = np.linalg

inputfile = '/usr/nobackup2/kulvv1am/POWERLAWMESHES/1/V/FEniCS_A4L_refinement6.txt'
mesh_xml = '/usr/nobackup2/kulvv1am/POWERLAWMESHES/1/V/A_refinement6_dolfin.xml'
mesh = dolfin.Mesh(mesh_xml)

parameters['allow_extrapolation'] = True

V = dolfin.FunctionSpace(mesh, 'Lagrange', 2)
importer = SolutionImporter.SolutionImporter()
solution = importer.importSolution(V, inputfile)
solver = VNotchSolverPowerLaw(False, 25*10**9, 0.5*10**9, 5, 25*10**9,DEBUG)
solver.initMesh(mesh, 2)
tas = solver.gettAsymptoticLinear(solution,  True, 'TAS')
tas13 = solver.gett13AsymptoticLinear(solution, 'T13_LAS', 'T13', tas)
tas23 = solver.gett23AsymptoticLinear(solution, 'T23_LAS', 'T23', tas)
xf = XDMFFile("tst.xdmf")
t13 = solver.gett13(solution, 'T13_L46')
t23 = solver.gett23(solution, 'T23_L46')
e13 = solver.gete13(solution, 'E13_L46')
e23 = solver.gete23(solution, 'E23_L46')
xf.write(t13, 0.0)
xf.write(t23, 0.0)
xf.write(e13, 0.0)
xf.write(e23, 0.0)
xf.write(tas23, 0.0)
xf.write(tas13, 0.0)
xf.write(tas, 0.0)
#solver.initMesh(mesh5, 2)
#xf.Encoding_ASCII = 1 ... nefunguje!
#parameters['allow_extrapolation'] = True
#solution5 = importer.importSolution(functionSpace5, inputfile5)
#t13 = solver.gett13(solution5, 'T13_A4NLMIN_refinement5')
#t23 = solver.gett23(solution5, 'T23_A4NLMIN_refinement5')
#e13 = solver.gete13(solution5, 'E13_A4NLMIN_refinement5')
#e23 = solver.gete23(solution5, 'E23_A4NLMIN_refinement5')
#parameters['allow_extrapolation'] = False
#t13 = project(t13, V)
#t23 = project(t23, V)
#e13 = project(e13, V)
#e23 = project(e23, V)
#xf.write(t13, 0.0)
#xf.write(t23, 0.0)
#xf.write(e13, 0.0)
#xf.write(e23, 0.0)
#type(t13)
#t13.name()
#valueat - https://fenicsproject.org/qa/980/how-to-extract-value-of-function-in-current-point
