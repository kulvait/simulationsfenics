from DirectoryWrapper import DirectoryWrapper
import MaterialModel as m
from dolfin import *
from VNotchSolverPowerLaw import VNotchSolverPowerLaw
from tabulate import tabulate
from math import log10, floor
import copy

models = m.MaterialModel.getModels()
keys = filter(lambda x: x[:2]!='A4', models.keys())
models = {x:models[x] for x in keys}
keys = models.keys()

def round_to_2(x):
	roundplaces =  -int(floor(log10(abs(x*100))))
	roundplaces = max(1, roundplaces)
	rounded = round(100*x, roundplaces)
	return(rounded)

def removeTrailingZeros(x):
	string = '{0:.10f}'.format(x)
	return(string.rstrip('0'))
	
# Remove trailing
for i in string:
  while i[-1] == "0":
    i = i[:-1]
  trailingremoved.append(i)


directory = '/usr/nobackup2/kulvv1am/POWERLAWMESHES/1/V'
dv = DirectoryWrapper(directory)

meshparameters = {}
for i in range(6):
	meshparameters[i] = []
	meshparameters[i].append(dv.getNumberElements(i+1))
	meshparameters[i].append(dv.getDofNumber(i+1))

parameters['allow_extrapolation'] = True
errornorms = {}
for key in keys:
	A6 = dv.getSolution(key, 6, 6)
	norm = dolfin.norm(A6, 'H1')
	errornorms[key] = []
	for j in range(5):
		Aj = dv.getSolution(key, j+1, 6)
		errornorms[key].append(dolfin.errornorm(Aj, A6, 'H1')/norm)

printkeys = ['A1L', 'A1NLMIN',  'A1NLMAX', 'A2NLMIN', 'A2NLMAX', 'A3NLMIN', 'A3NLMAX']
header = ['Elements', 'DOFs']
header = header + printkeys
table = []
for i in range(6):
	row = copy.deepcopy(meshparameters[i])
	row = map(lambda x: '$\\num{{{}}}$'.format(x), row)
	if i != 5:
		for key in printkeys:
			row.append('$\\num{{{}}}$'.format(removeTrailingZeros(round_to_2(errornorms[key][i]))))
	else:
		for key in printkeys:
			row.append('$\\num{0.0}$')
	table.append(row)

print tabulate(table, header, 'latex_booktabs')
transpose = [header]+table
transposedheader = [0,1,2,3,4,5]
print tabulate(zip(*transpose),transposedheader,'latex_booktabs') 

#linkeys = filter(lambda x: models[x].linearmodel, models.keys())
#linnorms = {x:errornorms[x] for x in linkeys}
#print(tabulate(errornorms))

#dv.getMeshParams(6)['hmax']
#dv.getMeshParams(6)['numelm']
