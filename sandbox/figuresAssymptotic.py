from DirectoryWrapper import DirectoryWrapper
from dolfin import *
from MaterialModel import MaterialModel
from VNotchSolverPowerLaw import VNotchSolverPowerLaw
parameters['allow_extrapolation'] = True
directory = '/usr/nobackup2/kulvv1am/POWERLAWMESHES/1/V'
dv = DirectoryWrapper(directory)
models = MaterialModel.getModels()
modelskeys = MaterialModel.keyToModel()
labels = ['A3L', 'A3NLMIN', 'A3NLMAX']
xf = XDMFFile("V1T.xdmf")
for label in labels:
	mesh = dv.getMesh(6)
	model = models[label]
	key = modelskeys[label]
	DEBUG = True
	solver = VNotchSolverPowerLaw(model.linearmodel, model.mu_l, model.tau0, model.qprime, model.mu, DEBUG)
	solver.initMesh(mesh, 2)
	A6 = dv.getSolution(label, 6, 6)
	xf.write(solver.gett13(A6, 'T13{}R6'.format(key)), 0.0)
	xf.write(solver.gett23(A6, 'T23{}R6'.format(key)), 0.0)
	xf.write(solver.gett13Asymptotic(A6,  'T13{}AS'.format(key)),0.0)
	xf.write(solver.gett23Asymptotic(A6,  'T23{}AS'.format(key)),0.0)
	print('Written solution and asymptotic solution {}R6'.format(key))
	print('For model {} the exponent for linear singularity is {} and for nonlinear singularity {}'.format(key, solver.getLinearExponent(A6), solver.getNonlinearExponent(A6)))
	for i in [1, 2, 3, 4, 5]:
		Ai = dv.getSolution(label, i, 6)
		xf.write(solver.gett13(Ai, 'T13{}R{}'.format(key, i)), 0.0)
		xf.write(solver.gett23(Ai, 'T23{}R{}'.format(key, i)), 0.0)
		print('Written solution {}R{}'.format(key, i))

	


import SolutionImporter

inputfile6 = '/usr/nobackup2/kulvv1am/POWERLAWMESHES/1/V/FEniCS_A4NLMIN_refinement6.txt'
mesh_xml6 = '/usr/nobackup2/kulvv1am/POWERLAWMESHES/1/V/A_refinement6_dolfin.xml'

inputfile = '/usr/nobackup2/kulvv1am/POWERLAWMESHES/1/V/FEniCS_A4L_refinement6.txt'
mesh_xml = '/usr/nobackup2/kulvv1am/POWERLAWMESHES/1/V/A_refinement6_dolfin.xml'
#inputfile5 = '/usr/nobackup2/kulvv1am/POWERLAWMESHES/1/V/FEniCS_A4NLMIN_refinement5.txt'
#mesh_xml5 = '/usr/nobackup2/kulvv1am/POWERLAWMESHES/1/V/A_refinement5_dolfin.xml'
#mesh5 = dolfin.Mesh(mesh_xml5)
mesh6 = dolfin.Mesh(mesh_xml6)

functionSpace6 = dolfin.FunctionSpace(mesh6, 'Lagrange', 2)
#functionSpace5 = dolfin.FunctionSpace(mesh5, 'Lagrange', 2)

importer = SolutionImporter.SolutionImporter()
solution6 = importer.importSolution(functionSpace6, inputfile6)
solution6 = importer.importSolution(functionSpace6, inputfile)
solver = VNotchSolverPowerLaw(False, 25*10**9, 0.5*10**9, 5, 25*10**9)
#xf = XDMFFile("tst.xdmf")
V = FunctionSpace(mesh6, 'Lagrange', 2)
solver.initMesh(mesh6, 2)
tas = solver.gettAsymptoticLinear(solution6,  True, 'TAS')
xf = XDMFFile("tst.xdmf")
t13 = solver.gett13(solution6, 'T13_A4NLMIN_refinement6')
t23 = solver.gett23(solution6, 'T23_A4NLMIN_refinement6')
e13 = solver.gete13(solution6, 'E13_A4NLMIN_refinement6')
e23 = solver.gete23(solution6, 'E23_A4NLMIN_refinement6')
xf.write(t13, 0.0)
xf.write(t23, 0.0)
xf.write(e13, 0.0)
xf.write(e23, 0.0)
xf.write(tas, 0.0)
#solver.initMesh(mesh5, 2)
#xf.Encoding_ASCII = 1 ... nefunguje!
parameters['allow_extrapolation'] = True
solution5 = importer.importSolution(functionSpace5, inputfile5)
t13 = solver.gett13(solution5, 'T13_A4NLMIN_refinement5')
t23 = solver.gett23(solution5, 'T23_A4NLMIN_refinement5')
e13 = solver.gete13(solution5, 'E13_A4NLMIN_refinement5')
e23 = solver.gete23(solution5, 'E23_A4NLMIN_refinement5')
#parameters['allow_extrapolation'] = False
#t13 = project(t13, V)
#t23 = project(t23, V)
#e13 = project(e13, V)
#e23 = project(e23, V)
xf.write(t13, 0.0)
xf.write(t23, 0.0)
xf.write(e13, 0.0)
xf.write(e23, 0.0)
#type(t13)
#t13.name()
#valueat - https://fenicsproject.org/qa/980/how-to-extract-value-of-function-in-current-point

ufunc = dolfin.interpolate(dolfin.Expression('0', degree=1), V)
        # ... without degree=1 results in warnings, jde o stupen polynomu pro ktery je numericka integrace vyrazu na danem elementu presna, tady s klidem 1
        u = ufunc.vector()
        if self.DEBUG:
            totalError = 0.0;
            for i in range(0, dofs):
                u[dofindices[i]] = matlabArray[matlabindices[i], 0][0][geomdim];
                totalError = totalError + (matlabArray[matlabindices[i], 0][0][0] - dof_coordinates[dofindices[i]][0][0][0])**2 + (matlabArray[matlabindices[i], 0][0][1] - dof_coordinates[dofindices[i]][0][0][1])**2
            assert(totalError==0)
        else:
            for i in range(0, dofs):
                u[dofindices[i]] = matlabArray[matlabindices[i], 0][0][geomdim]
        return(ufunc)



def estimateAlphaLocally(LL, r):
    geomdim = 2 
    dofcoord = V.tabulate_dof_coordinates()
    dofcoord.reshape(-1, geomdim)
    dofshifted = np.subtract(dofcoord, np.array([LL, 0.0]))
    ind = (LA.norm(x, axis=1) < r).nonzero()
    maxind = dofcoord[ind,1].argmax()#Should be at the edge
    X = dofcoord[maxind]
    alphaest = 2*np.tan((X[1]-LL)/X[0])
    return(alphaest)

from DirectoryWrapper import DirectoryWrapper
import MaterialModel as m
from dolfin import *
from VNotchSolverPowerLaw import VNotchSolverPowerLaw
from tabulate import tabulate
from math import log10, floor
import copy

models = m.MaterialModel.getModels()
keys = filter(lambda x: x[:2]!='A4', models.keys())
models = {x:models[x] for x in keys}
keys = models.keys()

def round_to_2(x):
	roundplaces =  -int(floor(log10(abs(x*100))))
	roundplaces = max(1, roundplaces)
	rounded = round(100*x, roundplaces)
	return(rounded)

def removeTrailingZeros(x):
	string = '{0:.10f}'.format(x)
	return(string.rstrip('0'))
	
# Remove trailing
for i in string:
  while i[-1] == "0":
    i = i[:-1]
  trailingremoved.append(i)


directory = '/usr/nobackup2/kulvv1am/POWERLAWMESHES/1/V'
dv = DirectoryWrapper(directory)

meshparameters = {}
for i in range(6):
	meshparameters[i] = []
	meshparameters[i].append(dv.getNumberElements(i+1))
	meshparameters[i].append(dv.getDofNumber(i+1))

parameters['allow_extrapolation'] = True
errornorms = {}
for key in keys:
	A6 = dv.getSolution(key, 6, 6)
	norm = dolfin.norm(A6, 'H1')
	errornorms[key] = []
	for j in range(5):
		Aj = dv.getSolution(key, j+1, 6)
		errornorms[key].append(dolfin.errornorm(Aj, A6, 'H1')/norm)

printkeys = ['A1L', 'A1NLMIN',  'A1NLMAX', 'A2NLMIN', 'A2NLMAX', 'A3NLMIN', 'A3NLMAX']
header = ['Elements', 'DOFs']
header = header + printkeys
table = []
for i in range(6):
	row = copy.deepcopy(meshparameters[i])
	row = map(lambda x: '$\\num{{{}}}$'.format(x), row)
	if i != 5:
		for key in printkeys:
			row.append('$\\num{{{}}}$'.format(removeTrailingZeros(round_to_2(errornorms[key][i]))))
	else:
		for key in printkeys:
			row.append('$\\num{0.0}$')
	table.append(row)

print tabulate(table, header, 'latex_booktabs')
transpose = [header]+table
transposedheader = [0,1,2,3,4,5]
print tabulate(zip(*transpose),transposedheader,'latex_booktabs') 

#linkeys = filter(lambda x: models[x].linearmodel, models.keys())
#linnorms = {x:errornorms[x] for x in linkeys}
#print(tabulate(errornorms))

#dv.getMeshParams(6)['hmax']
#dv.getMeshParams(6)['numelm']
