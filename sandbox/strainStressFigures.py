from DirectoryWrapper import DirectoryWrapper
from dolfin import *
from MaterialModel import MaterialModel
from VNotchSolverPowerLaw import VNotchSolverPowerLaw
parameters['allow_extrapolation'] = True
directory = '/usr/nobackup2/kulvv1am/POWERLAWMESHES/1/V'
dv = DirectoryWrapper(directory)
models = MaterialModel.getModels()
modelskeys = MaterialModel.keyToModel()
labels = ['A3L', 'A3NLMIN', 'A3NLMAX']
xf = XDMFFile("V1XX.xdmf")
for label in labels:
	mesh = dv.getMesh(6)
	model = models[label]
	key = modelskeys[label]
	DEBUG = True
	solver = VNotchSolverPowerLaw(model.linearmodel, model.mu_l, model.tau0, model.qprime, model.mu, DEBUG)
	solver.initMesh(mesh, 2)
	A6 = dv.getSolution(label, 6, 6)
	xf.write(solver.gett13(A6, 'T13{}R6'.format(key)), 0.0)
	xf.write(solver.gett23(A6, 'T23{}R6'.format(key)), 0.0)
	xf.write(solver.gete13(A6, 'E13{}R6'.format(key)), 0.0)
	xf.write(solver.gete23(A6, 'E23{}R6'.format(key)), 0.0)
	xf.write(solver.gett13Asymptotic(A6,  'T13{}AS'.format(key)),0.0)
	xf.write(solver.gett23Asymptotic(A6,  'T23{}AS'.format(key)),0.0)
	print('Written solution and asymptotic solution {}R6'.format(key))
	print('For model {} the exponent for linear singularity is {} and for nonlinear singularity {}'.format(key, solver.getLinearExponent(A6), solver.getNonlinearExponent(A6)))
	for i in [5]:
		Ai = dv.getSolution(label, i, 6)
		xf.write(solver.gett13(Ai, 'T13{}R{}'.format(key, i)), 0.0)
		xf.write(solver.gett23(Ai, 'T23{}R{}'.format(key, i)), 0.0)
		xf.write(solver.gete13(Ai, 'E13{}R{}'.format(key, i)), 0.0)
		xf.write(solver.gete23(Ai, 'E23{}R{}'.format(key, i)), 0.0)
		print('Written solution {}R{}'.format(key, i))
