from paraview.simple import *
renderView1_1 = CreateView('RenderView')
renderView1_1.ViewSize = [854, 840]
renderView1_1.AnnotationColor = [0.0, 0.0, 0.0]
renderView1_1.AxesGrid = 'GridAxes3DActor'
renderView1_1.OrientationAxesLabelColor = [0.0, 0.0, 0.0]
renderView1_1.OrientationAxesOutlineColor = [0.0, 0.0, 0.0]
renderView1_1.StereoType = 0
renderView1_1.Background = [1.0, 1.0, 1.0]
renderView1_1.AxesGrid.XTitleColor = [0.0, 0.0, 0.0]
renderView1_1.AxesGrid.YTitleColor = [0.0, 0.0, 0.0]
renderView1_1.AxesGrid.ZTitleColor = [0.0, 0.0, 0.0]
renderView1_1.AxesGrid.GridColor = [0.0, 0.0, 0.0]
renderView1_1.AxesGrid.XLabelColor = [0.0, 0.0, 0.0]
renderView1_1.AxesGrid.YLabelColor = [0.0, 0.0, 0.0]
renderView1_1.AxesGrid.ZLabelColor = [0.0, 0.0, 0.0]

# get layout
layout1 = GetLayout()

# place view in the layout
layout1.AssignView(0, renderView1_1)
fEniCS_A1NLMAX_refinement2_e13000000vtu = XMLUnstructuredGridReader(FileName=['/mnt/r1d1/BACKUP/POWERLAWMESHES/30/VO_0.05/FEniCS_A1NLMAX_refinement2_e13000000.vtu'])
fEniCS_A1NLMAX_refinement2_e13000000vtu.PointArrayStatus = ['E13']
e13LUT = GetColorTransferFunction('E13')

# show data in view
fEniCS_A1NLMAX_refinement2_e13000000vtuDisplay = Show(fEniCS_A1NLMAX_refinement2_e13000000vtu, renderView1_1)
# trace defaults for the display properties.
fEniCS_A1NLMAX_refinement2_e13000000vtuDisplay.Representation = 'Surface'
fEniCS_A1NLMAX_refinement2_e13000000vtuDisplay.AmbientColor = [0.0, 0.0, 0.0]
fEniCS_A1NLMAX_refinement2_e13000000vtuDisplay.ColorArrayName = ['POINTS', 'E13']
fEniCS_A1NLMAX_refinement2_e13000000vtuDisplay.LookupTable = e13LUT
fEniCS_A1NLMAX_refinement2_e13000000vtuDisplay.OSPRayScaleArray = 'E13'
fEniCS_A1NLMAX_refinement2_e13000000vtuDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
fEniCS_A1NLMAX_refinement2_e13000000vtuDisplay.SelectOrientationVectors = 'E13'
fEniCS_A1NLMAX_refinement2_e13000000vtuDisplay.ScaleFactor = 0.1
fEniCS_A1NLMAX_refinement2_e13000000vtuDisplay.SelectScaleArray = 'E13'
fEniCS_A1NLMAX_refinement2_e13000000vtuDisplay.GlyphType = 'Arrow'
fEniCS_A1NLMAX_refinement2_e13000000vtuDisplay.PolarAxes = 'PolarAxesRepresentation'
fEniCS_A1NLMAX_refinement2_e13000000vtuDisplay.ScalarOpacityUnitDistance = 0.08054941707858616
fEniCS_A1NLMAX_refinement2_e13000000vtuDisplay.GaussianRadius = 0.05
fEniCS_A1NLMAX_refinement2_e13000000vtuDisplay.SetScaleArray = ['POINTS', 'E13']
fEniCS_A1NLMAX_refinement2_e13000000vtuDisplay.ScaleTransferFunction = 'PiecewiseFunction'
fEniCS_A1NLMAX_refinement2_e13000000vtuDisplay.OpacityArray = ['POINTS', 'E13']
fEniCS_A1NLMAX_refinement2_e13000000vtuDisplay.OpacityTransferFunction = 'PiecewiseFunction'

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
fEniCS_A1NLMAX_refinement2_e13000000vtuDisplay.PolarAxes.PolarAxisTitleColor = [0.0, 0.0, 0.0]
fEniCS_A1NLMAX_refinement2_e13000000vtuDisplay.PolarAxes.PolarAxisLabelColor = [0.0, 0.0, 0.0]
fEniCS_A1NLMAX_refinement2_e13000000vtuDisplay.PolarAxes.LastRadialAxisTextColor = [0.0, 0.0, 0.0]
fEniCS_A1NLMAX_refinement2_e13000000vtuDisplay.PolarAxes.SecondaryRadialAxesTextColor = [0.0, 0.0, 0.0]

# reset view to fit data
renderView1_1.ResetCamera()

#changing interaction mode based on data extents
renderView1_1.InteractionMode = '2D'
renderView1_1.CameraPosition = [0.5, 0.5, 10000.0]
renderView1_1.CameraFocalPoint = [0.5, 0.5, 0.0]

# show color bar/color legend
fEniCS_A1NLMAX_refinement2_e13000000vtuDisplay.SetScalarBarVisibility(renderView1_1, True)

# change representation type
fEniCS_A1NLMAX_refinement2_e13000000vtuDisplay.SetRepresentationType('Wireframe')

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(e13LUT, renderView1_1)

# Properties modified on renderView1_1
renderView1_1.OrientationAxesVisibility = 0

# current camera placement for renderView1_1
renderView1_1.InteractionMode = '2D'
renderView1_1.CameraPosition = [0.5, 0.5, 10000.0]
renderView1_1.CameraFocalPoint = [0.5, 0.5, 0.0]
renderView1_1.CameraParallelScale = 0.7071067811865476

# save screenshot
SaveScreenshot('xxx.png', magnification=1, quality=100, view=renderView1_1)
