'''
Created on May 14, 2014

@author: Vojtech Kulvait
'''
import struct

class SolutionExporter(object):
    def exportSolution(self, u, fileName):
        V = u.function_space();
        mesh = V.mesh()
        geomdim = mesh.geometry().dim()
        dofs = V.dim()
        #dof_coordinates = V.dofmap().tabulate_all_coordinates(mesh)
        #this results to the AttributeError: 'GenericDofMap' object has no attribute 'tabulate_all_coordinates' in new version
        dof_coordinates = V.tabulate_dof_coordinates()
        dof_coordinates.resize((dofs, geomdim))
        vec =  u.vector();
        fout = open(fileName, 'wb')
        for i in range(0, dofs):
            for j in range(0, geomdim):
                fout.write(struct.pack('d', dof_coordinates[i, j]))
            fout.write(struct.pack('d', vec[i]))
        fout.close() 
