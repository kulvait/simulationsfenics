'''
Created on May 12, 2014

@author: Vojtech Kulvait
'''
import struct
import numpy
import dolfin

class SolutionImporter(object):

    def __init__(self, DEBUG=None):
        if DEBUG is None:
            self.DEBUG = False
        else:
            self.DEBUG = DEBUG

    def importSolution(self, V, matlabFile):
        #V is instance of class 'dolfin.functions.functionspace.FunctionSpace'
        #Matlab file structure is (x, y, val, x, y, val, ...)
        mesh = V.mesh()
        geomdim = mesh.geometry().dim()
        dofs = V.dim()
        #dof_coordinates = V.dofmap().tabulate_all_coordinates(mesh)
        dof_coordinates = V.tabulate_dof_coordinates()
        dof_coordinates.resize((dofs, geomdim))
        t = []
        order = []
        for i in range(0, geomdim):
            t.append(('{0}'.format(i), 'float64'))
            order.append('{0}'.format(i))
        
        dof_coordinates.dtype = t
        dofindices = numpy.argsort(dof_coordinates, axis=0, order=order)
        t.append(('{0}'.format('u'), 'float64'))
        matlabArray = self.importMatlabArray(dofs, geomdim, matlabFile)
        matlabArray.dtype = t
        matlabindices = numpy.argsort(matlabArray, axis=0, order=order)
        ufunc = dolfin.interpolate(dolfin.Expression('0', degree=1), V)
        # ... without degree=1 results in warnings, jde o stupen polynomu pro ktery je numericka integrace vyrazu na danem elementu presna, tady s klidem 1
        u = ufunc.vector()
        if self.DEBUG:
            totalError = 0.0;
            for i in range(0, dofs):
                u[dofindices[i]] = matlabArray[matlabindices[i], 0][0][geomdim];
                totalError = totalError + (matlabArray[matlabindices[i], 0][0][0] - dof_coordinates[dofindices[i]][0][0][0])**2 + (matlabArray[matlabindices[i], 0][0][1] - dof_coordinates[dofindices[i]][0][0][1])**2
            assert(totalError==0)
        else:
            for i in range(0, dofs):
                u[dofindices[i]] = matlabArray[matlabindices[i], 0][0][geomdim]
        return(ufunc)
        
    def importMatlabArray(self, dofs, geomdim, matlabFile):
        fin = open(matlabFile, "rb")
        finalArray = numpy.ndarray(shape=(dofs, geomdim+1), dtype=numpy.dtype('float64'), order='C')
        for i in range(0, dofs):
            for j in range(0, geomdim+1):
                finalArray[i,j] = self.readNextDouble(fin);
        fin.close()
        return(finalArray);
        
    def readNextDouble(self, f):
        v = struct.unpack('d', f.read(8));
        return v[0];
