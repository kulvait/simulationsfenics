import numpy as np
#This class define various anti plane domains of the type V notch with additional intrusions
#This class is unable to figure out if r is admissible since it does not work with alphas

class GeometryParams:
	'Class of V notch type domains with various properties'
	acceptableDomains = ['V', 'VO', 'VC']	

	def __init__(self, domaintype, r):
		if not domaintype in GeometryParams.acceptableDomains:
			raise Exception('Domain type {} is not recognized, please specify valid domain type.'.format(domaintype))
		self.domaintype = domaintype
		if domaintype == 'V':
			self.r = np.NaN
		else:
			self.r = r

	def getDirectoryString(self):
		if self.domaintype == 'V':
			return('V')
		else:
			return('{}_{}'.format(self.domaintype, self.r))
		
	@staticmethod
	def createGeometryParams(domainString):
		if domainString == 'V':
			return(GeometryParams('V', np.NaN))
		twoparts = domainString.split("_")
		return(GeometryParams(twoparts[0], float(twoparts[1])))

