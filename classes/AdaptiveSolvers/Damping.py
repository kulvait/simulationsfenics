'''
Created on May 29, 2014

@author: Vojtech Kulvait
'''
import numpy
import math
from dolfin import *
import __builtin__

class Damping(object):
    def __init__(self, Ffactory, u, v, bcs, V, maximumIterations = None, absoluteTolerance = None, minimumDamping = None):
        self.Ffactory = Ffactory
        self.u = u
        self.v = v
        self.bcs = bcs
        self.V = V
        if absoluteTolerance is None:
            self.absoluteTolerance = 1e-10
        else:
            self.absoluteTolerance = float(absoluteTolerance)
        if maximumIterations is None:
            self.maximumIterations = 30
        else:
            self.maximumIterations = int(maximumIterations)
        if minimumDamping is None:
            self.minimumDamping = 0.01
        else:
            self.minimumDamping = float(minimumDamping)
        
    def computeResidual(self, indicators = None):
        if indicators is None:
            indicators = assemble(self.F)
        if type(self.bcs) is list:
            for i in range(0, len(self.bcs)):
                self.bcs[i].apply(indicators)
        else:
            self.bcs.apply(indicators)
        error = numpy.linalg.norm(indicators.array())
        return(error)
    
    def optimalDamping(self, f, inc):
        if self.currentDamping < 0.00000001:
            self.currentDamping = self.minimumDamping
            self.tmp.vector()[:] = f + self.currentDamping * inc
        else:
            self.tmp.vector()[:] = f + self.currentDamping * inc;
            prevResidual = self.residualNow
            currentResidual = self.computeResidual(assemble(self.Ffactory(self.tmp, self.v)))
            prevdamping = self.currentDamping
            if currentResidual < prevResidual and self.currentDamping < 1:
                while currentResidual < prevResidual and self.currentDamping < 1:
                    prevdamping = self.currentDamping
                    self.currentDamping = __builtin__.min(1.5*self.currentDamping, 1.0)
                    self.tmp.vector()[:] = f + self.currentDamping * inc
                    prevResidual = currentResidual
                    currentResidual = self.computeResidual(assemble(self.Ffactory(self.tmp, self.v)))
                if currentResidual > prevResidual:
                    self.currentDamping = prevdamping
                    self.tmp.vector()[:] = f + self.currentDamping * inc
            while self.computeResidual(assemble(self.Ffactory(self.tmp, self.v))) > self.residualNow:
                self.currentDamping = self.currentDamping/1.5;
                self.tmp.vector()[:] = f + self.currentDamping * inc
        if self.currentDamping < 0.00000001:
            self.currentDamping = self.minimumDamping
            self.tmp.vector()[:] = f + self.currentDamping * inc   
        
    def solve(self):
        self.currentDamping = 1
        self.iteration = 0
        du = TrialFunction(self.V)
        self.F = self.Ffactory(self.u, self.v)
        J = derivative(self.F, self.u, du)
        self.tmp  = Function(self.V)
        self.residualNow = self.computeResidual()
        increment = Function(self.V)
        print("At the step {0} is the residual {1}.".format(0, self.residualNow))
        while self.residualNow > self.absoluteTolerance and self.iteration < self.maximumIterations:
            A, b = assemble_system(J, -self.F, self.bcs)
            solve(A, increment.vector(), b)
            self.optimalDamping(self.u.vector(), increment.vector())
            self.u.assign(self.tmp)
            self.residualNow = self.computeResidual()
            self.iteration = self.iteration + 1;
            print("At the step {0} is the residual {1} and damping {2}.".format(self.iteration, self.residualNow, self.currentDamping))
        if self.residualNow > self.absoluteTolerance:
            raise("Residual {0} did not converged under absolute tolerance {1} after {2} steps.".format(self.residualNow, self.absoluteTolerance, self.iteration))
        if math.isnan(self.residualNow):
            raise("Residual {0} did not converged under absolute tolerance {1} after {2} steps.".format(self.residualNow, self.absoluteTolerance, self.iteration))