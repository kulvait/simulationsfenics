# -*- coding: utf-8 -*-
'''
Created on May 06, 2017

@author: Vojtech Kulvait
'''
from DirectoryWrapper import DirectoryWrapper
from MaterialModel import MaterialModel
from dolfin import *
from VNotchSolverPowerLaw import VNotchSolverPowerLaw
from math import log10, floor
import copy
from math import log10, floor
import tabulate as tab

class ErrorNorm:
	def __init__(self, baseDirectory, domainParameters, models=None):
		if models is None:
			models = MaterialModel.getModels()
			keys = filter(lambda x: x[:2]!='A4', models.keys())
			keys = filter(lambda x: x[:3]=='A1L' or x[2]=='N', keys)
			models = {x:models[x] for x in keys}
			keys = models.keys()
			newkeys = MaterialModel.keyToModel()
			for key in keys:
				models[newkeys[key]] = models.pop(key)#Odstran polozku s danym key a pridej ji s novym key
		self.models = models
		self.domainParameters = domainParameters
		#self.baseDirectory = '/usr/nobackup2/kulvv1am/POWERLAWMESHES/1/V'
		self.baseDirectory = baseDirectory
		self.changeDirectory(domainParameters)
		print('Just set directory to {}'.format(self.directory))
		self.dv = DirectoryWrapper(self.directory)
		self.DEBUG = False
		tab.LATEX_ESCAPE_RULES = {}#No escaping latex
	
	def changeDirectory(self, domainParameters):
		dp = domainParameters
		if dp['geom'] == 'V':
			self.directory = '{}/{}/V'.format(self.baseDirectory, dp['alpha'])
		elif dp['geom'] == 'VC':
			self.directory = '{}/{}/VC_{}'.format(self.baseDirectory, dp['alpha'], dp['rc'])
		else:
			self.directory = '{}/{}/VO_{}'.format(self.baseDirectory, dp['alpha'], dp['rc'])

	def process(self, referenceLevel=None):
		if referenceLevel is None:
			referenceLevel = 6
		meshparameters = {}
		for i in range(referenceLevel):
			meshparameters[i] = []
			meshparameters[i].append(self.dv.getNumberElements(i+1))
			params = self.dv.getMeshParams(i+1)
			meshparameters[i].append(self.dv.getDofNumber(i+1))
			meshparameters[i].append(params['hmin'])
			meshparameters[i].append(params['hmax'])
		parameters['allow_extrapolation'] = True
		errornorms = {}
		keys = self.models.keys()
		for key in keys:
			A6 = self.dv.getSolution(key, referenceLevel, referenceLevel)
			norm = dolfin.norm(A6, 'H1')
			errornorms[key] = []
			for j in range(referenceLevel-1):
				Aj = self.dv.getSolution(key, j+1, referenceLevel)
				errornorms[key].append(dolfin.errornorm(Aj, A6, 'H1')/norm)
		return([meshparameters, errornorms])

	def generateLatexCode(self, referenceLevel, mp = None, nrm = None):
		if mp is None:
			[mp, nrm] = self.process(referenceLevel)
		latexCode = '\\begin{table}[htbp]\n'
		latexCode += '\\centering\n'
		latexCode += ErrorNorm.generateLatexTable(mp, nrm)
		latexCode += '\\caption{Mesh properties and error norms of the solutions with respect to a refinement level for computational domain '
		dp = self.domainParameters
		label = 'tb5:errornorms{}_alpha{}'.format(dp['geom'], dp['alpha'])
		if dp['geom'] == 'V':
			geostr = 'V with $\\alpha = {}^{{\circ}}$.'.format(dp['alpha'])
		else:
			geostr = '{} with $\\alpha = {}^{{\circ}}$ and $r_c = {}$.'.format(dp['geom'],dp['alpha'], dp['rc'])
			label += '_rc{}'.format(dp['rc'])
		latexCode += geostr
		latexCode += ' For detailed description of row labels meaning, see \secref{sc5:gcs}.}\n'
		latexCode += '\\label{{{}}}\n'.format(label)
		latexCode += '\\end{table}'
		return(latexCode)
	
	@staticmethod
	def generateLatexTable(mp, nrm):
		keys = nrm.keys()
		newkeys = MaterialModel.keyToModel()
		newkeys['A1L'] = 'LIN'#Chci aby v tabulce byly polozky LIN, NLS, NLB...
		for key in keys:
			nrm[newkeys[key]] = nrm.pop(key)#Odstranit key a pridat ho pod novym klicem
		printkeys = copy.deepcopy(nrm.keys())
		printkeys.sort()
		header = ['Elements', 'DOFs', '$h_{min}$', '$h_{max}$']
		header = header + map(lambda A: '$\\NORM{{A_{{{}}}}}^{{ref}}_{{1,2}}$'.format(A), printkeys)
		table = []
		for i in range(len(mp)):
			row = copy.deepcopy(mp[i])
			row[0] = '$\\num{{{}}}$'.format(row[0])
			row[1] = '$\\num{{{}}}$'.format(row[1])
			row[2] = '$\\num{{{}}}$'.format(ErrorNorm.round_to_2(row[2]))
			row[3] = '$\\num{{{}}}$'.format(ErrorNorm.round_to_2(row[3]))
			if i != len(mp)-1:
				for key in printkeys:
					row.append('$\\num{{{}}}$'.format(ErrorNorm.removeTrailingZeros(ErrorNorm.round_to_2(nrm[key][i]))))
			else:
				for key in printkeys:
					row.append('$\\num{0.0}$')
			table.append(row)
		transpose = [header]+table
		transposedheader = ['Refinement'] + range(len(mp))
		return(tab.tabulate(zip(*transpose),transposedheader,'latex_booktabs'))

	@staticmethod
	def round_to_2(x):
		roundplaces =  -int(floor(log10(abs(x))))
#roundplaces = max(1, roundplaces)
		rounded = round(x, roundplaces)
		return(rounded)
	
	@staticmethod
	def removeTrailingZeros(x):
		string = '{0:.10f}'.format(x)
		return(string.rstrip('0'))


#		printkeys = copy.deepcopy(self.models.keys())
#		header = ['Elements', 'DOFs', 'hmin', 'hmax']
#header = header + printkeys
#table = []
#for i in range(6):
#	row = copy.deepcopy(meshparameters[i])
#	row = map(lambda x: '$\\num{{{}}}$'.format(x), row)
#	if i != 5:
#		for key in printkeys:
#			row.append('$\\num{{{}}}$'.format(removeTrailingZeros(round_to_2(errornorms[key][i]))))
#	else:
#		for key in printkeys:
#			row.append('$\\num{0.0}$')
#	table.append(row)

#print tabulate(table, header, 'latex_booktabs')
#transpose = [header]+table
#transposedheader = [0,1,2,3,4,5]
#print tabulate(zip(*transpose),transposedheader,'latex_booktabs') 

#linkeys = filter(lambda x: models[x].linearmodel, models.keys())
#linnorms = {x:errornorms[x] for x in linkeys}
#print(tabulate(errornorms))

#dv.getMeshParams(6)['hmax']
#dv.getMeshParams(6)['numelm']
