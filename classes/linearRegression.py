import rpy2.robjects as robjects
from rpy2.robjects import FloatVector
from rpy2.robjects.packages import importr
stats = importr('stats')
r = robjects.r

#observedstress = [1,2,3,4,5]
#independentstress = [11, 19, 32, 40, 51]
#observedstress_r = robjects.FloatVector(observedstress)
#robjects.globalenv["observed"] = observedstress_r
#independentstress_r = robjects.FloatVector(independentstress)
#robjects.globalenv["independent"] = independentstress_r
#lm = stats.lm("observed ~ independent - 1")


#observedstress ... value of stress that is derived from computer simulation
#independentstress ... value of stress/K that is derived from asymptotic solution
def regressionSimpleModel(observedstress, independentstress):
	observedstress_r = robjects.FloatVector(observedstress)
	robjects.globalenv["observed"] = observedstress_r
	independentstress_r = robjects.FloatVector(independentstress)
	robjects.globalenv["independent"] = independentstress_r
	lm = stats.lm("observed ~ independent - 1")
	return(lm.rx('coefficients')[0][0])
