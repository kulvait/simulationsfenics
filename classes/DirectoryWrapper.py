# -*- coding: utf-8 -*-
'''
Created on May 05, 2017

@author: Vojtech Kulvait
'''

from dolfin import *
import SolutionImporter

class DirectoryWrapper:
    'Class to obtain object from given directory with the solution.'
    def __init__(self, directory):
        self.directory = directory
        self.DEBUG = False

    def getMeshXML(self, refNum):
        return('{}/A_refinement{}_dolfin.xml'.format(self.directory, refNum))    

    def getMeshParams(self, refNum):
        mesh = dolfin.Mesh(self.getMeshXML(refNum));
        return({'numelm':mesh.num_cells(), 'hmin':mesh.hmin(), 'hmax':mesh.hmax()})

    def getNumberElements(self, refNum):
        mesh = dolfin.Mesh(self.getMeshXML(refNum));
        return(mesh.num_cells())

    def getDofNumber(self, refNum):
        mesh = dolfin.Mesh(self.getMeshXML(refNum))
        V = FunctionSpace(mesh, 'Lagrange', 2)
        return(V.dim())

    def getV(self, refNum):
        mesh = dolfin.Mesh(self.getMeshXML(refNum))
        return(FunctionSpace(mesh, 'Lagrange', 2))

    def getSolutionTXT(self, solutionLabel, refNum):
        return('{}/FEniCS_{}_refinement{}.txt'.format(self.directory, solutionLabel, refNum))

    def getMesh(self, refNum):
        return(dolfin.Mesh(self.getMeshXML(refNum)))

    def getSolution(self, label, refNum, targetSpaceRefNum = None):
        if targetSpaceRefNum is None:
            extrapolate = False
        else:
            extrapolate = True
        importer = SolutionImporter.SolutionImporter()
        mesh = self.getMesh(refNum)
        V = FunctionSpace(mesh, 'Lagrange', 2)
        solTXT = self.getSolutionTXT(label, refNum)
        parameters['allow_extrapolation'] = extrapolate
        A = importer.importSolution(V, solTXT)
        if extrapolate and refNum != targetSpaceRefNum:
            print 'Extrapolating'
            mesh_extrapolate = self.getMesh(targetSpaceRefNum)
            V_extrapolate = FunctionSpace(mesh_extrapolate, 'Lagrange', 2)
            A = project(A, V_extrapolate)
#        parameters['allow_extrapolation'] = False    
        return(A)
