# -*- coding: utf-8 -*-
'''
Created on Apr 12, 2017

@author: Vojtech Kulvait
'''
import numpy as np
import os
import re
import sys
from MaterialModel import MaterialModel
from GeometryParams import GeometryParams

class DirectoryVisitor:
        'Class for visiting directory structure with computational domains and running solver.'
	def __init__(self, directory, solve):
		self.directory = directory
		self.solve = solve
		self.DEBUG = False

	#This method compute solutions, it just finds for which alphas we are going to compute solutions
	#and if there exists subdirectory for that alpha, then it calls generateFEniCSSolutionsInAngleDirectory
	def generateFEniCSSolutions(self, modelsdic, forcesize, alphaslist, domainsdeflist, refinementslist, overwriteexisting):
		if(alphaslist is None):
			subdirs = filter(lambda x: os.path.isdir(os.path.join(self.directory, str(x))), os.listdir(self.directory))
			subdirs = filter(lambda x: x.isdigit(), subdirs)
		else:
			subdirs = filter(lambda x: os.path.isdir(os.path.join(self.directory, str(x))), alphaslist)
			notsubdirs = filter(lambda x: not os.path.isdir(os.path.join(self.directory, str(x))), alphaslist)
			for notsubdir in notsubdirs:
				sys.stderr.write("Can not find subdirectory for alpha={}.\n".format(notsubdir))
		if(modelsdic is None):
			modelsdic = MaterialModel.getModels();
		for subdir in subdirs:
			if self.DEBUG:
				print('Going to directory {}.'.format(os.path.join(self.directory, str(subdir))))
			self.generateFEniCSSolutionsInAngleDirectory(os.path.join(self.directory, str(subdir)), modelsdic, forcesize, domainsdeflist, refinementslist, overwriteexisting)

	#This method finds which subdirs to process in particular alphasubdir
	def generateFEniCSSolutionsInAngleDirectory(self, alphasubdir, modelsdic, forcesize, domainsdeflist, refinementslist, overwriteexisting):
		if(domainsdeflist is None):
			subdirs = filter(lambda x: os.path.isdir(os.path.join(alphasubdir, x)), os.listdir(alphasubdir))
		else:
			subdirs = map(lambda x: x.getDirectoryString(), domainsdeflist)
			notsubdirs  = filter(lambda x: not os.path.isdir(os.path.join(alphasubdir, x)), subdirs)
			for notsubdir in notsubdirs:
				sys.stderr.write("Can not find subdirectory {}/{}.\n".format(alphasubdir, notsubdir))
			subdirs = filter(lambda x: os.path.isdir(os.path.join(alphasubdir, x)), subdirs)
		for subdir in subdirs:
			#if self.DEBUG:
				#print('Going to directory {}.'.format(subdir))
			self.generateFEniCSSolutionsInDirectory(os.path.join(alphasubdir, subdir), modelsdic, forcesize, refinementslist, overwriteexisting)
	
	#This is function that process top level directory with computational domain definitions
	def generateFEniCSSolutionsInDirectory(self, currentdir, modelsdic, forcesize, refinementslist, overwriteexisting):
		if(refinementslist is None):
			xmlfiles = filter(lambda x: x.endswith("dolfin.xml"), os.listdir(currentdir))
			refinementslist = map(DirectoryVisitor.numOfRefinements, xmlfiles)
		refinementslist.sort()
		#print("Working with currentdir {} and refinementlist: {}".format(currentdir, refinementslist));
		files = filter(lambda x: os.path.isfile(os.path.join(currentdir, "A_refinement{}_dolfin.xml".format(x))), refinementslist)
		files = map(lambda x: os.path.join(currentdir, "A_refinement{}_dolfin.xml".format(x)), files)
		notfiles = filter(lambda x: not os.path.isfile(os.path.join(currentdir, "A_refinement{}_dolfin.xml".format(x))), refinementslist)
		notfiles = map(lambda x: os.path.join(currentdir, "A_refinement{}_dolfin.xml".format(x)), notfiles)
		for notfile in notfiles:
			sys.stderr.write("File {} does not exist, can not compute solution.\n".format(notfile))
		for xmlfile in files:
			#if self.DEBUG:
			#	print('Creating solution for {} file.'.format(xmlfile))
			self.callSolver(xmlfile, modelsdic, forcesize, overwriteexisting)
	
	#Actual call to solver
	def callSolver(self, xmlfile, modelsdic, forcesize, overwriteexisting):
		#print("Processing file {}.".format(xmlfile))
		keys = modelsdic.keys();
		linearkeys = filter(lambda x: modelsdic[x].linearmodel, keys)
		nonlinearkeys = filter(lambda x: modelsdic[x].nonlinearmodel, keys)
		refinements = DirectoryVisitor.numOfRefinements(xmlfile)
		parrentdir = os.path.dirname(xmlfile)
		for key in linearkeys:
			if overwriteexisting or not os.path.isfile(os.path.join(parrentdir, "FEniCS_{}_refinement{}.txt".format(key, refinements))):
				self.solve(xmlfile, key, modelsdic[key], forcesize)
		for key in nonlinearkeys:
			if overwriteexisting or not os.path.isfile(os.path.join(parrentdir, "FEniCS_{}_refinement{}.txt".format(key, refinements))):
				#try:
				self.solve(xmlfile, key, modelsdic[key], forcesize)
				#except:
				#print "Unexpected error:", sys.exc_info()[0]
	
	@staticmethod		
	def numOfRefinements(xmlfilename):
		m = re.search(r'(\d+)_dolfin.xml$', xmlfilename)
		# if the string ends in digits m will be a Match object, or None otherwise.
		if m is not None:
    			return(int(m.group(1)))
		else:
			return np.NaN

			


