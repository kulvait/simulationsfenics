import numpy as np;

class MaterialModel:
	'class for storing material model data to use for the anti-plane stress computations'
#In python static members are declared outside constructor, other inside
#approxsol ... to define initial solution when using nonlinear model, natural choice is linear approximation or interpolation of less refined solution
	def __init__(self, nonlinearmodel, mu_l, mu, qprime, tau0, approxsol):
		self.nonlinearmodel = nonlinearmodel
		self.linearmodel = not nonlinearmodel
		if self.nonlinearmodel:
			self.mu_l = mu_l
			self.tau0 = tau0
			self.qprime = qprime
			self.mu = mu
			self.approxsol = approxsol
		else:
			self.mu_l = mu_l
			self.tau0 = np.NaN
			self.qprime = np.NaN
			self.mu = np.NaN
			self.approxsol = ''

	@staticmethod
	def getModels():
		A1L = MaterialModel(False, 23.5*10**9, 0,0,0, '')
		A2L = MaterialModel(False, 21.75*10**9,0,0,0, '')
		A3L = MaterialModel(False, 22.05*10**9,0,0,0, '')
		A4L = MaterialModel(False, 30.8*10**9,0,0,0, '')
		A1NLMIN = MaterialModel(True, 23.5*10**9, 24.7*10**9, 2.23, 0.5*10**9, 'A1L')
		A2NLMIN = MaterialModel(True, 21.75*10**9, 27.4*10**9, 2.49, 0.5*10**9, 'A2L')
		A3NLMIN = MaterialModel(True, 22.05*10**9, 20.2*10**9, 2.99, 0.5*10**9, 'A3L')
		A4NLMIN = MaterialModel(True, 30.8*10**9, 30.8*10**9, 4.29, 0.5*10**9, 'A4L')
		A1NLMAX = MaterialModel(True, 23.5*10**9, 22864*10**9, 7.65, 0.5*10**9, 'A1L')
		A2NLMAX = MaterialModel(True, 21.75*10**9, 1226*10**9, 9.15, 0.5*10**9, 'A2L')
		A3NLMAX = MaterialModel(True, 22.05*10**9, 4137*10**9, 15.68, 0.5*10**9, 'A3L')
		A4NLMAX = MaterialModel(True, 30.8*10**9, 662287*10**9, 56.49, 0.5*10**9, 'A4L')
		cmd = {};
		cmd["A1L"] = A1L
		cmd["A2L"] = A2L
		cmd["A3L"] = A3L
		cmd["A4L"] = A4L
		cmd["A1NLMIN"] = A1NLMIN
		cmd["A2NLMIN"] = A2NLMIN
		cmd["A3NLMIN"] = A3NLMIN
		cmd["A4NLMIN"] = A4NLMIN
		cmd["A1NLMAX"] = A1NLMAX
		cmd["A2NLMAX"] = A2NLMAX
		cmd["A3NLMAX"] = A3NLMAX
		cmd["A4NLMAX"] = A4NLMAX
		return(cmd)

	@staticmethod
	def tabulateModels(dic):
		fancydic = {}
		for k in dic.keys():
			fancydic[k] = {}
			fancydic[k]['linear'] = dic[k].linearmodel
			fancydic[k]['mu_l'] = dic[k].mu_l
			fancydic[k]['tau0'] = dic[k].tau0
			fancydic[k]['qprime'] = dic[k].qprime
			fancydic[k]['mu'] = dic[k].mu
		return(fancydic)

	@staticmethod
	def keyToModel():
		dic = {}
		dic['A1L'] = 'LIN1'
		dic['A2L'] = 'LIN2'
		dic['A3L'] = 'LIN3'
		dic['A4L'] = 'LIN4'
		dic['A1NLMIN'] = 'NLB1'
		dic['A2NLMIN'] = 'NLB2'
		dic['A3NLMIN'] = 'NLB3'
		dic['A4NLMIN'] = 'NLB4'
		dic['A1NLMAX'] = 'NLS1'
		dic['A2NLMAX'] = 'NLS2'
		dic['A3NLMAX'] = 'NLS3'
		dic['A4NLMAX'] = 'NLS4'
		return(dic)
