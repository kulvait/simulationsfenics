'''
Created on March 29, 2016
This model is an exponential model to fit Gum Metal experiment and followup in anti plane setting.

Parameters of the model that were fitted to shear response.
mu_linear = 26 * 10**9
mu = 9.16 10**39
r = 5.25

@author: Vojtech Kulvait
'''
from dolfin import *
from BoundaryConditions import boundary_definition
from Damping import Damping

class VNotchSolverExtended(object):
    def __init__(self, deg, r, mu, mu_linear):
        self.deg = float(deg)
        self.r = float(r)
        self.mu = float(mu)
	self.mu_linear = float(mu_linear)

    def initMesh(self, meshObject, lagrangeSpaceDim):
        self.mesh = meshObject
        self.lagrangeSpaceDim = lagrangeSpaceDim;

    #Bylo by mozne pouzit stejny postup jako u solveNonLinear a melo by zkonvergovat v jednom kroku.
    def solveLinear(self, meshXML, lagrangeSpaceDim=None):
        if lagrangeSpaceDim is None:
            self.lagrangeSpaceDim = int(1)
        else:
            self.lagrangeSpaceDim = int(lagrangeSpaceDim)        
        
        self.mesh = dolfin.Mesh(meshXML)
        V = FunctionSpace(self.mesh, 'Lagrange', self.lagrangeSpaceDim)
        bc = boundary_definition()
        return(self.solveLinearOn(V, bc));
    
    def solveLinearOn(self, V, bc):
        u = TrialFunction(V)
        v = TestFunction(V)
        a = inner(self.psilinear()*nabla_grad(u), nabla_grad(v))*dx
        bcs = bc.fullBC(V)
        nothing = Constant(0.0)
        rhs = nothing*v*dx
        A, b = assemble_system(a, rhs, bcs)
        u_k = Function(V)
        U_k = u_k.vector()
        solve(A, U_k, b)
        indicators = assemble(inner(self.psilinear()*nabla_grad(u_k), nabla_grad(v))*dx)
        
        for i in range(0, len(bcs)):
            bcs[i].apply(indicators, U_k)
        error_estimate = sqrt(sum(i**2 for i in indicators.array()))
        print "error_estimate = ", error_estimate
        return(u_k)
    
    def getF(self, u, v):
        return(inner(self.psi(sqrt(2)*sqrt(inner(nabla_grad(u), nabla_grad(u))))*nabla_grad(u), nabla_grad(v))*dx)
        
    
    def solveNonlinear(self, meshXML, lagrangeSpaceDim = None, adaptiveSolver = None, damping = None, toExtrapolate = None, extrapolateSpace = None):
        if lagrangeSpaceDim is None:
            lagrangeSpaceDim = int(1)
        else:
            lagrangeSpaceDim = int(lagrangeSpaceDim)
            
        if adaptiveSolver is None:
            adaptiveSolver = "newton"
        else:
            adaptiveSolver = str(adaptiveSolver)

        if damping is None:
            damping = float(1.0)
        else:
            damping = float(damping)
        
        self.mesh = dolfin.Mesh(meshXML);
        V = FunctionSpace(self.mesh, 'Lagrange', lagrangeSpaceDim)
        bc = boundary_definition()
        du = TrialFunction(V)
        parameters['allow_extrapolation'] = True
        u_ = Function(V)     # most recently computed solution
        u_ = self.solveLinearOn(V, bc)#interpolate(Expression("x[0]"), V)
        #could not call u_ = self.solveLinear(meshXML, lagrangeSpaceDim) this problem described here
        #http://fenicsproject.org/qa/3537/mesh-xml-inconsistent-import
        v  = TestFunction(V)
        #This original formulation does not match comsol formulation
        #F  = inner(self.psi(2*sqrt(inner(nabla_grad(u_), nabla_grad(u_))))*nabla_grad(u_), nabla_grad(v))*dx
        F  = self.getF(u_, v)
        
        J  = derivative(F, u_, du)  # Gateaux derivative in dir. of du
        #J._coefficients
        #nelze dosadit problem = NonlinearVariationalProblem(F, u_, bc.fullBC(V), J) asi kvuli praci s pointery
        bcs = bc.fullBC(V)
        problem = NonlinearVariationalProblem(F, u_, bcs, J)
        solver  = NonlinearVariationalSolver(problem)
        prm = solver.parameters
        if adaptiveSolver == "newton":
            prm['newton_solver']['absolute_tolerance'] = 1E-30
            prm['newton_solver']['relative_tolerance'] = 1E-10
            prm['newton_solver']['maximum_iterations'] = 500
            prm['newton_solver']['relaxation_parameter'] = damping
            dolfin.info(solver.parameters, verbose=True)
            solver.solve()
        elif adaptiveSolver == "snes":
            prm['nonlinear_solver'] = 'snes'
            prm['snes_solver']['line_search'] = 'bt'   #newtonls a cp fungovalo, ale divne
            prm['snes_solver']['absolute_tolerance'] = 1E-10
            prm['snes_solver']['relative_tolerance'] = 1E-10
            prm['snes_solver']['maximum_iterations'] = 1000
            prm['snes_solver']['linear_solver'] = 'lu'    # use direct LU solver
            prm['snes_solver']['report'] = True
            prm['snes_solver']['error_on_nonconvergence'] = True
            prm['snes_solver']['method'] = 'newtontr'  # this is Newton with adaptive Trust region method
            #prm['snes_solver']['method'] = 'newtonls'  # this is Newton with adaptive line-search method
            # parametrs for direct LU solver
            prm['snes_solver']['lu_solver']['same_nonzero_pattern']=True
            #prm['snes_solver']['lu_solver']['symmetric']=True
            prm['snes_solver']['lu_solver']['verbose']=True
            dolfin.info(solver.parameters, verbose=True)
            solver.solve()
            F  = inner(self.psi(sqrt(2)*sqrt(inner(nabla_grad(u_), nabla_grad(u_))))*nabla_grad(u_), nabla_grad(v))*dx
            J  = derivative(F, u_, du)  # Gateaux derivative in dir. of du
            bcs = bc.fullBC(V)
            problem = NonlinearVariationalProblem(F, u_, bcs, J)
            solver  = NonlinearVariationalSolver(problem)
            prm = solver.parameters
            prm['nonlinear_solver'] = 'newton'
            prm['newton_solver']['absolute_tolerance'] = 1E-10
            prm['newton_solver']['relative_tolerance'] = 1E-10
            prm['newton_solver']['maximum_iterations'] = 500
            prm['newton_solver']['relaxation_parameter'] = damping
            dolfin.info(solver.parameters, verbose=True)
            solver.solve()
        elif adaptiveSolver == "damped":
            if not toExtrapolate is None:
                parameters['allow_extrapolation'] = False
                u_ = project(toExtrapolate, V)
                #meshExtrapolate = extrapolateSpace.mesh()
                #geomdim = meshExtrapolate.geometry().dim()
                #dofs = extrapolateSpace.dim()
                #dof_coordinates = extrapolateSpace.dofmap().tabulate_all_coordinates(mesh)                      
                #dof_coordinates.resize((dofs, geomdim))    
            bcs = bc.zeroBC(V)
            damped = Damping(self.getF, u_, v, bcs, V, 1000, 1E-10, damping)
            damped.solve()
        else:
            raise Exception("Unknown type of adaptive solver {0}.".format(adaptiveSolver))
        return(u_)
        
        
    def psi(self, x):
        return(( (1.5 + x ** 2) ** ((self.r - 2)/2) )/(2*self.mu))
    
    def psilinear(self):
        return(1/(2*self.mu_linear))
    
    def gett13(self, A):
        tvector = project(grad(A), VectorFunctionSpace(self.mesh, 'Lagrange', self.lagrangeSpaceDim))
        [t23, t13] = tvector.split(deepcopy=True)  # extract components
        t23 = -t23
	t13.rename('T13', 'Size of stress component T13 in Pa.')
        return(t13)

    def gett23(self, A):
        tvector = project(grad(A), VectorFunctionSpace(self.mesh, 'Lagrange', self.lagrangeSpaceDim))
        [t23, t13] = tvector.split(deepcopy=True)  # extract components
        t23 = project(-t23, FunctionSpace(self.mesh, 'Lagrange', self.lagrangeSpaceDim))
        t23.rename('T23', 'Size of stress component T23 in Pa.')
	return(t23)
        
    def gete13Linear(self, A):
        e13 = project(self.gett13(A)/(2*self.mu_linear), FunctionSpace(self.mesh, 'Lagrange', self.lagrangeSpaceDim))
	e13.rename('E13_LINEAR', 'Size of strain component E13')	
        return(e13)
        
    def gete23Linear(self, A):
	e23 = project(self.gett23(A)/(2*self.mu_linear), FunctionSpace(self.mesh, 'Lagrange', self.lagrangeSpaceDim))
	e23.rename('E23_LINEAR', 'Size of strain component E23')
	return(e23)
    
    def gete13Nonlinear(self, A):
        e13 = project(self.gett13(A)*self.psi(sqrt(2)*sqrt(inner(nabla_grad(A), nabla_grad(A)))), FunctionSpace(self.mesh, 'Lagrange', self.lagrangeSpaceDim))
	e13.rename('E13', 'Size of strain component E13')	
        return(e13)

    def gete23Nonlinear(self, A):
        e23 = project(self.gett23(A)*self.psi(sqrt(2)*sqrt(inner(nabla_grad(A), nabla_grad(A)))), FunctionSpace(self.mesh, 'Lagrange', self.lagrangeSpaceDim))
	e23.rename('E23', 'Size of strain component E23')
	return(e23)
