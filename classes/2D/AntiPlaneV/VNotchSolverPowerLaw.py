'''
Created on March 13, 2017
This model is an exponential model to fit titanium alloys experiment and followup in anti plane setting.

@author: Vojtech Kulvait
'''
from dolfin import *
from BoundaryConditionsInverted import boundary_definition_inverted
from Damping import Damping
import numpy as np
from numpy import linalg as LA
from linearRegression import regressionSimpleModel

class VNotchSolverPowerLaw(object):
    def __init__(self, linearproblem, mu_linear, tau_zero, qprime, mu, DEBUG=None):
        if DEBUG is None:
            self.DEBUG = False
        else:
            self.DEBUG = DEBUG
        self.linearproblem = bool(linearproblem)
        self.mu_linear = float(mu_linear)
        self.tau_zero = float(tau_zero)
        self.qprime = float(qprime)
        self.mu = float(mu)
        if(not self.linearproblem and (np.isnan(self.mu_linear) or self.mu_linear <= 0)):
            print('Parameter mu_linear was not specified. Setting manually to mu.')
            self.mu_linear = self.mu
        print("VNotchSolverPowerLaw, init: Problem linear={}, mulinear={}, tau_zero={}, qprime={}, mu={}".format(self.linearproblem, self.mu_linear, self.tau_zero, self.qprime, self.mu))

    def initMesh(self, meshObject, lagrangeSpaceDim):
        self.mesh = meshObject
        self.lagrangeSpaceDim = lagrangeSpaceDim;
        self.V = FunctionSpace(self.mesh, 'Lagrange', self.lagrangeSpaceDim)
        self.W = VectorFunctionSpace(self.mesh, 'Lagrange', self.lagrangeSpaceDim)#For projecting solutions

    #Bylo by mozne pouzit stejny postup jako u solveNonLinear a melo by zkonvergovat v jednom kroku.
    def solveLinear(self, meshXML, lagrangeSpaceDim=None):
        if lagrangeSpaceDim is None:
            self.lagrangeSpaceDim = int(2)
        else:
            self.lagrangeSpaceDim = int(lagrangeSpaceDim)        
        
        self.mesh = dolfin.Mesh(meshXML)
        V = FunctionSpace(self.mesh, 'Lagrange', self.lagrangeSpaceDim)
        bc = boundary_definition_inverted()
        return(self.solveLinearOn(V, bc));
    
    def solveLinearOn(self, V, bc):
        u = TrialFunction(V)
        v = TestFunction(V)
        a = inner(self.psilinear()*nabla_grad(u), nabla_grad(v))*dx
        #This causes warnings
        bcs = bc.fullBC(V)
        #This causes warnings
        nothing = Constant(0.0)
        rhs = nothing*v*dx
        A, b = assemble_system(a, rhs, bcs)
        u_k = Function(V)
        U_k = u_k.vector()
        solve(A, U_k, b)
        indicators = assemble(inner(self.psilinear()*nabla_grad(u_k), nabla_grad(v))*dx)
        
        for i in range(0, len(bcs)):
            bcs[i].apply(indicators, U_k)
        error_estimate = sqrt(sum(i**2 for i in indicators.array()))
        print "error_estimate = ", error_estimate
        return(u_k)
    
    def getF(self, u, v):
        return(inner(self.psi(sqrt(2)*sqrt(inner(nabla_grad(u), nabla_grad(u))))*nabla_grad(u), nabla_grad(v))*dx(degree=5))#Mozna staci 6 nebo 7 jde o stupen polynomu pro 3 konverguje hezky, pro 5 neni takova hitparada
        
    
    def solveNonlinear(self, meshXML, lagrangeSpaceDim = None, adaptiveSolver = None, damping = None, toExtrapolate = None, extrapolateSpace = None):
        if lagrangeSpaceDim is None:
            self.lagrangeSpaceDim = int(2)
        else:
            self.lagrangeSpaceDim = int(lagrangeSpaceDim)
            
        if adaptiveSolver is None:
            adaptiveSolver = "newton"
        else:
            adaptiveSolver = str(adaptiveSolver)

        if damping is None:
            damping = float(1.0)
        else:
            damping = float(damping)
        
        self.mesh = dolfin.Mesh(meshXML);
        V = FunctionSpace(self.mesh, 'Lagrange', self.lagrangeSpaceDim)
        bc = boundary_definition_inverted()
        du = TrialFunction(V)
        parameters['allow_extrapolation'] = True
        u_ = Function(V)     # most recently computed solution
        u_ = self.solveLinearOn(V, bc)#interpolate(Expression("x[0]"), V)
        #could not call u_ = self.solveLinear(meshXML, self.lagrangeSpaceDim) this problem described here
        #http://fenicsproject.org/qa/3537/mesh-xml-inconsistent-import
        v  = TestFunction(V)
        #This original formulation does not match comsol formulation
        #F  = inner(self.psi(2*sqrt(inner(nabla_grad(u_), nabla_grad(u_))))*nabla_grad(u_), nabla_grad(v))*dx
        F  = self.getF(u_, v)
        
        J  = derivative(F, u_, du)  # Gateaux derivative in dir. of du
        #J._coefficients
        #nelze dosadit problem = NonlinearVariationalProblem(F, u_, bc.fullBC(V), J) asi kvuli praci s pointery
        bcs = bc.fullBC(V)
        problem = NonlinearVariationalProblem(F, u_, bcs, J)
        solver  = NonlinearVariationalSolver(problem)
        prm = solver.parameters
        if adaptiveSolver == "newton":
            prm['newton_solver']['absolute_tolerance'] = 1E-30
            prm['newton_solver']['relative_tolerance'] = 1E-10
            prm['newton_solver']['maximum_iterations'] = 500
            prm['newton_solver']['relaxation_parameter'] = damping
            dolfin.info(solver.parameters, verbose=True)
            solver.solve()
        elif adaptiveSolver == "snes":
            prm['nonlinear_solver'] = 'snes'
            prm['snes_solver']['line_search'] = 'bt'   #newtonls a cp fungovalo, ale divne
            prm['snes_solver']['absolute_tolerance'] = 1E-10
            prm['snes_solver']['relative_tolerance'] = 1E-10
            prm['snes_solver']['maximum_iterations'] = 1000
            prm['snes_solver']['linear_solver'] = 'lu'    # use direct LU solver
            prm['snes_solver']['report'] = True
            prm['snes_solver']['error_on_nonconvergence'] = True
            prm['snes_solver']['method'] = 'newtontr'  # this is Newton with adaptive Trust region method
            #prm['snes_solver']['method'] = 'newtonls'  # this is Newton with adaptive line-search method
            # parametrs for direct LU solver
            prm['snes_solver']['lu_solver']['same_nonzero_pattern']=True
            #prm['snes_solver']['lu_solver']['symmetric']=True
            prm['snes_solver']['lu_solver']['verbose']=True
            dolfin.info(solver.parameters, verbose=True)
            solver.solve()
            F  = inner(self.psi(sqrt(2)*sqrt(inner(nabla_grad(u_), nabla_grad(u_))))*nabla_grad(u_), nabla_grad(v))*dx
            J  = derivative(F, u_, du)  # Gateaux derivative in dir. of du
            bcs = bc.fullBC(V)
            problem = NonlinearVariationalProblem(F, u_, bcs, J)
            solver  = NonlinearVariationalSolver(problem)
            prm = solver.parameters
            prm['nonlinear_solver'] = 'newton'
            prm['newton_solver']['absolute_tolerance'] = 1E-10
            prm['newton_solver']['relative_tolerance'] = 1E-10
            prm['newton_solver']['maximum_iterations'] = 500
            prm['newton_solver']['relaxation_parameter'] = damping
            dolfin.info(solver.parameters, verbose=True)
            solver.solve()
        elif adaptiveSolver == "damped":
            if not toExtrapolate is None:
                parameters['allow_extrapolation'] = False
                u_ = project(toExtrapolate, V)
                #meshExtrapolate = extrapolateSpace.mesh()
                #geomdim = meshExtrapolate.geometry().dim()
                #dofs = extrapolateSpace.dim()
                #dof_coordinates = extrapolateSpace.dofmap().tabulate_all_coordinates(mesh)
                #dof_coordinates.resize((dofs, geomdim))
            bcs = bc.zeroBC(V)
            damped = Damping(self.getF, u_, v, bcs, V, 1000, 1E-10, damping)
            damped.solve()
        else:
            raise Exception("Unknown type of adaptive solver {0}.".format(adaptiveSolver))
        return(u_)

    def psi(self, x):
        leadingConstant = Constant(1/(2*self.mu));
        leadingTermNumerator = Constant((2*self.tau_zero**2)/3);
        numerator = leadingTermNumerator + x**2
        denominator = Constant((2*self.tau_zero**2)/3)
        exponent = Constant((self.qprime-2)/2)
        return(leadingConstant*((numerator/denominator)**exponent))

    def psilinear(self):
        return(1/(2*self.mu_linear))

    def renameFunction(self, function, name, label, defaultname, defaultlabel):
        if name is None:
            name = defaultname
        if label is None:
            label = defaultlabel
        if self.DEBUG:
            print('Renaming function to name={}, label={}'.format(name, label))
        function.rename(name, label)

    #Vraci objekt ufl.indexed.Indexed bez projekce, je vhodny k pozdejsi projekci
    def t13(self, A):
        t13 = A.dx(1)#T13=A.dx2$
        return(t13)

    def t23(self, A):
        t23 = -A.dx(0)#T23=-A.dx1
        return(t23)

    #Objekt reseni dolfin.functions.function.Function
    def getSolution(self, A, name=None, label=None):
        self.renameFunction(A, name, label, 'A', 'Solution representin Airy\'s stress function')
        return(A)

    #Using https://fenicsproject.org/qa/7286/piecewise-definition-of-grad-help-with-syntax
    def gett(self, A, name=None, label=None):
        tvector = interpolate(Expression(('0.0', '0.0'), degree=1), self.W)#Jde o stupen polynomu pro ktery je numericka integrace presna
        assign(tvector.sub(0), project(self.t13(A), self.V))
        assign(tvector.sub(1), project(self.t23(A), self.V))
        self.renameFunction(tvector, name, label, 'T', 'Vector (T13, T23)')
        return(tvector)

    def gettabs(self, A, name=None, label=None):
        tabs = sqrt(2*inner(nabla_grad(A), nabla_grad(A)))
        tabs = project(tabs, self.V)
        self.renameFunction(tabs, name, label, 'abs(T)', 'Norm of T (norm of tensor scaled by sqrt(2))')
        return(tabs)

    def gett13(self, A, name=None, label=None):
        #tvector = project(grad(A), VectorFunctionSpace(self.mesh, 'Lagrange', self.lagrangeSpaceDim))
        #[t23, t13] = tvector.split(deepcopy=True)  # extract components
        #t23 = -t23
        #t13.rename('T13', 'Size of stress component T13 in Pa.')
        t13 = project(self.t13(A), self.V)
        self.renameFunction(t13, name, label, 'T13', 'Stress component T13')
        return(t13)

    def gett23(self, A, name=None, label=None):
        #tvector = project(grad(A), VectorFunctionSpace(self.mesh, 'Lagrange', self.lagrangeSpaceDim))
        #[t23, t13] = tvector.split(deepcopy=True)  # extract components
        #t23 = project(-t23, FunctionSpace(self.mesh, 'Lagrange', self.lagrangeSpaceDim))
        #t23.rename('T23', 'Size of stress component T23 in Pa.')
        t23 = project(self.t23(A), self.V)
        self.renameFunction(t23, name, label, 'T23', 'Stress component T23')
        return(t23)

    def getdofcoords(self):
        dofcoord = self.V.tabulate_dof_coordinates()
        geomdim = self.mesh.geometry().dim()
        dofs = self.V.dim()
        dofcoord.resize(dofs, geomdim)
        return(dofcoord)

    #This function estimates alpha locally in the area of diameter r from point LL
    def estimateAlphaLocally(self, LL, r):
        dofcoord = self.getdofcoords()
        dofshifted = np.subtract(dofcoord, np.array([0.0, LL]))
        ind = (LA.norm(dofshifted, axis=1) < r).nonzero()[0]
        ind = filter(lambda x: dofcoord[x, 1]<0.5 and dofcoord[x, 1] > LL, ind)
        dist = r
        while np.size(ind) < 2:
            dist = dist + r
            ind = (LA.norm(dofshifted, axis=1) < dist).nonzero()[0]
            ind = filter(lambda x: dofcoord[x, 1]<0.5 and dofcoord[x, 1] > LL, ind)
        maxind = dofcoord[ind,1].argmax()#Should be at the edge
        X = dofcoord[ind[maxind]]
        alphaest = 2*np.arctan((X[1]-LL)/X[0])
        return(alphaest)

    #When alpha is not explicitely known this function infer it from geometry
    def estimateAlpha(self):
        dofcoord = self.getdofcoords() 
        #Find triangle of lowerleft, upperright, center
        ind = np.logical_and(dofcoord[:,1] < 0.5, dofcoord[:,0]==0).nonzero() #All coordinates along x=0 with y<0.5
        LL = dofcoord[ind,1].max()
        r=0.5-LL
        epsilon=1e-5
        alphaest = self.estimateAlphaLocally(LL, r)
        while np.absolute(self.estimateAlphaLocally(LL, r) - self.estimateAlphaLocally(LL, r/2)) > epsilon:
            r=r/2
        alphaest = self.estimateAlphaLocally(LL, r)
        return(alphaest)

    def estimateC(self, alpha=None):
        if alpha is None:
            alpha = self.estimateAlpha()
        dof_coordinates = self.getdofcoords()
        ind = np.logical_and(dof_coordinates[:,1] < 0.5, dof_coordinates[:,0]==0).nonzero() #All coordinates along x=0 with y<0.5$
        LL = dof_coordinates[ind,1].max()
        ldist = 0.5-LL
        return(np.array([ldist/np.tan(alpha/2), 0.5]))

    def estimaterc(self, alpha=None):
        if alpha is None:
            alpha = self.estimateAlpha()
        C = self.estimateC()
        dofcoord = self.getdofcoords()
        dofshifted = np.subtract(dofcoord, C)
        rc = np.min(LA.norm(dofshifted, axis=1))
        if rc < 0.0001/2:
            rc = 0
        return(rc)

    @staticmethod
    def atan2(x,y):
        if x>0:
            return(np.arctan(y/x));
        elif x<0 and y >= 0:
            return(np.arctan(y/x) + np.pi)
        elif x<0 and y<0:
            return(np.arctan(y/x) - np.pi)
        elif x==0 and y > 0:
            return(np.pi/2)
        elif x==0 and y < 0:
            return(-np.pi/2)
        else:#x==0 and y==0
            return 0

    def getNonlinearExponent(self, A):
        alpha = self.estimateAlpha()
        lambda_x=np.pi/(np.pi-alpha)
        return(-1/((self.qprime)/2 + np.sqrt(((self.qprime-2)/2)**2+(lambda_x**2)*(self.qprime-1))))

    def getLinearExponent(self, A):
        alpha = self.estimateAlpha()
        lambda_l=np.pi/(2*np.pi-alpha)
        return(lambda_l-1)
 
    #This is not exact computation since phi dependence is complicated we estimated it as in linear case, for phi=0 formula is exact
    def tAsymptoticNonLinearValue(self, KIF, C, alpha, coordinates):
        coordC = np.subtract(coordinates, C)
        r = LA.norm(coordC)
        phi = VNotchSolverPowerLaw.atan2(coordC[0], coordC[1])
        A = KIF/np.sqrt(2*np.pi)
        lambda_x=np.pi/(np.pi-alpha)
        lambda_l=np.pi/(2*np.pi-alpha)
        s = -1/((self.qprime)/2 + np.sqrt(((self.qprime-2)/2)**2+(lambda_x**2)*(self.qprime-1)))
        Trz = A*(r**s)*np.sin(lambda_l*phi)
        Tphiz =  A*(r**s)*np.cos(lambda_l*phi)
        T13 = Trz*np.cos(phi) - Tphiz*np.sin(phi)
        T23 = Trz*np.sin(phi) + Tphiz*np.cos(phi)
        return(np.array([T13, T23]))
    
    #K is stress intensity factor
    def tAsymptoticLinearValue(self, KIF, C, alpha, coordinates):
        coordC = np.subtract(coordinates, C)
        r = LA.norm(coordC)
        phi = VNotchSolverPowerLaw.atan2(coordC[0], coordC[1])
        A = KIF/np.sqrt(2*np.pi)
        lambda_l=np.pi/(2*np.pi-alpha)
        Trz = A*(r**(lambda_l-1))*np.sin(lambda_l*phi)
        Tphiz =  A*(r**(lambda_l-1))*np.cos(lambda_l*phi)
        T13 = Trz*np.cos(phi) - Tphiz*np.sin(phi)
        T23 = Trz*np.sin(phi) + Tphiz*np.cos(phi)
        return(np.array([T13, T23]))

    #Function::set_allow_extrapolation(true) is a good idea
    #Point from C in the direction of phi where f=target, assume f is monotone along phi
    def dst(self, phi, target, f=None, C=None, rc=None, tol=None):
        if rc is None:
            rc = self.estimaterc()
        if C is None:
            C = self.estimateC()
        if f is None:
            f = self.gett23(A)
        if tol is None:
            if target is 0:
                tol = 10**-5
            else:
                tol = target / 10**6
        #I will assume that I can go to the distance 0.5 thus I do not need to compute where exactly is the boundary
        rmin = rc
        rmax = 0.5
        p0 = np.add(C, rmin*np.array([np.cos(phi),np.sin(phi)]))
        p1 = np.add(C, rmax*np.array([np.cos(phi),np.sin(phi)]))
        d = 0.5
        inc = 0.25
        i = 0
        evalpoint = np.add(p0, d*np.subtract(p1,p0))
        if self.DEBUG:
            print("Evaluating f at p0={} and p1={}".format(p0, p1))
        if(f(p0[0], p0[1])< target or f(p1[0], p1[1]) > target):
            sys.stderr.write("VNotchSolverPowerLaw:dst:Function f({})={}, f({})={} at points used does not have value {} return np.nan!\n".format(p0, f(p0[0], p0[1]), p1, f(p1[0], p1[1]), target))
            return(np.nan)
        while np.abs(f(evalpoint[0], evalpoint[1])-target)>tol and i != 100:
            if f(evalpoint[0], evalpoint[1]) > target:
                d = d + inc
            else:
                d = d - inc
            inc = inc/2
            i = i + 1
            evalpoint = np.add(p0, d*np.subtract(p1,p0))
        return(evalpoint)

    def getSetOfPointsToEstimateValueAt(self, A, F=None):
        if F is None:
            F = 10**8
        t23 = self.gett23(A)
        t23.set_allow_extrapolation(True)
        rc = self.estimaterc()
        C = self.estimateC()
        evalat = np.add(C, [rc, 0])
        Tmax = t23(evalat[0], evalat[1])
        if self.DEBUG:
            print("Tmax:{} C:{} rc:{}".format(Tmax, C, rc))
        X = np.array([])
        if Tmax <= 2*F:
            onlypoint = np.add(np.add(C, [0,rc]), [0.0, 0.1])
            X.append(onlypoint)
            X.resize(1,2)
        else:
            count = 0
            appendvalue = 2*F
            while Tmax > appendvalue:
                count = count+1
                X = np.append(X, self.dst(0, appendvalue, t23, C, rc))
                #X = self.dst(0, appendvalue, t23, C, rc)#Maximal value
                appendvalue = 2*appendvalue
                #appendvalue = appendvalue + F
            #Alternative estimation
            #X = np.array([])
            #secondpoint = [1,0.5]
            #for i in range(20):
            #    point = np.add(C, i*np.subtract(secondpoint, C)/19)
            #    X = np.append(X, point)
            X.resize(count, 2)
        return(X)

    def estimateKIF(self, A, linear):
        KIF = 1
        t23 = self.gett23(A)
        S = self.getSetOfPointsToEstimateValueAt(A)
        observedt23 = np.array([])
        independent23 = np.array([])
        for i in range(np.shape(S)[0]):
            observedt23 = np.append(observedt23, t23(S[i,0], S[i,1]))
            if linear:
                independent23 = np.append(independent23, self.tAsymptoticLinearValue(KIF, C, alpha, S[i])[1])
            else:
                independent23 = np.append(independent23, self.tAsymptoticNonLinearValue(KIF, C, alpha, S[i])[1])
        KIF = regressionSimpleModel(observedt23, independent23)
        if self.DEBUG:
            print("KIF={}.".format(KIF))
        return(KIF)


    def gettAsymptotic(self, A, linear=None, KIF=None, name=None, label=None):
        if linear is None:
            linear = True
        #Parameters of geometry
        dof_coordinates = self.getdofcoords()
        ind = np.logical_and(dof_coordinates[:,1] < 0.5, dof_coordinates[:,0]==0).nonzero() #All coordinates along x=0 with y<0.5
        LL = dof_coordinates[ind,1].max()
        ldist = 0.5-LL
        alpha = self.estimateAlpha()
        C=self.estimateC()
        #Stress intensity factor formula from Zappalorto, Lazzarin (2011)
        if KIF is None:
            KIF = self.estimateKIF(A, linear)
        #KIF = 1
        #t23 = self.gett23(A)
        #S = self.getSetOfPointsToEstimateValueAt(A)
        #observedt23 = np.array([])
        #independent23 = np.array([])
        #for i in range(np.shape(S)[0]):
        #    observedt23 = np.append(observedt23, t23(S[i,0], S[i,1]))
        #    independent23 = np.append(independent23, self.tAsymptoticLinearValue(KIF, C, alpha, S[i])[1])
        #KIF = regressionSimpleModel(observedt23, independent23)
        #if self.DEBUG:
        #    print("KIF={}.".format(KIF))
        tvector = interpolate(Expression(('1.0', '0.0'), degree=1), self.W)#Jde o stupen polynomu pro ktery je numericka integrace presna
        geomdim = self.mesh.geometry().dim()
        dofs = self.W.dim()
        dof_coordinates = self.W.tabulate_dof_coordinates()
        dof_coordinates.resize((dofs, geomdim))
        t13dofs = self.W.sub(0).dofmap().dofs()#Degrees of freedom for T13
        t23dofs = self.W.sub(1).dofmap().dofs()
        tx = tvector.vector()
        if linear:
            for i in range(t13dofs.size):
                if(np.array_equal(dof_coordinates[t13dofs[i]], dof_coordinates[t23dofs[i]])):#To bude porad
                    tvalueat = self.tAsymptoticLinearValue(KIF, C, alpha, dof_coordinates[t13dofs[i]])
                    tx[t13dofs[i]] = tvalueat[0]
                    tx[t23dofs[i]] = tvalueat[1]
                else:
                    tvalueat1 = self.tAsymptoticLinearValue(KIF, C, alpha, dof_coordinates[t13dofs[i]])
                    tvalueat2 = self.tAsymptoticLinearValue(KIF, C, alpha, dof_coordinates[t23dofs[i]])
                    tx[t13dofs[i]] = tvalueat1[0]
                    tx[t23dofs[i]] = tvalueat2[1]
            self.renameFunction(tvector, name, label, 'T_linas', 'Vector (T13, T23)')
        else:
            for i in range(t13dofs.size):
                if(np.array_equal(dof_coordinates[t13dofs[i]], dof_coordinates[t23dofs[i]])):#To bude porad
                    tvalueat = self.tAsymptoticNonLinearValue(KIF, C, alpha, dof_coordinates[t13dofs[i]])
                    tx[t13dofs[i]] = tvalueat[0]
                    tx[t23dofs[i]] = tvalueat[1]
                else:
                    tvalueat1 = self.tAsymptoticNonLinearValue(KIF, C, alpha, dof_coordinates[t13dofs[i]])
                    tvalueat2 = self.tAsymptoticNonLinearValue(KIF, C, alpha, dof_coordinates[t23dofs[i]])
                    tx[t13dofs[i]] = tvalueat1[0]
                    tx[t23dofs[i]] = tvalueat2[1]
            self.renameFunction(tvector, name, label, 'T_nlasy', 'Vector (T13, T23)')
        return(tvector)

    def gett13AsymptoticLinear(self, A, name=None, label=None, Ta=None):
        if Ta is None:
            Ta = self.gettAsymptotic(A, True, 10**8)
        t13 = project(Ta.sub(0), self.V)
        self.renameFunction(t13, name, label, 'T13_linas', 'Stress component T13')
        return(t13)

    def gett23AsymptoticLinear(self, A, name=None, label=None, Ta=None):
        if Ta is None:
            Ta = self.gettAsymptotic(A, True, 10**8)
        t23 = project(Ta.sub(1), self.V)
        self.renameFunction(t23, name, label, 'T23_linas', 'Stress component T23')
        return(t23)

    def gett13AsymptoticNonLinear(self, A, name=None, label=None, Ta=None):
        if Ta is None:
            Ta = self.gettAsymptotic(A, False, 10**8)
        t13 = project(Ta.sub(0), self.V)
        self.renameFunction(t13, name, label, 'T13_nlas', 'Stress component T13')
        return(t13)

    def gett23AsymptoticNonLinear(self, A, name=None, label=None, Ta=None):
        if Ta is None:
            Ta = self.gettAsymptotic(A, False, 10**8)
        t23 = project(Ta.sub(1), self.V)
        self.renameFunction(t23, name, label, 'T23_nlas', 'Stress component T23')
        return(t23)
    
    def gett13Asymptotic(self, A, name=None, label=None):
        if self.linearproblem:
            t13 = self.gett13AsymptoticLinear(A, name, label)
        else:
            t13 = self.gett13AsymptoticNonLinear(A, name, label)
        return(t13)

    def gett23Asymptotic(self, A, name=None, label=None):
        if self.linearproblem:
            t23 = self.gett23AsymptoticLinear(A, name, label)
        else:
            t23 = self.gett23AsymptoticNonLinear(A, name, label)
        return(t23)

    def gete13Linear(self, A, name=None, label=None):
        e13 = project(self.t13(A)/(2*self.mu_linear), self.V)
        self.renameFunction(e13, name, label, 'E13_LINEAR', 'Strain component E13')
        return(e13)

    def gete23Linear(self, A, name=None, label=None):
        e23 = project(self.t23(A)/(2*self.mu_linear), self.V)
        self.renameFunction(e23, name, label, 'E23_LINEAR', 'Strain component E23')
        return(e23)

    def gete13NonLinear(self, A, name=None, label=None):
        e13 = project(self.t13(A)*self.psi(sqrt(2)*sqrt(inner(nabla_grad(A), nabla_grad(A)))), self.V)
        self.renameFunction(e13, name, label, 'E13', 'Strain component E13')
        return(e13)

    def gete23NonLinear(self, A, name=None, label=None):
        e23 = project(self.t23(A)*self.psi(sqrt(2)*sqrt(inner(nabla_grad(A), nabla_grad(A)))), self.V)
        self.renameFunction(e23, name, label, 'E23', 'Strain component E23')
        return(e23)

    def gete(self, A, name=None, label=None):
        evector = interpolate(Expression(('0.0', '0.0'), degree=1), self.W)#Jde o stupen polynomu pro ktery je numericka integrace presna
        if self.linearproblem:
            e13 = self.t13(A)/(2*self.mu_linear)
            e23 = self.t23(A)/(2*self.mu_linear)
        else:
            e13 = self.t13(A)*self.psi(sqrt(2)*sqrt(inner(nabla_grad(A), nabla_grad(A))))
            e23 = self.t23(A)*self.psi(sqrt(2)*sqrt(inner(nabla_grad(A), nabla_grad(A))))
        assign(evector.sub(0), project(e13, self.V))
        assign(evector.sub(1), project(e23, self.V))
        self.renameFunction(evector, name, label, 'E', 'Vector (E13, E23)')
        return(evector)

    def geteabs(self, A, name=None, label=None):
        if self.linearproblem:
            e = project(sqrt(2*inner(nabla_grad(A), nabla_grad(A)))/(2*self.mu_linear), self.V)
        else:
            e = project(sqrt(2*inner(nabla_grad(A), nabla_grad(A)))*self.psi(sqrt(2)*sqrt(inner(nabla_grad(A), nabla_grad(A)))), self.V, form_compiler_parameters={'quadrature_degree': 5}) #Jedna se o L2 projekci
        self.renameFunction(e, name, label, 'abs(E)', 'Norm of E (norm of tensor scaled by sqrt(2)).')
        return(e)

    def gete13(self, A, name=None, label=None):
        if self.linearproblem:
            e13 = self.gete13Linear(A, name, label)
        else:
            e13 = self.gete13NonLinear(A, name, label)
        return(e13)

    def gete23(self, A, name=None, label=None):
        if self.linearproblem:
            e23 = self.gete23Linear(A, name, label)
        else:
            e23 = self.gete23NonLinear(A, name, label)
        return(e23)
