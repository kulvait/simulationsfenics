# -*- coding: utf-8 -*-
'''
Created on Apr 15, 2016

@author: Vojtech Kulvait
'''
import os
import re
from vtk import *
        
class VTUDirectoryProcessor(object):
    def __init__(self, directory):
        self.directory = directory
        self.DEBUG = False

    def createImageTable(self):
        tableToRefinement = list()
        for f in os.listdir(self.directory):
                if f.endswith("nonlinear_e13.pvd"):
                    m = re.search("table(\d+)_refinement(\d+)_nonlinear_e13.pvd", f)
                    if m:
                        tableToRefinement.append((m.group(1), m.group(2)))
        tableToRefinement.sort() 
        return(tableToRefinement)


    def printImageTable(self):
        ttr = self.createImageTable()
        for tup in ttr:
            print("In directory found table {0}, refinement {1} data.".format(tup[0], tup[1]))
    
    def createAllPvd(self):
        ttr = self.createImageTable()
        for tup in ttr:
            self.createTopLevelPvd(tup[0], tup[1])
    
    def createTopLevelPvd(self, table, refinement):
        pvd = open(os.path.join(self.directory, "table{0}_refinement{1}.pvd".format(table, refinement)), "w")
        vtufilename = "table{0}_refinement{1}.vtu".format(table, refinement)
        pvd.write(r"""<?xml version="1.0"?>
        <VTKFile type="Collection" version="0.1">
            <Collection>
                <DataSet timestep="0" part="0" file="{0}"/>
            </Collection>
        </VTKFile>""".format(vtufilename) + "\n")
        pvd.close()
        self.createVtuFile(table, refinement);
        
    def getUnstructuredGrid(self, vtufile):
        vtufile = os.path.join(self.directory, vtufile)
        if not os.path.isfile(vtufile):
            raise Exception("File {0} does not exist.".format(vtufile))
        vtuR = vtk.vtkIOXML.vtkXMLUnstructuredGridReader()
        vtuR.SetFileName(vtufile)
        vtuR.Update()
        allData = vtuR.GetOutput()
        return(allData)
        
    def readScalarFromFile(self, vtufile, scalarName):
        allData = self.getUnstructuredGrid(vtufile)
        if allData.GetPointData().GetArrayName(0) == scalarName:
            out = allData.GetPointData().GetArray(0)
        else:
            raise Exception('In file {0} the array number 0 has a name {1} and not {2}'.format(vtufile, allData.GetPointData().GetArrayName(0), scalarName))
        return(out)
        
    def createVtuFile(self, table, refinement):
        vtufilename = "table{0}_refinement{1}.vtu".format(table, refinement)
        vtufilename = os.path.join(self.directory, vtufilename)
        referencevtufile = "table{0}_refinement{1}_linear_e13000000.vtu".format(table, refinement)
        refGrid = self.getUnstructuredGrid(referencevtufile)
        refPointData = refGrid.GetPointData()
        e13l = refPointData.GetArray(0)
        e23l = self.readScalarFromFile("table{0}_refinement{1}_linear_{2}000000.vtu".format(table, refinement, "e23"), "E23_LINEAR")
        t13l = self.readScalarFromFile("table{0}_refinement{1}_linear_{2}000000.vtu".format(table, refinement, "t13"), "T13_LINEAR")
        t23l = self.readScalarFromFile("table{0}_refinement{1}_linear_{2}000000.vtu".format(table, refinement, "t23"), "T23_LINEAR")
        e13 = self.readScalarFromFile("table{0}_refinement{1}_nonlinear_{2}000000.vtu".format(table, refinement, "e13"), "E13")
        e23 = self.readScalarFromFile("table{0}_refinement{1}_nonlinear_{2}000000.vtu".format(table, refinement, "e23"), "E23")
        t13 = self.readScalarFromFile("table{0}_refinement{1}_nonlinear_{2}000000.vtu".format(table, refinement, "t13"), "T13")
        t23 = self.readScalarFromFile("table{0}_refinement{1}_nonlinear_{2}000000.vtu".format(table, refinement, "t23"), "T23")
        refPointData.AddArray(e13l)
        refPointData.AddArray(e23l)
        refPointData.AddArray(t13l)
        refPointData.AddArray(t23l)
        refPointData.AddArray(t13)
        refPointData.AddArray(t23)
        refPointData.AddArray(e13)
        refPointData.AddArray(e23)
        vtuW = vtk.vtkIOXML.vtkXMLUnstructuredGridWriter()
        if self.DEBUG:
            vtuW.SetDataModeToAscii()
        vtuW.SetFileName(vtufilename)
        vtuW.SetInputData(refGrid)
        vtuW.Write()

#p = VTUDirectoryProcessor('/mnt/R0/VK/FENICS/IMAGES')
#p.printImageTable()
#p.createAllPvd()