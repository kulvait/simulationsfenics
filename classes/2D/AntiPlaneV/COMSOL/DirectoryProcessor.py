# -*- coding: utf-8 -*-
'''
Created on Apr 26, 2014

@author: Vojtech Kulvait
'''
from VNotchSolver import VNotchSolver

import csv
import math
import os.path
import dolfin
from dolfin import *
import SolutionExporter
import SolutionImporter
import sys
        
class DirectoryProcessor(object):
    def __init__(self, directory, einfinity=None):
        self.directory = directory
        if einfinity is None:
            self.einfinity = 0.01
        else:
            self.einfinity = float(einfinity)

    def generateImages(self, tableNums, outputDir):
        for table in tableNums:
            [deg, a, kappa, mu] = self.processParameters(table)
            maxRefinementNum = self.getDensestMeshNumber(5, table)
            print("Max refinement num for table {0} is {1}".format(table, maxRefinementNum))
            meshXML = self.getMesh(maxRefinementNum, table)
            solver = VNotchSolver(deg, a, kappa, mu)
            Alinear = solver.solveLinear(meshXML, 2)
            Anonlinear = solver.solveNonlinear(meshXML, 2)
            self.createImages(Alinear, Anonlinear, solver, table, outputDir)
    
    def generateFenicsSolutions(self, tableNums, outputDir, adaptiveSolver = None, fromMesh = None, toMesh = None, damping = None):
        if adaptiveSolver is None:
            adaptiveSolver = "newton"
        else:
            adaptiveSolver = str(adaptiveSolver)
            
        if fromMesh is None:
            fromMesh = 1
        else:
            fromMesh = int(fromMesh)
            
        if toMesh is None:
            toMesh = 7
        else:
            toMesh = int(toMesh)

        if damping is None:
                damping = float(1.0)
        else:
                damping = float(damping)
            
        fromMesh = fromMesh - 1;
        
        exporter = SolutionExporter.SolutionExporter()
        importer = SolutionImporter.SolutionImporter()
        for table in tableNums:
            [deg, a, kappa, mu] = self.processParameters(table)
            maxRefinementNum = self.getDensestMeshNumber(1000, table)
            print("Max refinement num for table {0} is {1}".format(table, maxRefinementNum))
            toMesh = min(maxRefinementNum, toMesh)
            for i in range(fromMesh, toMesh):
                meshXML = self.getMesh(i+1, table)
                solver = VNotchSolver(deg, a, kappa, mu)
                try:
                    print("Solving table{0}_refinement{1}_FENICSNLsolution.txt".format(table, i+1))
                    toExtrapolate = None
                    VExtrapolate = None
                    if False:#i != 0:
                        extrapolateFile = "{0}/table{1}_refinement{2}_FENICSNLsolution.txt".format(outputDir, table, i)
                        if os.path.isfile(extrapolateFile):
                            meshExtrapolate = dolfin.Mesh(self.getMesh(i, table))
                            VExtrapolate = dolfin.FunctionSpace(meshExtrapolate, 'Lagrange', 2)
                            toExtrapolate = importer.importSolution(VExtrapolate, extrapolateFile)
                    Anonlinear = solver.solveNonlinear(meshXML, 2, adaptiveSolver, damping, toExtrapolate, VExtrapolate)
                    exporter.exportSolution(Anonlinear, "{0}/table{1}_refinement{2}_FENICSNLsolution.txt".format(outputDir, table, i+1))
                    Alinear = solver.solveLinear(meshXML, 2)
                    exporter.exportSolution(Alinear, "{0}/table{1}_refinement{2}_FENICSLsolution.txt".format(outputDir, table, i+1))
                except RuntimeError, e:
                    sys.stderr.write("Error occurred solving table{0}_refinement{1}_FENICSNLsolution.txt".format(table, i+1))
                    sys.stderr.write(e.message)

    def processParameters(self, num):
        infoFile = self.directory + "/table{0}_INFO.txt".format(num);
        deg = -1.0;
        mu = -1.0;
        a = -1.0;
        with open(infoFile,'r') as tsvin:
            tsvin = csv.reader(tsvin, delimiter='\t')
            for row in tsvin:
                if row[0]=="deg":
                    deg = float(row[1])
                elif row[0]=="mu":
                    mu = float(row[1])
                elif row[0]=="a":
                    a = float(row[1])
        if a == -1.0 or mu == -1.0 or deg == -1.0:
            raise Exception("There are missing fields in INFO file.")
        kappa = 1/(self.einfinity*2.0*mu*math.sqrt(2.0))**a
        return([deg, a, kappa, mu])
    
    def getMesh(self, refinement, tableNum):
        meshxml = self.directory + "/table{0}_refinement{1}_dolfin.xml".format(tableNum, refinement);
        return(meshxml)
        
    #Get mesh number less or equal maxRefinement such that all less refined meshes exist.
    def getDensestMeshNumber(self, maxRefinement, tableNum):
        maxref = maxRefinement;
        i = maxRefinement
        while i!=0:
            f = self.directory + "/table{0}_refinement{1}_dolfin.xml".format(tableNum, i);
            i = i - 1;
            if not os.path.isfile(f):
                maxref = i
        return(maxref)
    
    def createImages(self, Alinear, Anonlinear, solver, tableNum, outputDir):
        t13 = solver.gett13(Alinear)
        t23 = solver.gett23(Alinear)
        e13 = solver.gete13Linear(Alinear)
        e23 = solver.gete23Linear(Alinear)
        ft13 = dolfin.io.File("{0}/table{1}_linear_t13.pvd".format(outputDir, tableNum))
        ft23 = dolfin.io.File("{0}/table{1}_linear_t23.pvd".format(outputDir, tableNum))
        fe13 = dolfin.io.File("{0}/table{1}_linear_e13.pvd".format(outputDir, tableNum))
        fe23 = dolfin.io.File("{0}/table{1}_linear_e23.pvd".format(outputDir, tableNum))
        t13.rename('T13_LINEAR', 'Size of stress component T13 in Pa.')
	ft13 << t13
	t23.rename('T23_LINEAR', 'Size of stress component T23 in Pa.')
        ft23 << t23
        fe13 << e13
        fe23 << e23
        t13 = solver.gett13(Anonlinear)
        t23 = solver.gett23(Anonlinear)
        e13 = solver.gete13Nonlinear(Anonlinear)
        e23 = solver.gete23Nonlinear(Anonlinear)        
        ft13 = dolfin.io.File("{0}/table{1}_nonlinear_t13.pvd".format(outputDir, tableNum))
        ft23 = dolfin.io.File("{0}/table{1}_nonlinear_t23.pvd".format(outputDir, tableNum))
        fe13 = dolfin.io.File("{0}/table{1}_nonlinear_e13.pvd".format(outputDir, tableNum))
        fe23 = dolfin.io.File("{0}/table{1}_nonlinear_e23.pvd".format(outputDir, tableNum))
        ft13 << t13
        ft23 << t23
        fe13 << e13
        fe23 << e23
