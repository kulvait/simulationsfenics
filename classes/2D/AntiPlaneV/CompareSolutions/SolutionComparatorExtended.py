'''
Created on April 28, 2016

Create LaTeX tables for power law model antiplane problem. 

@author: Vojtech Kulvait
'''
from DirectoryProcessor import DirectoryProcessor
from SolutionImporter import SolutionImporter
from BoundaryConditions import boundary_definition
from dolfin import *
import numpy
import math
import itertools

#Fenics solution dir is a dir where object with a solution were exported (R0/VK/FENICS)
#Info dir is a dir with info objects that were created for previous problem ... in this case there are 8 refinements
class SolutionComparatorExtended(object):
	def __init__(self, fenicsSolutionsDir, infoDir):
		self.fenicsSolutionsDir = fenicsSolutionsDir;
		self.infoDir = infoDir;
		self.dp = DirectoryProcessor(self.infoDir)
		self.si = SolutionImporter()
		self.mu = 9.15 * 10**39
		self.mu_linear = 26 * 10**9
		self.r = 5.25

	def generateComparisonTable(self, tableNum):
		maxRefinementNum = 8;
		out = []
		for i in range(0, maxRefinementNum):
			x = self.generateTableRowNL(tableNum, i+1);
			out.append(x)
		return out
		
	def generateDistanceTable(self, tableNum):
		maxRefinementNum = 8;
		out = [];
		for i in range(0, maxRefinementNum):
			x = self.generateDistanceRow(tableNum, i+1)
			out.append(x)
		return out

	def linearIndicators(self, solution):
		v = TestFunction(self.V)
		indicators = assemble(inner(self.psilinear()*nabla_grad(solution), nabla_grad(v))*dx)
		bc = boundary_definition();
		bcs = bc.fullBC(self.V)#If we use bc.zeroBC(self.V) we may then use bcs[i].apply(indicators). This is FeniCS hack to do the same on dirichlet boundary. 
		if type(bcs) is list:
			for i in range(0, len(bcs)):
				bcs[i].apply(indicators, solution.vector())
		else:
			bcs.apply(indicators, solution.vector())
		return(indicators);
		
	def nonLinearIndicators(self, solution):
		indicators = assemble(self.getF(solution))
		bc = boundary_definition();
		bcs = bc.fullBC(self.V)
		if type(bcs) is list:
			for i in range(0, len(bcs)):
				bcs[i].apply(indicators, solution.vector())
		else:
			bcs.apply(indicators, solution.vector())
		return(indicators);
		
	def getF(self, solution):
		v  = TestFunction(self.V)
		return (inner(self.psi(sqrt(2)*sqrt(inner(nabla_grad(solution), nabla_grad(solution))))*nabla_grad(solution), nabla_grad(v))*dx)
		
	def nrm(self, a):
		x = 0.0;
		for i in range(0, len(a)):
			x = a[i] ** 2 + x;
		return(math.sqrt(x))
				
	def generateTableRowNL(self, tableNum, refinement):
		[deg, _unussed, _unussed, _unussed] = self.dp.processParameters(tableNum);
		outputList  = [];
		#outputList.append(deg);#Alpha in degrees
		outputList.append(refinement-1);#Refinement num
		meshXML = self.dp.getMesh(refinement, tableNum)
		mesh = dolfin.Mesh(meshXML)
		self.V = dolfin.FunctionSpace(mesh, 'Lagrange', 2)
		outputList.append(self.V.dim())#Number of degrees of freedom
		outputList.append(mesh.num_cells())#Number of elements
		meshMaxXML = self.dp.getMesh(8, tableNum)
		meshMax = dolfin.Mesh(meshMaxXML)
		self.MaxV = dolfin.FunctionSpace(meshMax, 'Lagrange', 2)
		solutionFile = "{0}/table{1}_refinement{2}_FENICSNLsolution.txt".format(self.fenicsSolutionsDir, tableNum, refinement)
		solutionMaxFile = "{0}/table{1}_refinement{2}_FENICSNLsolution.txt".format(self.fenicsSolutionsDir, tableNum, 8)
		parameters['allow_extrapolation'] = True
		fenicsSol = self.si.importSolution(self.V, solutionFile);
		maxSol = self.si.importSolution(self.MaxV, solutionMaxFile);
		indicatorsF = self.nonLinearIndicators(fenicsSol)
		tmpV = self.V#Pro volani indikatoru potrebuju spravny prostor nad kterym je sestavena matice, ten je v promenne self.V tohle je hack.
		self.V = self.MaxV
		indicatorsM = self.nonLinearIndicators(maxSol)
		self.V = tmpV
		resF = Function(self.V, indicatorsF);
		res12 = dolfin.norm(resF, 'H1', mesh);
		outputList.append(res12) # Norm of residuum
		#indicatorsF = self.linearIndicators(fenicsSol)
		#indicatorsC = self.linearIndicators(comsolSol)
		#errorF = self.nrm(indicatorsF.array())
		#errorM = self.nrm(indicatorsM.array())
		#indiF = indicatorsF.array()
		#indiM = indicatorsM.array()
		#plot(Function(self.V, indicatorsC), interactive=True, title = "Error Comsol:({0})".format(errorC))
		#plot(Function(self.V, indicatorsF), interactive=True, title = "Error Fenics:({0})".format(errorF))
		#print "Euclidean error (max solution, {3} refined solution) = ({0}, {1})".format(errorM, errorF, refinement)
		#differenceSol = Function(self.V)
		#differenceSol.vector()[:] = fenicsSol.vector() - comsolSol.vector()
		#plot(fenicsSol, title="FeniCS solution {0}".format(refinement),  interactive=True)
		#plot(comsolSol, title="COMSOL solution {0}".format(refinement),  interactive=True)
		#plot(differenceSol, title="Difference solution {0}".format(refinement),  interactive=True)
		#plot(sqrt(inner(nabla_grad(fenicsSol), nabla_grad(fenicsSol))), title="FeniCS derivative norm {0}".format(refinement),  interactive=True)
		#plot(sqrt(inner(nabla_grad(comsolSol), nabla_grad(comsolSol))), title="Comsol derivative norm {0}".format(refinement),  interactive=True)
		#plot(sqrt(inner(nabla_grad(differenceSol), nabla_grad(differenceSol))), title="Difference solution derivative norm {0}".format(refinement),  interactive=True)
		#bc = boundary_definition()
		#F  = self.getNewtonFunction(comsolSol, V)
		#bcs = bc.fullBC(V)
		#problem = NonlinearVariationalProblem(F, comsolSol, bcs)
		#x = problem.residual_form()
		#A, b = assemble_system(x, Constant(0), bcs)
		#Interpolate to another mesh
		if(solutionFile == solutionMaxFile):
			outputList.append(0)
		else:	
			extrapolatedSolution = project(fenicsSol, self.MaxV)
			parameters['allow_extrapolation'] = False
			#u = project(u, V_without_hole)
			difference = project(extrapolatedSolution - maxSol, self.MaxV) 
			#Insert this code to perform mesh interpolation
			w12difference = dolfin.norm(difference, 'H1', meshMax)
			#l2normfenics=dolfin.norm(fenicsSol, 'L2', mesh)
			#l2normcomsol=dolfin.norm(comsolSol, 'L2', mesh)
			#w12normfenics=dolfin.norm(fenicsSol, 'H1', mesh)
			w12normMaxSol=dolfin.norm(maxSol, 'H1', meshMax)
			#w12normcomsol=dolfin.norm(comsolSol, 'H1', mesh)
			#l2normdifference=dolfin.norm(differenceSol, 'L2', mesh)
			#w12normdifference=dolfin.norm(differenceSol, 'H1', mesh)
			#rell2 = 2*l2normdifference/(l2normfenics+l2normcomsol)
			#relh1 = 2*w12normdifference/(w12normfenics+w12normcomsol)
			#outputList.append([l2normfenics, l2normcomsol, w12normfenics, w12normcomsol, l2normdifference, w12normdifference, rell2, relh1])
			outputList.append(w12difference/w12normMaxSol)#Relative W12 norm of solution
		return outputList;

	def generateDistanceRow(self, tableNum, refinement):
		[deg, _unused, _unused, _unused] = self.dp.processParameters(tableNum);
		outputList  = [];
		#outputList.append(deg);#Alpha in degrees
		outputList.append(refinement-1);#Refinement num
		meshXML = self.dp.getMesh(refinement, tableNum)
		mesh = dolfin.Mesh(meshXML)
		self.V = dolfin.FunctionSpace(mesh, 'Lagrange', 2)
		n = self.V.dim()                                                                      
		d = mesh.geometry().dim()    
		dof_coordinates = self.V.dofmap().tabulate_all_coordinates(mesh)                      
		dof_coordinates.resize((n, d))                                                   
		dof_x = dof_coordinates[:, 0]                                                    
		dof_y = dof_coordinates[:, 1]    
		airySolFile = "{0}/table{1}_refinement{2}_FENICSNLsolution.txt".format(self.fenicsSolutionsDir, tableNum, refinement)
		airyLinFile = "{0}/table{1}_refinement{2}_FENICSLsolution.txt".format(self.fenicsSolutionsDir, tableNum, refinement)
		A = self.si.importSolution(self.V, airySolFile);
		A_lin = self.si.importSolution(self.V, airyLinFile)	
		T = self.gett(A, mesh)#Type is Function described here http://fenicsproject.org/documentation/dolfin/1.0.0/python/programmers-reference/cpp/Function.html
		T_lin = self.gett(A_lin, mesh)
		T_v = T.vector()
		T_lin_v = T_lin.vector()
		T_v = as_backend_type(T_v)
		T_lin_v = as_backend_type(T_lin_v)
		T_v = T_v.vec().getArray()
		#matrixOfValues = numpy.column_stack((dof_coordinates, T_v, T_lin_v))
		#numpy.savetxt("{0}/table{1}_refinement{2}.csv".format(self.fenicsSolutionsDir,tableNum, refinement), matrixOfValues, delimiter="\t")
		T_lin_v = T_lin_v.vec().getArray()
		T_max_i = T_v.argmax()
		T_lin_max_i = T_lin_v.argmax()
		T_max = T_v[T_max_i]
		T_max_x = dof_x[T_max_i]
		T_max_y = dof_y[T_max_i]
		T_lin_max = T_lin_v[T_lin_max_i]
		T_lin_max_x = dof_x[T_lin_max_i]
		T_lin_max_y = dof_y[T_lin_max_i]
		outputList.append(T_max)
		#outputList.append(T_max_x)
		#outputList.append(T_max_y)
		outputList.append(T_lin_max)
		#outputList.append(T_lin_max_x)
		#outputList.append(T_lin_max_y)
		#outputList.append(T(0.5,0.5))
		#outputList.append(T_lin(0.5,0.5))
		if T_max > 6*10**8:
			outputList.append(self.getDistance(T, 6*10**8, numpy.array([0,0.5]), numpy.array([0.5,0.5])))
		else:
			outputList.append('-')
		if T_max > 9*10**8:
			outputList.append(self.getDistance(T, 9*10**8, numpy.array([0,0.5]), numpy.array([0.5,0.5])))
		else:
			outputList.append('-')
		if T_lin_max > 6*10**8:
			outputList.append(self.getDistance(T_lin, 6*10**8, numpy.array([0,0.5]), numpy.array([0.5,0.5])))
		else:
			outputList.append('-')
		if T_lin_max > 9*10**8:
			outputList.append(self.getDistance(T_lin, 9*10**8, numpy.array([0,0.5]), numpy.array([0.5,0.5])))
		else:
			outputList.append('-')
		return outputList
	
	def generateCsvValues(self, tableNum):
		for refinement in range(1,9):
			[deg, _unused, _unused, _unused] = self.dp.processParameters(tableNum);
			meshXML = self.dp.getMesh(refinement, tableNum)
			mesh = dolfin.Mesh(meshXML)
			self.V = dolfin.FunctionSpace(mesh, 'Lagrange', 2)
			n = self.V.dim()
			d = mesh.geometry().dim()
			dof_coordinates = self.V.dofmap().tabulate_all_coordinates(mesh)
			dof_coordinates.resize((n, d))
			airySolFile = "{0}/table{1}_refinement{2}_FENICSNLsolution.txt".format(self.fenicsSolutionsDir, tableNum, refinement)
			airyLinFile = "{0}/table{1}_refinement{2}_FENICSLsolution.txt".format(self.fenicsSolutionsDir, tableNum, refinement)
			A = self.si.importSolution(self.V, airySolFile);
			A_lin = self.si.importSolution(self.V, airyLinFile)
			T = self.gett(A, mesh)#Type is Function described here http://fenicsproject.org/documentation/dolfin/1.0.0/python/programmers-reference/cpp/Function.html
			T_lin = self.gett(A_lin, mesh)
			T_v = T.vector()
			T_lin_v = T_lin.vector()
			T_v = as_backend_type(T_v)
			T_lin_v = as_backend_type(T_lin_v)
			T_v = T_v.vec().getArray()
			T_lin_v = T_lin_v.vec().getArray()
			matrixOfValues = numpy.column_stack((dof_coordinates, T_v, T_lin_v))
			numpy.savetxt("{0}/table{1}_refinement{2}.csv".format(self.fenicsSolutionsDir,tableNum, refinement), matrixOfValues, delimiter="\t")


	def differenceBetweenRefinements(self, tableNum, refinement1, refinement2, nonlinear, baseTValue, maxDifference):
		i = 1
		distance1=1
		distance2=1
		meshXML1 = self.dp.getMesh(refinement1, tableNum)
		meshXML2 = self.dp.getMesh(refinement2, tableNum)
		mesh1 = dolfin.Mesh(meshXML1)
		mesh2 = dolfin.Mesh(meshXML2)
		V1 = dolfin.FunctionSpace(mesh1, 'Lagrange', 2)
		V2 = dolfin.FunctionSpace(mesh2, 'Lagrange', 2)
		if nonlinear:
			solFile1 = "{0}/table{1}_refinement{2}_FENICSNLsolution.txt".format(self.fenicsSolutionsDir, tableNum, refinement1)
			solFile2 = "{0}/table{1}_refinement{2}_FENICSNLsolution.txt".format(self.fenicsSolutionsDir, tableNum, refinement2)
		else:
			solFile1 = "{0}/table{1}_refinement{2}_FENICSLsolution.txt".format(self.fenicsSolutionsDir, tableNum, refinement1)
			solFile2 = "{0}/table{1}_refinement{2}_FENICSLsolution.txt".format(self.fenicsSolutionsDir, tableNum, refinement2)
		A1 = self.si.importSolution(V1, solFile1)
		A2 = self.si.importSolution(V2, solFile2)
		T1 = self.gett(A1, mesh1)
		T2 = self.gett(A2, mesh2)
		while abs(distance2-distance1)/(0.5*distance1+0.5*distance2) < maxDifference:
			distance1 = self.getDistance(T1, baseTValue*i, numpy.array([0,0.5]), numpy.array([0.5,0.5]))
			distance2 = self.getDistance(T2, baseTValue*i, numpy.array([0,0.5]), numpy.array([0.5,0.5]))
			#print("Iteration {0} distance1={1} distance2={2}".format(i, distance1, distance2))
			i = i+1
		return i-1;

	def getDistance(self, F, value, p0, p1, tol=None):
		d = 0.5
		inc = 0.25
		if tol is None:
			if value is 0:
				tol = 10**-5
			else:
				tol = value / 100000
		i = 0
		while abs(self.valueOf(F, p0+d*(p1-p0))-value)>tol and i != 100:
			if self.valueOf(F, p0+d*(p1-p0)) > value:
				d = d - inc
			else:
				d = d + inc
			inc = inc/2
			i = i + 1
			#print("Value of function going to {} at point with d={} is {}.".format(value, d, self.valueOf(F, p0+d*(p1-p0))))
		#print(numpy.linalg.norm(p1 - p0 - d*(p1-p0)))
		return numpy.linalg.norm(p1 - p0 - d*(p1-p0)); 
		
	def valueOf(self, F, numpypoint):
		return F(numpypoint[0], numpypoint[1])

	def gett(self, A, mesh):
		tvector = project(grad(A), VectorFunctionSpace(mesh, 'Lagrange', 2))
		[t23, t13] = tvector.split(deepcopy=True)  # extract components
		t = project(sqrt(t13*t13+t23*t23), FunctionSpace(mesh, 'Lagrange', 2))
		t.rename('T_modulus', 'sqrt(t13^2+t23^2)')
		return(t)

	def psi(self, x):
		return(( (1 + x ** 2) ** ((self.r - 2)/2) )/(2*self.mu))

	def psilinear(self):
		return(1/(2*self.mu_linear))
	
	def generateImages(self, tableNum, outputDir, refinement=None):
		if refinement is None:
			refinement = self.getDensestMeshNumber(5, tableNum)
		else:
			refinement = int(refinement)
		fenicsFile = "{0}/table{1}_refinement{2}_FENICSNLsolution.txt".format(self.fenicsSolutionsDir, tableNum, refinement)
		comsolFile = "{0}/table{1}_refinement{2}_NLsolution.txt".format(self.comsolSolutionsDir, tableNum, refinement)
		meshXML = self.dp.getMesh(refinement, tableNum)
		mesh = dolfin.Mesh(meshXML)
		V = dolfin.FunctionSpace(mesh, 'Lagrange', 2)
		fenicsSolNL = SolutionImporter(self.si.importSolution(V, fenicsFile), V);
		comsolSolNL = SolutionImporter(self.si.importSolution(V, comsolFile), V);
		t13 = fenicsSolNL.gett13()
		t23 = fenicsSolNL.gett23()
		e13 = fenicsSolNL.gete13Nonlinear()
		e23 = fenicsSolNL.gete23Nonlinear()		
		ft13 = dolfin.io.File("{0}/table{1}_FeniCSnonlinear_t13.pvd".format(outputDir, tableNum))
		ft23 = dolfin.io.File("{0}/table{1}_FeniCSnonlinear_t23.pvd".format(outputDir, tableNum))
		fe13 = dolfin.io.File("{0}/table{1}_FeniCSnonlinear_e13.pvd".format(outputDir, tableNum))
		fe23 = dolfin.io.File("{0}/table{1}_FeniCSnonlinear_e23.pvd".format(outputDir, tableNum))
		ft13 << t13
		ft23 << t23
		fe13 << e13
		fe23 << e23
		t13 = comsolSolNL.gett13()
		t23 = comsolSolNL.gett23()
		e13 = comsolSolNL.gete13Nonlinear()
		e23 = comsolSolNL.gete23Nonlinear()		
		ft13 = dolfin.io.File("{0}/table{1}_COMSOLnonlinear_t13.pvd".format(outputDir, tableNum))
		ft23 = dolfin.io.File("{0}/table{1}_COMSOLnonlinear_t23.pvd".format(outputDir, tableNum))
		fe13 = dolfin.io.File("{0}/table{1}_COMSOLnonlinear_e13.pvd".format(outputDir, tableNum))
		fe23 = dolfin.io.File("{0}/table{1}_COMSOLnonlinear_e23.pvd".format(outputDir, tableNum))
		ft13 << t13
		ft23 << t23
		fe13 << e13
		fe23 << e23
		fenicsFile = "{0}/table{1}_refinement{2}_FENICSLsolution.txt".format(self.fenicsSolutionsDir, tableNum, refinement)
		comsolFile = "{0}/table{1}_refinement{2}_Lsolution.txt".format(self.comsolSolutionsDir, tableNum, refinement)
		fenicsSolNL = SolutionImporter(self.si.importSolution(V, fenicsFile), V);
		comsolSolNL = SolutionImporter(self.si.importSolution(V, comsolFile), V);
		t13 = fenicsSolNL.gett13()
		t23 = fenicsSolNL.gett23()
		e13 = fenicsSolNL.gete13linear()
		e23 = fenicsSolNL.gete23linear()		
		ft13 = dolfin.io.File("{0}/table{1}_FeniCSlinear_t13.pvd".format(outputDir, tableNum))
		ft23 = dolfin.io.File("{0}/table{1}_FeniCSlinear_t23.pvd".format(outputDir, tableNum))
		fe13 = dolfin.io.File("{0}/table{1}_FeniCSlinear_e13.pvd".format(outputDir, tableNum))
		fe23 = dolfin.io.File("{0}/table{1}_FeniCSlinear_e23.pvd".format(outputDir, tableNum))
		ft13 << t13
		ft23 << t23
		fe13 << e13
		fe23 << e23
		t13 = comsolSolNL.gett13()
		t23 = comsolSolNL.gett23()
		e13 = comsolSolNL.gete13linear()
		e23 = comsolSolNL.gete23linear()		
		ft13 = dolfin.io.File("{0}/table{1}_COMSOLlinear_t13.pvd".format(outputDir, tableNum))
		ft23 = dolfin.io.File("{0}/table{1}_COMSOLlinear_t23.pvd".format(outputDir, tableNum))
		fe13 = dolfin.io.File("{0}/table{1}_COMSOLlinear_e13.pvd".format(outputDir, tableNum))
		fe23 = dolfin.io.File("{0}/table{1}_COMSOLlinear_e23.pvd".format(outputDir, tableNum))
		ft13 << t13
		ft23 << t23
		fe13 << e13
		fe23 << e23
