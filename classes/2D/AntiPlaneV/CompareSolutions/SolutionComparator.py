'''
Created on May 15, 2014

@author: Vojtech Kulvait
'''

from DirectoryProcessor import DirectoryProcessor
from SolutionImporter import SolutionImporter
from BoundaryConditions import boundary_definition
from dolfin import *
import numpy
import math


class SolutionComparator(object):
	def __init__(self, fenicsSolutionsDir, comsolSolutionsDir, infoDir, einfinity=None):
		if einfinity is None:
			self.einfinity = 0.01
		else:
			self.einfinity = float(einfinity)
		self.fenicsSolutionsDir = fenicsSolutionsDir;
		self.comsolSolutionsDir = comsolSolutionsDir;
		self.infoDir = infoDir;
		self.dp = DirectoryProcessor(self.infoDir)
		self.si = SolutionImporter()

	def generateComparisonTable(self, tableNum):
		maxRefinementNum = self.dp.getDensestMeshNumber(5, tableNum);
		out = []
		for i in range(0, maxRefinementNum):
			x = self.generateTableRowNL(tableNum, i+1);
			out.append(x)
		return out
	
	def linearIndicators(self, solution):
		v = TestFunction(self.V)
		indicators = assemble(inner(self.psilinear()*nabla_grad(solution), nabla_grad(v))*dx)
		bc = boundary_definition();
		bcs = bc.fullBC(self.V)#If we use bc.zeroBC(self.V) we may then use bcs[i].apply(indicators). This is FeniCS hack to do the same on dirichlet boundary. 
		if type(bcs) is list:
			for i in range(0, len(bcs)):
				bcs[i].apply(indicators, solution.vector())
		else:
			bcs.apply(indicators, solution.vector())
		return(indicators);
	
	def nonLinearIndicators(self, solution):
		indicators = assemble(self.getF(solution))
		bc = boundary_definition();
		bcs = bc.fullBC(self.V)
		if type(bcs) is list:
			for i in range(0, len(bcs)):
				bcs[i].apply(indicators, solution.vector())
		else:
			bcs.apply(indicators, solution.vector())
		return(indicators);
	
	def getF(self, solution):
		v  = TestFunction(self.V)
		return (inner(self.psi(sqrt(2)*sqrt(inner(nabla_grad(solution), nabla_grad(solution))))*nabla_grad(solution), nabla_grad(v))*dx)
	
	def nrm(self, a):
		x = 0.0;
		for i in range(0, len(a)):
			x = a[i] ** 2 + x;
		return(math.sqrt(x))
		
			
	def generateTableRowNL(self, tableNum, refinement):
		[self.deg, self.a, self.kappa, self.mu] = self.dp.processParameters(tableNum);
		outputList  = [];
		outputList.append(self.mu);
		outputList.append(self.a);
		outputList.append(self.kappa);
		outputList.append(self.deg);
		outputList.append('T')
		outputList.append(refinement-1);
		meshXML = self.dp.getMesh(refinement, tableNum)
		mesh = dolfin.Mesh(meshXML)
		self.V = dolfin.FunctionSpace(mesh, 'Lagrange', 2)
		fenicsFile = "{0}/table{1}_refinement{2}_FENICSNLsolution.txt".format(self.fenicsSolutionsDir, tableNum, refinement)
		#Compare Comsol L to Comsol NL
		#fenicsFile = "{0}/table{1}_refinement{2}_COMSOLLsolution.txt".format(self.comsolSolutionsDir, tableNum, refinement)
		comsolFile = "{0}/table{1}_refinement{2}_COMSOLNLsolution.txt".format(self.comsolSolutionsDir, tableNum, refinement)
		fenicsSol = self.si.importSolution(self.V, fenicsFile);
		comsolSol = self.si.importSolution(self.V, comsolFile);
		indicatorsF = self.nonLinearIndicators(fenicsSol)
		indicatorsC = self.nonLinearIndicators(comsolSol)
		#indicatorsF = self.linearIndicators(fenicsSol)
		#indicatorsC = self.linearIndicators(comsolSol)
		errorF = self.nrm(indicatorsF.array())
		errorC = self.nrm(indicatorsC.array())
		indiF = indicatorsF.array()
		indiC = indicatorsC.array()
		#plot(Function(self.V, indicatorsC), interactive=True, title = "Error Comsol:({0})".format(errorC))
		#plot(Function(self.V, indicatorsF), interactive=True, title = "Error Fenics:({0})".format(errorF))
		
		#This hack is to extract residual form from NonlinearVariationalProblem. 
		#It uses by default norm of residuum parameters["convergence_criterion"] == "residual" same as numpy.linalg.norm(nonLinearIndicators(fenicsSol).array())
		#It may also use size of refinement du parameters["convergence_criterion"] == "incremental"
		#du = TrialFunction(self.V)
		#FF = self.getF(fenicsSol)
		#JF  = derivative(FF, fenicsSol, du)  # Gateaux derivative in dir. of du
		#bc = boundary_definition();
		#bcs = bc.fullBC(self.V)
		#nelze dosadit problem = NonlinearVariationalProblem(F, u_, bc.fullBC(V), J) asi kvuli praci s pointery
		#problem = NonlinearVariationalProblem(FF, fenicsSol, bcs, JF)
		#RES = problem.residual_form()
		#indicators = assemble(RES)
		#if type(bcs) is list:
		#	for i in range(0, len(bcs)):
		#		bcs[i].apply(indicators, fenicsSol.vector())
		#else:
		#	bcs.apply(indicators, fenicsSol.vector())
		#error = self.nrm(indicators.array())
		#plot(Function(self.V, indicators), interactive=True, title = "Error Fenics:({0})".format(error))
		
		print "Euclidean error (comsol, fenics) = ({0}, {1})".format(errorC, errorF)
		
		differenceSol = Function(self.V)
		differenceSol.vector()[:] = fenicsSol.vector() - comsolSol.vector()
		#plot(fenicsSol, title="FeniCS solution {0}".format(refinement),  interactive=True)
		#plot(comsolSol, title="COMSOL solution {0}".format(refinement),  interactive=True)
		#plot(differenceSol, title="Difference solution {0}".format(refinement),  interactive=True)
		#plot(sqrt(inner(nabla_grad(fenicsSol), nabla_grad(fenicsSol))), title="FeniCS derivative norm {0}".format(refinement),  interactive=True)
		#plot(sqrt(inner(nabla_grad(comsolSol), nabla_grad(comsolSol))), title="Comsol derivative norm {0}".format(refinement),  interactive=True)
		#plot(sqrt(inner(nabla_grad(differenceSol), nabla_grad(differenceSol))), title="Difference solution derivative norm {0}".format(refinement),  interactive=True)
		#bc = boundary_definition()
		#F  = self.getNewtonFunction(comsolSol, V)
		#bcs = bc.fullBC(V)
		#problem = NonlinearVariationalProblem(F, comsolSol, bcs)
		#x = problem.residual_form()
		#A, b = assemble_system(x, Constant(0), bcs)
		outputList.append(self.V.dim());
		outputList.append(mesh.num_cells())
		l2normfenics=dolfin.norm(fenicsSol, 'L2', mesh)
		l2normcomsol=dolfin.norm(comsolSol, 'L2', mesh)
		w12normfenics=dolfin.norm(fenicsSol, 'H1', mesh)
		w12normcomsol=dolfin.norm(comsolSol, 'H1', mesh)
		l2normdifference=dolfin.norm(differenceSol, 'L2', mesh)
		w12normdifference=dolfin.norm(differenceSol, 'H1', mesh)
		rell2 = 2*l2normdifference/(l2normfenics+l2normcomsol)
		relh1 = 2*w12normdifference/(w12normfenics+w12normcomsol)
		outputList.append([l2normfenics, l2normcomsol, w12normfenics, w12normcomsol, l2normdifference, w12normdifference, rell2, relh1])
		return outputList;
	
	def psi(self, x):
		return(1/((2*self.mu)** (1/self.a)*((1+self.kappa*(x ** self.a)) ** (1/self.a))))
	
	def psilinear(self):
		return(1/(2*self.mu))
		
	def generateImages(self, tableNum, outputDir, refinement=None):
		if refinement is None:
			refinement = self.getDensestMeshNumber(5, tableNum)
		else:
			refinement = int(refinement)
		fenicsFile = "{0}/table{1}_refinement{2}_FENICSNLsolution.txt".format(self.fenicsSolutionsDir, tableNum, refinement)
		comsolFile = "{0}/table{1}_refinement{2}_NLsolution.txt".format(self.comsolSolutionsDir, tableNum, refinement)
		meshXML = self.dp.getMesh(refinement, tableNum)
		mesh = dolfin.Mesh(meshXML)
		V = dolfin.FunctionSpace(mesh, 'Lagrange', 2)
		fenicsSolNL = SolutionImporter(self.si.importSolution(V, fenicsFile), V);
		comsolSolNL = SolutionImporter(self.si.importSolution(V, comsolFile), V);
		t13 = fenicsSolNL.gett13()
		t23 = fenicsSolNL.gett23()
		e13 = fenicsSolNL.gete13Nonlinear()
		e23 = fenicsSolNL.gete23Nonlinear()		
		ft13 = dolfin.io.File("{0}/table{1}_FeniCSnonlinear_t13.pvd".format(outputDir, tableNum))
		ft23 = dolfin.io.File("{0}/table{1}_FeniCSnonlinear_t23.pvd".format(outputDir, tableNum))
		fe13 = dolfin.io.File("{0}/table{1}_FeniCSnonlinear_e13.pvd".format(outputDir, tableNum))
		fe23 = dolfin.io.File("{0}/table{1}_FeniCSnonlinear_e23.pvd".format(outputDir, tableNum))
		ft13 << t13
		ft23 << t23
		fe13 << e13
		fe23 << e23
		t13 = comsolSolNL.gett13()
		t23 = comsolSolNL.gett23()
		e13 = comsolSolNL.gete13Nonlinear()
		e23 = comsolSolNL.gete23Nonlinear()		
		ft13 = dolfin.io.File("{0}/table{1}_COMSOLnonlinear_t13.pvd".format(outputDir, tableNum))
		ft23 = dolfin.io.File("{0}/table{1}_COMSOLnonlinear_t23.pvd".format(outputDir, tableNum))
		fe13 = dolfin.io.File("{0}/table{1}_COMSOLnonlinear_e13.pvd".format(outputDir, tableNum))
		fe23 = dolfin.io.File("{0}/table{1}_COMSOLnonlinear_e23.pvd".format(outputDir, tableNum))
		ft13 << t13
		ft23 << t23
		fe13 << e13
		fe23 << e23
		fenicsFile = "{0}/table{1}_refinement{2}_FENICSLsolution.txt".format(self.fenicsSolutionsDir, tableNum, refinement)
		comsolFile = "{0}/table{1}_refinement{2}_Lsolution.txt".format(self.comsolSolutionsDir, tableNum, refinement)
		fenicsSolNL = SolutionImporter(self.si.importSolution(V, fenicsFile), V);
		comsolSolNL = SolutionImporter(self.si.importSolution(V, comsolFile), V);
		t13 = fenicsSolNL.gett13()
		t23 = fenicsSolNL.gett23()
		e13 = fenicsSolNL.gete13linear()
		e23 = fenicsSolNL.gete23linear()		
		ft13 = dolfin.io.File("{0}/table{1}_FeniCSlinear_t13.pvd".format(outputDir, tableNum))
		ft23 = dolfin.io.File("{0}/table{1}_FeniCSlinear_t23.pvd".format(outputDir, tableNum))
		fe13 = dolfin.io.File("{0}/table{1}_FeniCSlinear_e13.pvd".format(outputDir, tableNum))
		fe23 = dolfin.io.File("{0}/table{1}_FeniCSlinear_e23.pvd".format(outputDir, tableNum))
		ft13 << t13
		ft23 << t23
		fe13 << e13
		fe23 << e23
		t13 = comsolSolNL.gett13()
		t23 = comsolSolNL.gett23()
		e13 = comsolSolNL.gete13linear()
		e23 = comsolSolNL.gete23linear()		
		ft13 = dolfin.io.File("{0}/table{1}_COMSOLlinear_t13.pvd".format(outputDir, tableNum))
		ft23 = dolfin.io.File("{0}/table{1}_COMSOLlinear_t23.pvd".format(outputDir, tableNum))
		fe13 = dolfin.io.File("{0}/table{1}_COMSOLlinear_e13.pvd".format(outputDir, tableNum))
		fe23 = dolfin.io.File("{0}/table{1}_COMSOLlinear_e23.pvd".format(outputDir, tableNum))
		ft13 << t13
		ft23 << t23
		fe13 << e13
		fe23 << e23
		