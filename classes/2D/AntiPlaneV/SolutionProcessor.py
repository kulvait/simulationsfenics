'''
Created on May 15, 2014

@author: vojta
'''

from dolfin import project
from dolfin import grad
from dolfin import VectorFunctionSpace
import dolfin

class SolutionProcessor(object):
    def __init__(self, u, V, lagrangeSpaceDim = None):
        self.u = u
        self.V = V
        self.mesh = V.mesh()
        if lagrangeSpaceDim is None:
            self.lagrangeSpaceDim = 2
        else:
            self.lagrangeSpaceDim = int(lagrangeSpaceDim)
        
    def L2Norm(self):
        return()
    
    def gett13(self):
        tvector = project(grad(self.u), VectorFunctionSpace(self.mesh, 'Lagrange', self.lagrangeSpaceDim))
        [t23, t13] = tvector.split(deepcopy=True)  # extract components
        return(t13)

    def gett23(self):
        tvector = project(grad(self.u), VectorFunctionSpace(self.mesh, 'Lagrange', self.lagrangeSpaceDim))
        [t23, t13] = tvector.split(deepcopy=True)  # extract components
        t23 = project(-t23, self.V)
        return(t23)
        
    def gete13Linear(self):
        return project(self.gett13(self.u)/(2*self.mu), self.V)
        
    def gete23Linear(self):
        return project(self.gett23(self.u)/(2*self.mu), self.V)
    
    def gete13Nonlinear(self):
        return project(self.gett13(self.u)*self.psi(dolfin.sqrt(2)*dolfin.sqrt(dolfin.inner(dolfin.nabla_grad(self.u), dolfin.nabla_grad(self.u)))), self.V)
        
    def gete23Nonlinear(self):
        return project(self.gett23(self.u)*self.psi(dolfin.sqrt(2)*dolfin.sqrt(dolfin.inner(dolfin.nabla_grad(self.u), dolfin.nabla_grad(self.u)))), self.V)
        