# -*- coding: utf-8 -*-

'''
Created on Apr 9, 2014

@author: Vojtech Kulvait
'''
from dolfin import *

class boundary_definition:
    def __init__(self, l=None, F=None):
        if l is None:
            self.l = 1.0
        else:
            self.l = float(l)
        if F is None:
            self.F = 10.0 ** 8
        else:
            self.F = float(F)

    def Gamma_1267(self, V, expression):
        return(DirichletBC(V, expression, boundary_Gamma1267))
    
    def Gamma3(self, V, expression):
        return(DirichletBC(V, expression, boundary_Gamma3))
    
    def Gamma4(self, V, expression):
        return(DirichletBC(V, expression, boundary_Gamma4))
    
    def Gamma5(self, V, expression):
        return(DirichletBC(V, expression, boundary_Gamma5))
    
    class Gamma3Expression(Expression):
        def __init__(self, l=None, F=None):
            if l is None:
                self.l = 1.0
            else:
                self.l = float(l)
            if F is None:
                self.F = 1.0
            else:
                self.F = float(F) 
        def eval(self, values, x):
            values[0] = self.F*(self.l-x[0])
        
    class Gamma4Expression(Expression):
        def __init__(self, l=None, F=None):
            if l is None:
                self.l = 1.0
            else:
                self.l = float(l)
            if F is None:
                self.F = 1.0
            else:
                self.F = float(F)
                
        def eval(self, values, x):
            values[0] = self.F*self.l
        
    class Gamma5Expression(Expression):
        def __init__(self, l=None, F=None):
            if l is None:
                self.l = 1.0
            else:
                self.l = float(l)
            if F is None:
                self.F = 1.0
            else:
                self.F = float(F)
                
        def eval(self, values, x):
            values[0] = self.F*(self.l-x[0])
        
    def fullBC(self, V):
        return([self.Gamma_1267(V, Constant(0.0)), self.Gamma3(V, self.Gamma3Expression(self.l, self.F)), \
                self.Gamma4(V, self.Gamma4Expression(self.l, self.F)), self.Gamma5(V, self.Gamma5Expression(self.l, self.F))])
    
    def zeroBC(self, V):
        return ([self.Gamma_1267(V, Constant(0.0)), self.Gamma3(V, Constant(0.0)), self.Gamma4(V, Constant(0.0)), \
                 self.Gamma5(V, Constant(0.0))])
        
    def zeroBC2(self, V):
        return (DirichletBC(V, Constant(0.0), boundary_anywhere))

class BC(Expression):
    def __init__(self, l=None, F=None):
        if l is None:
            self.l = 1.0
        else:
            self.l = float(l)
        if F is None:
            self.F = 100*10.0**6
        else:
            self.F = float(F)
            
    def eval(self, values, x):
        if dolfin.near(x[1], 0.):
            values[0] = -self.F*x[0]
        elif dolfin.near(x[1], self.l):
            values[0] = -self.F*x[0])
        elif dolfin.near(x[0], self.l):
            values[0] = -self.F*self.l
        else:
            values[0] = 0.
            
def boundary_Gamma3(x, on_boundary):
    return(on_boundary and dolfin.near(x[1], 1.0))

def boundary_Gamma4(x, on_boundary):
    return(on_boundary and dolfin.near(x[0], 0.0))

def boundary_Gamma5(x, on_boundary):
    return(on_boundary and dolfin.near(x[1], 0.0))

def boundary_Gamma1267(x, on_boundary):
    if on_boundary and dolfin.near(x[0], 1.0):#THIS IS A FIX FOR https://bitbucket.org/fenics-project/dolfin/issue/309/dirichlet-boundary-conditions
        return(True);
    if on_boundary :
        if boundary_Gamma3(x, on_boundary) == False:
            if boundary_Gamma4(x, on_boundary) == False:
                if boundary_Gamma5(x, on_boundary) == False:
                    return(True);
    return(False);

def boundary_anywhere(x, on_boundary):
    return(on_boundary)


